:: TODO: Corregir los nombres marcados con "khedice"

::SET SongPMS1=
::SET SongPMS2=
::CALL :Process "preview.wav";shortf;%TR1%;filename;origfolder;destfolder;false;false;false;

SET link=facebook.com/NEX-Team-1514347278646894/
CALL :SetTitleInit
CHCP 65001

SET AutoCategory=true
SET DebugMode=true
SET WriteImageInfoFile=true
SET ErrorGot=No errors found.

::  Avoid matching "Pop'n Music 1" with "Pop'n Music 10" or "Pop'n Music 11"
SET f_pnm1ac="01_Pop'n Music 1"
SET f_pnm2ac="02_Pop'n Music 2"
SET f_pnm3ac="03_Pop'n Music 3"
SET f_pnm4ac="04_Pop'n Music 4"
SET f_pnm5ac="05_Pop'n Music 5"
SET f_pnm6ac="06_Pop'n Music 6"
SET f_pnm7ac="07_Pop'n Music 7"
SET f_pnm8ac="08_Pop'n Music 8"
SET f_pnm9ac="09_Pop'n Music 9"
SET f_pnm10ac="10_Pop'n Music 10"
SET f_pnm11ac="11_Pop'n Music 11"
SET f_pnm12ac="12_Pop'n Music 12 Iroha"
SET f_pnm13ac="13_Pop'n Music 13 Carnival"
SET f_pnm14ac="14_Pop'n Music 14 Fever"
SET f_pnm15ac="15_Pop'n Music 15 Adventure"
SET f_pnm16ac="16_Pop'n Music 16 Party"
SET f_pnm17ac="17_Pop'n Music 17 The Movie"
SET f_pnm18ac="18_Pop'n Music 18 Sengoku Retsuden"
SET f_pnm19ac="19_Pop'n Music 19 Tune Street"
SET f_pnm20ac="20_Pop'n Music 20 Fantasia"
SET f_pnm21ac="21_Pop'n Music 21 Sunny Park"
SET f_pnm22ac="22_Pop'n Music 22 Lapistoria"
SET f_pnm23ac="23_Pop'n Music 23 eclale"
SET f_pnm24ac="24_Pop'n Music 24 Usagi to neko to shounen no yume"
::	Non mainstream versions
SET f_pnmee="EE_Pop'n Music ee'Mall"
SET f_pnmani1="CS_Pop'n Music Animelo 1"
SET f_pnmani2="CS_Pop'n Music Animelo 2"
::---------  SCRIPT HELPERS  --------------------
SET SimfileImagesFileName="__UI_Assets.txt"
SET preview_dest="__SongPreview.wav"
SET cprefix="_pre"
SET cprefix_def="_pre"
SET f2prefx="_2nd"
SET f2prefx_def="_2nd"
SET f3prefx="_3rd"
SET f3prefx_def="_3rd"
::---------  PROCESS TYPES  ---------------------
SET TR1="Process_Regular_1"
SET TR2="Process_Regular_2"
SET TR3="Process_Regular_3"
SET TN1="Process_Neutral_1"
SET TN2="Process_Neutral_2"
SET TS1="Process_Simple_1"
SET TS2="Process_Simple_2"
SET TX1="Process_Extra_1"
SET P10="Process_POP10"
SET RPK="Process_RootedPack"
::---------  FIXED VALUES  ----------------------
SET pr_d1="0001.wav"

SET DeleteRAR=false
IF %DebugMode%==true (ECHO Debug Mode On) ELSE ECHO Off
IF %DebugMode%==true (ECHO -Skipping CLS-) ELSE CLS

ECHO	Este programa resolvera los simfiles de:
ECHO/
ECHO		- Pop'n Music 1 al 10
ECHO		- Pop'n Music 14
ECHO		- Pop'n Music ee'MALL
ECHO/
ECHO	Borrar RARs despues del proceso?
ECHO	(ENTER para no borrar nada)
SET /P	DeleteRAR=true o 1 para borrar rars: 

IF %DeleteRAR%==1 SET DeleteRAR=true
IF %DebugMode%==true (ECHO -Skipping CLS-) ELSE CLS

::CALL :Module_BringFilesToProgramRoot

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 1  (AC)
CALL :TitleLookingUp "Pop'n Music 1"
SET curgamef=%f_pnm1ac%

SET SongPMS1="ポップス [5button]_102";"ポップス [BATTLE]_189";"ポップス [HYPER]_189";"ポップス [NORMAL]_111";"ポップス [NORMAL]_189";
SET SongPMS2=" [5button]_102";" [BATTLE]_189";" [HYPER]_189";" [NORMAL]_111";" [NORMAL]_189";
CALL :Process %pr_d1%;"pops";%TR2%;"[PMS] Pop'n Music - ポップス - I REALLY WANT TO HURT YOU 190927.rar";"ポップス - I REALLY WANT TO HURT YOU";"POPS - I REALLY WANT TO HURT YOU";"kc_0000";"ha_rie_1a";"ha_rie_1b";

SET SongPMS1="Ｊ－テクノ [5button]_103";"Ｊ－テクノ [BATTLE]_174";"Ｊ－テクノ [HYPER]_175";"Ｊ－テクノ [NORMAL]_104";
SET SongPMS2=" [5button]_103";" [BATTLE]_174";" [HYPER]_175";" [NORMAL]_104";
CALL :Process %pr_d1%;"jtekno";%TR1%;"[PMS] Pop'n Music - Ｊ－テクノ - Quick Master 190927.rar";"Ｊ－テクノ - Quick Master";"J-TEKNO - Quick Master";"kc_0002";"ha_shol_1a";"ha_shol_1b";

SET SongPMS1="ファンタジー [5button]_100";"ファンタジー [BATTLE]_131";"ファンタジー [EX]_431";"ファンタジー [HYPER]_327";"ファンタジー [NORMAL]_117";"ファンタジー [NORMAL]_327";
SET SongPMS2=" [5button]_100";" [BATTLE]_131";" [EX]_431";" [HYPER]_327";" [NORMAL]_117";" [NORMAL]_327";
CALL :Process %pr_d1%;"fantasy";%TX1%;"[PMS] Pop'n Music - ファンタジー - monde des songe 190927.rar";"ファンタジー - monde des songe";"FANTASY - monde des songe";"kc_0003";"ha_dino_1a";"ha_dino_1b";

SET SongPMS1="ラテン [5button]_104";"ラテン [BATTLE]_117";"ラテン [NORMAL]_117";
SET SongPMS2=" [5button]_104";" [BATTLE]_117";" [NORMAL]_117";
CALL :Process %pr_d1%;"latin";%TR1%;"[PMS] Pop'n Music - ラテン - El pais del sol 190927.rar";"ラテン - El pais del sol";"LATIN - El pais del sol";"kc_0004";"ha_don_1a";"ha_don_1b";

SET SongPMS1="ダンス [5button]_111";"ダンス [BATTLE]_218";"ダンス [HYPER]_224";"ダンス [NORMAL]_121";"ダンス [NORMAL]_224";
SET SongPMS2=" [5button]_111";" [BATTLE]_218";" [HYPER]_224";" [NORMAL]_121";" [NORMAL]_224";
CALL :Process %pr_d1%;"dance";%TR2%;"[PMS] Pop'n Music - ダンス - Hi-Tekno 190927.rar";"ダンス - Hi-Tekno";"DANCE - Hi-Tekno";"kc_0005";"ha_judy_1a";"ha_judy_1b";

SET SongPMS1="テクノポップ [5button]_194";"テクノポップ [BATTLE]_289";"テクノポップ [HYPER]_289";"テクノポップ [NORMAL]_203";
SET SongPMS2=" [5button]_194";" [BATTLE]_289";" [HYPER]_289";" [NORMAL]_203";
CALL :Process %pr_d1%;"technopop";%TR1%;"[PMS] Pop'n Music - テクノポップ - Electronic Fill 190927.rar";"テクノポップ - Electronic Fill";"TECHNO POP - Electronic Fill";"kc_0008";"ha_kraf_1a";"ha_kraf_1b";

SET SongPMS1="アニメヒーロー [5button]_167";"アニメヒーロー [BATTLE]_325";"アニメヒーロー [HYPER]_325";"アニメヒーロー [NORMAL]_177";"アニメヒーロー [NORMAL]_325";
SET SongPMS2=" [5button]_167";" [BATTLE]_325";" [HYPER]_325";" [NORMAL]_177";" [NORMAL]_325";
CALL :Process %pr_d1%;"anime";%TR2%;"[PMS] Pop'n Music - アニメヒーロー - The theme of GAMBLER Z 190927.rar";"アニメヒーロー - The theme of GAMBLER Z";"ANIME HERO - The theme of GAMBLER Z";"kc_0009";"ha_toru_1a";"ha_toru_1b";

SET SongPMS1="スパイ [5button]_174";"スパイ [BATTLE]_351";"スパイ [HYPER]_351";"スパイ [NORMAL]_226";"スパイ [NORMAL]_351";
SET SongPMS2=" [5button]_174";" [BATTLE]_351";" [HYPER]_351";" [NORMAL]_226";" [NORMAL]_351";
CALL :Process %pr_d1%;"spy";%TR2%;"[PMS] Pop'n Music - スパイ - SPICY PIECE 190927.rar";"スパイ - SPICY PIECE";"SPY - SPICY PIECE";"kc_0010";"ha_chrl_1a";"ha_chrl_1b";

SET SongPMS1="レイヴ [5button]_102";"レイヴ [BATTLE]_148";"レイヴ [NORMAL]_136";
SET SongPMS2=" [5button]_102";" [BATTLE]_148";" [NORMAL]_136";
CALL :Process %pr_d1%;"rave";%TR1%;"[PMS] Pop'n Music - レイヴ - e-motion 190927.rar";"レイヴ - e-motion";"RAVE - e-motion";"kc_0012";"ha_rave_1a";"ha_rave_1b";

SET SongPMS1="ボーナストラック [5button]_168";"ボーナストラック [BATTLE]_192";"ボーナストラック [NORMAL]_192";
SET SongPMS2=" [5button]_168";" [BATTLE]_192";" [NORMAL]_192";
CALL :Process %pr_d1%;"bonus";%TR1%;"[PMS] Pop'n Music - ボーナストラック - すれちがう二人 190927.rar";"ボーナストラック - すれちがう二人";"BONUS TRACK - surechigau futari";"kc_0013";"ha_sana_1a";"ha_sana_1b";

SET SongPMS1="ディスコキング [5button]_195";"ディスコキング [BATTLE]_246";"ディスコキング [HYPER]_240";"ディスコキング [NORMAL]_240";
SET SongPMS2=" [5button]_195";" [BATTLE]_246";" [HYPER]_240";" [NORMAL]_240";
CALL :Process %pr_d1%;"king";%TR1%;"[PMS] Pop'n Music - ディスコキング - FUNKY TOWN '75 190927.rar";"ディスコキング - FUNKY TOWN '75";"DISCO KING - FUNKY TOWN 75";false;"ha_bamb_1a";"ha_bamb_1b";

SET SongPMS1="ディスコクィーン [5button]_133";"ディスコクィーン [BATTLE]_187";"ディスコクィーン [NORMAL]_187";
SET SongPMS2=" [5button]_133";" [BATTLE]_187";" [NORMAL]_187";
CALL :Process %pr_d1%;"queen";%TR1%;"[PMS] Pop'n Music - ディスコクィーン - what i want 190927.rar";"ディスコクィーン - what i want";"DISCO QUEEN - what i want";false;"ha_cham_1a";"ha_cham_1b";

SET SongPMS1="ラップ [5button]_95";"ラップ [BATTLE]_102";"ラップ [HYPER]_461";"ラップ [NORMAL]_102";"ラップ [NORMAL]_460";
SET SongPMS2=" [5button]_95";" [BATTLE]_102";" [HYPER]_461";" [NORMAL]_102";" [NORMAL]_460";
CALL :Process %pr_d1%;"rap";%TX1%;"[PMS] Pop'n Music - ラップ - YOUNG DREAM 190927.rar";"ラップ - YOUNG DREAM";"RAP - YOUNG DREAM";false;"ha_jam_1a";"ha_jam_1b";

SET SongPMS1="レゲエ [5button]_114";"レゲエ [BATTLE]_173";"レゲエ [NORMAL]_173";
SET SongPMS2=" [5button]_114";" [BATTLE]_173";" [NORMAL]_173";
CALL :Process %pr_d1%;"reggae";%TR1%;"[PMS] Pop'n Music - レゲエ - Baby，I'm yours 190927.rar";"レゲエ - Baby，I'm yours";"REGGAE - Baby Im yours";false;"ha_oliv_1a";"ha_oliv_1b";

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 2  (AC)
CALL :TitleLookingUp "Pop'n Music 2"
SET curgamef=%f_pnm2ac%

SET SongPMS1="Ｊ－Ｒ＆Ｂ [5button]_84";"Ｊ－Ｒ＆Ｂ [BATTLE]_230";"Ｊ－Ｒ＆Ｂ [NORMAL]_230";
SET SongPMS2=" [5button]_84";" [BATTLE]_230";" [NORMAL]_230";
CALL :Process %pr_d1%;"jrb";%TR1%;"[PMS] Pop'n Music 2 - Ｊ－Ｒ＆Ｂ - SAYONARA 190927.rar";"Ｊ－Ｒ＆Ｂ - SAYONARA";"J-RNB - SAYONARA";"kc_0027";"ha_trum_2a";"ha_trum_2b";

SET SongPMS1="デジロック [5button]_196";"デジロック [BATTLE]_233";"デジロック [NORMAL]_233";
SET SongPMS2=" [5button]_196";" [BATTLE]_233";" [NORMAL]_233";
CALL :Process %pr_d1%;"degirock";%TR1%;"[PMS] Pop'n Music 2 - デジロック - ROSE～恋人よ、ばら色に染まれ～ 190927.rar";"デジロック - ROSE～恋人よ、ばら色に染まれ～";"DIGI ROCK - ROSE Koibito yo barairo ni somare";"kc_0020";"ha_time_2a";"ha_time_2b";

SET SongPMS1="ユーロクィーン [5button]_166";"ユーロクィーン [BATTLE]_206";"ユーロクィーン [NORMAL]_206";
SET SongPMS2=" [5button]_166";" [BATTLE]_206";" [NORMAL]_206";
CALL :Process %pr_d1%;"euro";%TR1%;"[PMS] Pop'n Music 2 - ユーロクィーン - what i want(EURO MIX) 190927.rar";"ユーロクィーン - what i want(EURO MIX)";"EURO QUEEN - what i want(EURO MIX)";false;"ha_koko_2a";"ha_koko_2b";

SET SongPMS1="ラウンジ [5button]_133";"ラウンジ [BATTLE]_161";"ラウンジ [NORMAL]_161";
SET SongPMS2=" [5button]_133";" [BATTLE]_161";" [NORMAL]_161";
CALL :Process %pr_d1%;"lounge";%TR1%;"[PMS] Pop'n Music 2 - ラウンジ - Cherry&Raquel 190927.rar";"ラウンジ - Cherry&Raquel";"LOUNGE - Cherry&Raquel";"kc_0026";"ha_reo_2a";"ha_reo_2b";

SET SongPMS1="メロウ [5button]_98";"メロウ [BATTLE]_130";"メロウ [NORMAL]_130";
SET SongPMS2=" [5button]_98";" [BATTLE]_130";" [NORMAL]_130";
CALL :Process %pr_d1%;"mellow";%TR1%;"[PMS] Pop'n Music 2 - メロウ - 光の季節 190927.rar";"メロウ - 光の季節";"MELLOW - hikari no kisetsu";"kc_0017";"ha_sana_2a";"ha_sana_2b";

SET SongPMS1="ネオアコ [5button]_182";"ネオアコ [BATTLE]_346";"ネオアコ [HYPER]_348";"ネオアコ [NORMAL]_250";
SET SongPMS2=" [5button]_182";" [BATTLE]_346";" [HYPER]_348";" [NORMAL]_250";
CALL :Process %pr_d1%;"neoaco";%TR1%;"[PMS] Pop'n Music 2 - ネオアコ - (fly higher than)the stars 190927.rar";"ネオアコ - (fly higher than)the stars";"NEO ACO - (fly higher than)the stars";"kc_0016";"ha_sugi_2a";"ha_sugi_2b";

SET SongPMS1="へビーメタル [5button]_316";"へビーメタル [BATTLE]_344";"へビーメタル [NORMAL]_350";
SET SongPMS2=" [5button]_316";" [BATTLE]_344";" [NORMAL]_350";
CALL :Process %pr_d1%;"hm";%TR1%;"[PMS] Pop'n Music 2 - へビーメタル - I'm on fire 190927.rar";"へビーメタル - I'm on fire";"HEAVY METAL - I'm on fire";"kc_0025";"ha_dami_2a";"ha_dami_2b";

SET SongPMS1="ポップラップ [5button]_104";"ポップラップ [BATTLE]_143";"ポップラップ [NORMAL]_143";
SET SongPMS2=" [5button]_104";" [BATTLE]_143";" [NORMAL]_143";
CALL :Process %pr_d1%;"poprap";%TR1%;"[PMS] Pop'n Music 2 - ポップラップ - Smile The Night Away 190927.rar";"ポップラップ - Smile The Night Away";"POP RAP - Smile The Night Away";"kc_0023";"ha_kraf_2a";"ha_kraf_2b";

SET SongPMS1="アフリカ [5button]_149";"アフリカ [BATTLE]_363";"アフリカ [HYPER]_414";"アフリカ [NORMAL]_212";
SET SongPMS2=" [5button]_149";" [BATTLE]_363";" [HYPER]_414";" [NORMAL]_212";
CALL :Process %pr_d1%;"africa";%TR1%;"[PMS] Pop'n Music CS - アフリカ - Con te sabi 2119 191027.rar";"アフリカ - Con te sabi 2119";"AFRICA - Con te sabi 2119";"kc_0202";"ha_unbaho_csa";"ha_unbaho_csb";

SET SongPMS1="クラシック [5button]_133";"クラシック [BATTLE]_425";"クラシック [HYPER]_425";"クラシック [NORMAL]_293";"クラシック [NORMAL]_425";"クラシック [x5button]_130";"クラシック [xNORMAL]_130";
SET SongPMS2=" [5button]_133";" [BATTLE]_425";" [HYPER]_425";" [NORMAL]_293";" [NORMAL]_425";" [x5button]_130";" [xNORMAL]_130";
CALL :Process %pr_d1%;"classic";%TR2%;"[PMS] Pop'n Music CS - クラシック - Chaos Age 191027.rar";"クラシック - Chaos Age";"CLASSIC - Chaos Age";false;"ha_hama_csa";"ha_hama_csb";

SET SongPMS1="テクノ’８０ [5button]_150";"テクノ’８０ [BATTLE]_295";"テクノ’８０ [HYPER]_295";"テクノ’８０ [NORMAL]_208";
SET SongPMS2=" [5button]_150";" [BATTLE]_295";" [HYPER]_295";" [NORMAL]_208";
CALL :Process %pr_d1%;"tch80";%TR1%;"[PMS] Pop'n Music CS - テクノ’８０ - Water Melon Woman 191027.rar";"テクノ’８０ - Water Melon Woman";"TECHNO80 - Water Melon Woman";"kc_0203";"ha_boy_csa";"ha_boy_csb";

SET SongPMS1="フュージョン [5button]_127";"フュージョン [BATTLE]_151";"フュージョン [NORMAL]_178";
SET SongPMS2=" [5button]_127";" [BATTLE]_151";" [NORMAL]_178";
CALL :Process %pr_d1%;"fusion";%TR1%;"[PMS] Pop'n Music CS - フュージョン - CROSSOVER 12 191027.rar";"フュージョン - CROSSOVER 12";"FUSION - CROSSOVER 12";false;"ha_syl_csa";"ha_syl_csb";

SET SongPMS1="マサラ [5button]_131";"マサラ [BATTLE]_138";"マサラ [NORMAL]_138";
SET SongPMS2=" [5button]_131";" [BATTLE]_138";" [NORMAL]_138";
CALL :Process %pr_d1%;"masara";%TR1%;"[PMS] Pop'n Music 2 - マサラ - すてきなタブーラ 190927.rar";"マサラ - すてきなタブーラ";"MASARA - sutekina tabla";"kc_0014";"ha_karl_2a";"ha_karl_2b";

SET SongPMS1="エンカ [5button]_148";"エンカ [BATTLE]_277";"エンカ [HYPER]_277";"エンカ [NORMAL]_189";"エンカ [NORMAL]_277";
SET SongPMS2=" [5button]_148";" [BATTLE]_277";" [HYPER]_277";" [NORMAL]_189";" [NORMAL]_277";
CALL :Process %pr_d1%;"enka";%TR2%;"[PMS] Pop'n Music 2 - エンカ - お江戸花吹雪 190927.rar";"エンカ - お江戸花吹雪";"ENKA - Oedo hanafubuki";"kc_0021";"ha_s8ta_2a";"ha_s8ta_2b";

SET SongPMS1="アイドルガール [5button]_130";"アイドルガール [BATTLE]_192";"アイドルガール [HYPER]_412";"アイドルガール [NORMAL]_192";"アイドルガール [NORMAL]_412";
SET SongPMS2=" [5button]_130";" [BATTLE]_192";" [HYPER]_412";" [NORMAL]_192";" [NORMAL]_412";
CALL :Process %pr_d1%;"idolgirl";%TR2%;"[PMS] Pop'n Music 2 - アイドルガール - LOVE FIRE 190927.rar";"アイドルガール - LOVE FIRE";"IDOL GIRL - LOVE FIRE";"kc_0015";"ha_judy_2a";"ha_judy_2b";

SET SongPMS1="ヴィジュアル [5button]_130";"ヴィジュアル [BATTLE]_219";"ヴィジュアル [NORMAL]_219";
SET SongPMS2=" [5button]_130";" [BATTLE]_219";" [NORMAL]_219";
CALL :Process %pr_d1%;"visual";%TR1%;"[PMS] Pop'n Music 2 - ヴィジュアル - WHITE BIRD 190927.rar";"ヴィジュアル - WHITE BIRD";"VISUAL";"kc_0018";"ha_yuli_2a";"ha_yuli_2b";

SET SongPMS1="アニメヒーローＲ [5button]_204";"アニメヒーローＲ [BATTLE]_247";"アニメヒーローＲ [EX]_960";"アニメヒーローＲ [HYPER]_747";"アニメヒーローＲ [NORMAL]_247";"アニメヒーローＲ [NORMAL]_948";
SET SongPMS2=" [5button]_204";" [BATTLE]_247";" [EX]_960";" [HYPER]_747";" [NORMAL]_247";" [NORMAL]_948";
CALL :Process %pr_d1%;"animer";%TR2%;"[PMS] Pop'n Music 2 - アニメヒーローＲ - はばたけ、ザ グレートギャンブラー 190927.rar";"アニメヒーローＲ - はばたけ、ザ・グレートギャンブラー";"ANIME HERO R - Habatake The Great Gambler";false;"ha_hiro_2a";"ha_hiro_2b";

SET SongPMS1="Ｊ－ポップ [5button]_109";"Ｊ－ポップ [BATTLE]_130";"Ｊ－ポップ [HYPER]_323";"Ｊ－ポップ [NORMAL]_201";"Ｊ－ポップ [NORMAL]_323";
SET SongPMS2=" [5button]_109";" [BATTLE]_130";" [HYPER]_323";" [NORMAL]_201";" [NORMAL]_323";
CALL :Process %pr_d1%;"jpop";%TR2%;"[PMS] Pop'n Music CS - Ｊ－ポップ - Life 191027.rar";"Ｊ－ポップ - Life";"JPOP - Life";false;"ha_pret_csa";"ha_pret_csb";

SET SongPMS1="ガーリィ [5button]_140";"ガーリィ [BATTLE]_289";"ガーリィ [NORMAL]_289";
SET SongPMS2=" [5button]_140";" [BATTLE]_289";" [NORMAL]_289";
CALL :Process %pr_d1%;"girly";%TR1%;"[PMS] Pop'n Music 2 - ガーリィ - Love Is Strong To The Sky 190927.rar";"ガーリィ - Love Is Strong To The Sky";"GIRLY - Love Is Strong To The Sky";"kc_0024";"ha_rie_2a";"ha_rie_2b";

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 3  (AC)
CALL :TitleLookingUp "Pop'n Music 3"
SET curgamef=%f_pnm3ac%

SET SongPMS1="Ｊ－Ｒ＆Ｂ２ [5button]_103";"Ｊ－Ｒ＆Ｂ２ [BATTLE]_159";"Ｊ－Ｒ＆Ｂ２ [NORMAL]_159";
SET SongPMS2=" [5button]_103";" [BATTLE]_159";" [NORMAL]_159";
CALL :Process %pr_d1%;"jrb2";%TR1%;"[PMS] Pop'n Music 3 - Ｊ－Ｒ＆Ｂ２ - 愛を探そう 190927.rar";"Ｊ－Ｒ＆Ｂ２ - 愛を探そう";"J-RNB2 - Ai wo sagasou";false;"ha_trum_3a";"ha_trum_3b";

SET SongPMS1="カントン [5button]_166";"カントン [BATTLE]_457";"カントン [HYPER]_457";"カントン [NORMAL]_262";"カントン [NORMAL]_457";
SET SongPMS2=" [5button]_166";" [BATTLE]_457";" [HYPER]_457";" [NORMAL]_262";" [NORMAL]_457";
CALL :Process %pr_d1%;"kangtong";%TR2%;"[PMS] Pop'n Music 3 - カントン - 和称一起走 190927.rar";"カントン - 和称一起走";"KANGTONG - Woneijatheizau";"kc_0031";"ha_ling_3a";"ha_ling_3b";

SET SongPMS1="ギターポップ [5button]_155";"ギターポップ [BATTLE]_201";"ギターポップ [NORMAL]_201";
SET SongPMS2=" [5button]_155";" [BATTLE]_201";" [NORMAL]_201";
CALL :Process %pr_d1%;"g_pop";%TR1%;"[PMS] Pop'n Music 3 - ギターポップ - Take me to… 190927.rar";"ギターポップ - Take me to…";"G-POP - Take me to";"kc_0028";"ha_rie_3a";"ha_rie_3b";

SET SongPMS1="ケルト [5button]_178";"ケルト [BATTLE]_238";"ケルト [NORMAL]_239";
SET SongPMS2=" [5button]_178";" [BATTLE]_238";" [NORMAL]_239";
CALL :Process %pr_d1%;"celt";%TR1%;"[PMS] Pop'n Music 3 - ケルト - 水中家族のテーマ 190927.rar";"ケルト - 水中家族のテーマ";"CELT - Suichuukazoku no theme";"kc_0034";"ha_poet_3a";"ha_poet_3b";

SET SongPMS1="サウンドトラック [5button]_172";"サウンドトラック [BATTLE]_268";"サウンドトラック [EX]_671";"サウンドトラック [HYPER]_506";"サウンドトラック [NORMAL]_270";"サウンドトラック [NORMAL]_671";
SET SongPMS2=" [5button]_172";" [BATTLE]_268";" [EX]_671";" [HYPER]_506";" [NORMAL]_270";" [NORMAL]_671";
CALL :Process %pr_d1%;"sndtrack";%TR2%;"[PMS] Pop'n Music 3 - サウンドトラック - 宇宙船Q-Mex 190927.rar";"サウンドトラック - 宇宙船Q-Mex";"SOUND TRACK - Uchuusen Q-Mex";"kc_0038";"ha_p1p2_3a";"ha_p1p2_3b";

SET SongPMS1="ジャングル [5button]_202";"ジャングル [BATTLE]_343";"ジャングル [NORMAL]_343";
SET SongPMS2=" [5button]_202";" [BATTLE]_343";" [NORMAL]_343";
CALL :Process %pr_d1%;"jungle";%TR1%;"[PMS] Pop'n Music 3 - ジャングル - LA LA LA 190927.rar";"ジャングル - LA LA LA";"JUNGLE - LA LA LA";false;"ha_bamb_3a";"ha_bamb_3b";

SET SongPMS1="ダンスポップ [5button]_219";"ダンスポップ [BATTLE]_296";"ダンスポップ [NORMAL]_296";
SET SongPMS2=" [5button]_219";" [BATTLE]_296";" [NORMAL]_296";
CALL :Process %pr_d1%;"dancepop";%TR1%;"[PMS] Pop'n Music 3 - ダンスポップ - TILL THE END OF TIME 190927.rar";"ダンスポップ - TILL THE END OF TIME";"DANCE POP - TILL THE END OF TIME";false;"ha_rave_3a";"ha_rave_3b";

SET SongPMS1="ハートフル [5button]_94";"ハートフル [BATTLE]_147";"ハートフル [NORMAL]_147";
SET SongPMS2=" [5button]_94";" [BATTLE]_147";" [NORMAL]_147";
CALL :Process %pr_d1%;"heartful";%TR1%;"[PMS] Pop'n Music 3 - ハートフル - peppermint 190927.rar";"ハートフル - peppermint";"HEARTFUL - peppermint";"kc_0033";"ha_cand_3a";"ha_cand_3b";

SET SongPMS1="パンク [5button]_263";"パンク [BATTLE]_307";"パンク [HYPER]_659";"パンク [NORMAL]_307";"パンク [NORMAL]_659";
SET SongPMS2=" [5button]_263";" [BATTLE]_307";" [HYPER]_659";" [NORMAL]_307";" [NORMAL]_659";
CALL :Process %pr_d1%;"punk";%TR2%;"[PMS] Pop'n Music 3 - パンク - LET ME FEEL SO HIGH 190927.rar";"パンク - LET ME FEEL SO HIGH";"PUNK - LET ME FEEL SO HIGH";"kc_0035";"ha_donn_3a";"ha_donn_3b";

SET SongPMS1="マジカルガール [5button]_168";"マジカルガール [BATTLE]_186";"マジカルガール [NORMAL]_186";
SET SongPMS2=" [5button]_168";" [BATTLE]_186";" [NORMAL]_186";
CALL :Process %pr_d1%;"majoanim";%TR1%;"[PMS] Pop'n Music 3 - マジカルガール - 恋のシャレード 190927.rar";"マジカルガール - 恋のシャレード";"MAGICAL GIRL - Koi no charade";"kc_0029";"ha_megu_3a";"ha_megu_3b";

SET SongPMS1="モンド [5button]_146";"モンド [BATTLE]_181";"モンド [NORMAL]_182";
SET SongPMS2=" [5button]_146";" [BATTLE]_181";" [NORMAL]_182";
CALL :Process %pr_d1%;"mondo";%TR1%;"[PMS] Pop'n Music 3 - モンド - Little Robin's Drawer 190927.rar";"モンド - Little Robin's Drawer";"MONDO - Little Robin's Drawer";"kc_0032";"ha_cybe_3a";"ha_cybe_3b";

SET SongPMS1="ユーロダンス [5button]_271";"ユーロダンス [BATTLE]_377";"ユーロダンス [NORMAL]_377";
SET SongPMS2=" [5button]_271";" [BATTLE]_377";" [NORMAL]_377";
CALL :Process %pr_d1%;"eurodance";%TR1%;"[PMS] Pop'n Music 3 - ユーロダンス - Bit of Love 190927.rar";"ユーロダンス - Bit of Love";"EURODANCE - Bit of Love";"kc_0030";"ha_dia_3a";"ha_dia_3b";

SET SongPMS1="ラブリー [5button]_162";"ラブリー [BATTLE]_199";"ラブリー [NORMAL]_199";
SET SongPMS2=" [5button]_162";" [BATTLE]_199";" [NORMAL]_199";
CALL :Process %pr_d1%;"lovely";%TR1%;"[PMS] Pop'n Music 3 - ラブリー - ８月のサヨナラ 190927.rar";"ラブリー - ８月のサヨナラ";"LOVELY - 8gatsu no sayonara";"kc_0040";"ha_sana_3a";"ha_sana_3b";

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 4  (AC)
CALL :TitleLookingUp "Pop'n Music 4"
SET curgamef=%f_pnm4ac%

SET SongPMS1="Ｊ－テクノＲＥＭＩＸ [5button]_151";"Ｊ－テクノＲＥＭＩＸ [BATTLE]_223";"Ｊ－テクノＲＥＭＩＸ [NORMAL]_230";
SET SongPMS2=" [5button]_151";" [BATTLE]_223";" [NORMAL]_230";
CALL :Process %pr_d1%;"x_jtekno";%TR1%;"[PMS] Pop'n Music 4 - Ｊ－テクノＲＥＭＩＸ - Quick Master(Millennium mix) 190929.rar";"Ｊ－テクノＲＥＭＩＸ - Quick Master(Millennium mix)";"J-TEKNO REMIX - Quick Master(Millennium mix)";"kc_0062";false;false;

SET SongPMS1="アナログテクノ [5button]_108";"アナログテクノ [BATTLE]_494";"アナログテクノ [HYPER]_507";"アナログテクノ [NORMAL]_249";"アナログテクノ [NORMAL]_507";
SET SongPMS2=" [5button]_108";" [BATTLE]_494";" [HYPER]_507";" [NORMAL]_249";" [NORMAL]_507";
CALL :Process %pr_d1%;"anatch";%TR2%;"[PMS] Pop'n Music 4 - アナログテクノ - ELECTRONISM 190929.rar";"アナログテクノ - ELECTRONISM";"ANALOG TECHNO - ELECTRONISM";"kc_0052";"ha_pal_4a";"ha_pal_4b";

SET SongPMS1="アンビエント [5button]_96";"アンビエント [BATTLE]_367";"アンビエント [HYPER]_367";"アンビエント [NORMAL]_204";"アンビエント [NORMAL]_367";
SET SongPMS2=" [5button]_96";" [BATTLE]_367";" [HYPER]_367";" [NORMAL]_204";" [NORMAL]_367";
CALL :Process %pr_d1%;"ambient";%TR2%;"[PMS] Pop'n Music 4 - アンビエント - birds 190929.rar";"アンビエント - birds";"AMBIENT - birds";"kc_0050";"ha_sana_4a";"ha_sana_4b";

SET SongPMS1="ヴィジュアル２ [5button]_171";"ヴィジュアル２ [BATTLE]_309";"ヴィジュアル２ [EX]_685";"ヴィジュアル２ [HYPER]_505";"ヴィジュアル２ [NORMAL]_309";"ヴィジュアル２ [NORMAL]_685";
SET SongPMS2=" [5button]_171";" [BATTLE]_309";" [EX]_685";" [HYPER]_505";" [NORMAL]_309";" [NORMAL]_685";
CALL :Process %pr_d1%;"visual2";%TR2%;"[PMS] Pop'n Music 4 - ヴィジュアル２ - Cry Out 190929.rar";"ヴィジュアル２ - Cry Out";"VISUAL2 - Cry Out";"kc_0053";"ha_yuli_4a";"ha_yuli_4b";

SET SongPMS1="オキナワ [5button]_128";"オキナワ [BATTLE]_395";"オキナワ [HYPER]_404";"オキナワ [NORMAL]_207";
SET SongPMS2=" [5button]_128";" [BATTLE]_395";" [HYPER]_404";" [NORMAL]_207";
CALL :Process %pr_d1%;"okinawa";%TR1%;"[PMS] Pop'n Music 4 - オキナワ - 夢添うてぃ 190929.rar";"オキナワ - 夢添うてぃ";"OKINAWA - Yume suruuti";false;false;false;

SET SongPMS1="カートゥーン [5button]_153";"カートゥーン [BATTLE]_526";"カートゥーン [HYPER]_588";"カートゥーン [NORMAL]_282";
SET SongPMS2=" [5button]_153";" [BATTLE]_526";" [HYPER]_588";" [NORMAL]_282";
CALL :Process %pr_d1%;"toon";%TR1%;"[PMS] Pop'n Music 4 - カートゥーン - TOON MANIAC 190929.rar";"カートゥーン - TOON MANIAC";"CARTOON - TOON MANIAC";"kc_0045";"ha_henr_4a";"ha_henr_4b";

SET SongPMS1="カヨウハウス [5button]_178";"カヨウハウス [BATTLE]_248";"カヨウハウス [NORMAL]_270";
SET SongPMS2=" [5button]_178";" [BATTLE]_248";" [NORMAL]_270";
CALL :Process %pr_d1%;"kayohouse";%TR1%;"[PMS] Pop'n Music 4 - カヨウハウス - Nanja-Nai 190929.rar";"カヨウハウス - Nanja-Nai";"KAYOU HOUSE - Nanja-Nai";"kc_0051";"ha_nk2k_4a";"ha_nk2k_4b";

SET SongPMS1="カラオケ [5button]_121";"カラオケ [BATTLE]_140";"カラオケ [HYPER]_312";"カラオケ [NORMAL]_140";
SET SongPMS2=" [5button]_121";" [BATTLE]_140";" [HYPER]_312";" [NORMAL]_140";
CALL :Process %pr_d1%;"karaoke";%TR1%;"[PMS] Pop'n Music 4 - カラオケ - 愛言葉～アイコトバ～ 190929.rar";"カラオケ - 愛言葉～アイコトバ～";"KARAOKE - Ai kotoba";"kc_0048";false;false;

SET SongPMS1="コケティッシュ [5button]_182";"コケティッシュ [BATTLE]_337";"コケティッシュ [HYPER]_384";"コケティッシュ [NORMAL]_245";
SET SongPMS2=" [5button]_182";" [BATTLE]_337";" [HYPER]_384";" [NORMAL]_245";
CALL :Process %pr_d1%;"coquet";%TR1%;"[PMS] Pop'n Music 4 - コケティッシュ - 恋は天然 190929.rar";"コケティッシュ - 恋は天然";"COQUETTISH - Koi wa tennen";"kc_0044";"ha_rie_4a";"ha_rie_4b";

SET SongPMS1="スーパーユーロ [5button]_159";"スーパーユーロ [BATTLE]_281";"スーパーユーロ [NORMAL]_326";
SET SongPMS2=" [5button]_159";" [BATTLE]_281";" [NORMAL]_326";
CALL :Process %pr_d1%;"seuro";%TR1%;"[PMS] Pop'n Music 4 - スーパーユーロ - WE TWO ARE ONE 190929.rar";"スーパーユーロ - WE TWO ARE ONE";"SUPER EURO - WE TWO ARE ONE";false;false;false;

SET SongPMS1="スペシャルエンディング [5button]_257";"スペシャルエンディング [BATTLE]_329";"スペシャルエンディング [HYPER]_484";"スペシャルエンディング [NORMAL]_351";
SET SongPMS2=" [5button]_257";" [BATTLE]_329";" [HYPER]_484";" [NORMAL]_351";
CALL :Process %pr_d1%;"ending";%TR1%;"[PMS] Pop'n Music 4 - スペシャルエンディング - Tap'n! Slap'n! Pop'n Music! 190929.rar";"スペシャルエンディング - Tap'n! Slap'n! Pop'n Music!";"SPECIAL ENDING - Tap'n! Slap'n! Pop'n Music!";"kc_0061";"ha_rave_4a";"ha_rave_4b";

SET SongPMS1="ダンスＲＥＭＩＸ [5button]_143";"ダンスＲＥＭＩＸ [BATTLE]_253";"ダンスＲＥＭＩＸ [NORMAL]_253";
SET SongPMS2=" [5button]_143";" [BATTLE]_253";" [NORMAL]_253";
CALL :Process %pr_d1%;"x_dance";%TR1%;"[PMS] Pop'n Music 4 - ダンスＲＥＭＩＸ - Hi-Tekno(Millennium mix) 190929.rar";"ダンスＲＥＭＩＸ - Hi-Tekno(Millennium mix)";"DANCE REMIX - Hi-Tekno(Millennium mix)";"kc_0063";false;false;

SET SongPMS1="ティーンフォーク [5button]_91";"ティーンフォーク [BATTLE]_145";"ティーンフォーク [NORMAL]_145";
SET SongPMS2=" [5button]_91";" [BATTLE]_145";" [NORMAL]_145";
CALL :Process %pr_d1%;"teen";%TR1%;"[PMS] Pop'n Music 4 - ティーンフォーク - 時代を変えてくTeenage 190929.rar";"ティーンフォーク - 時代を変えてくTeenage";"TEEN FOLK - Jidai wo kaeteku Teenage";false;false;false;

SET SongPMS1="ディスコガールズ [5button]_181";"ディスコガールズ [BATTLE]_242";"ディスコガールズ [HYPER]_507";"ディスコガールズ [NORMAL]_249";"ディスコガールズ [NORMAL]_507";
SET SongPMS2=" [5button]_181";" [BATTLE]_242";" [HYPER]_507";" [NORMAL]_249";" [NORMAL]_507";
CALL :Process %pr_d1%;"discogirls";%TR2%;"[PMS] Pop'n Music 4 - ディスコガールズ - HONEまでトゥナイト 190929.rar";"ディスコガールズ - HONEまでトゥナイト";"DISCO GIRLS - HONE made tonight";"kc_0057";"ha_yuki_4a";"ha_yuki_4b";

SET SongPMS1="ドゥーアップ [5button]_124";"ドゥーアップ [BATTLE]_142";"ドゥーアップ [NORMAL]_152";
SET SongPMS2=" [5button]_124";" [BATTLE]_142";" [NORMAL]_152";
CALL :Process %pr_d1%;"doup";%TR1%;"[PMS] Pop'n Music 4 - ドゥーアップ - Joy! 190929.rar";"ドゥーアップ - Joy!";"DO UP - Joy!";false;false;false;

SET SongPMS1="ナイトアウト [5button]_136";"ナイトアウト [BATTLE]_263";"ナイトアウト [EX]_635";"ナイトアウト [HYPER]_531";"ナイトアウト [NORMAL]_263";"ナイトアウト [NORMAL]_531";
SET SongPMS2=" [5button]_136";" [BATTLE]_263";" [EX]_635";" [HYPER]_531";" [NORMAL]_263";" [NORMAL]_531";
CALL :Process %pr_d1%;"night";%TR2%;"[PMS] Pop'n Music 4 - ナイトアウト - 透明なマニキュア 190929.rar";"ナイトアウト - 透明なマニキュア";"NIGHT OUT - Toumeina manicure";"kc_0043";"ha_judy_4a";"ha_judy_4b";

SET SongPMS1="パッション [5button]_166";"パッション [BATTLE]_384";"パッション [HYPER]_384";"パッション [NORMAL]_191";
SET SongPMS2=" [5button]_166";" [BATTLE]_384";" [HYPER]_384";" [NORMAL]_191";
CALL :Process %pr_d1%;"passion";%TR1%;"[PMS] Pop'n Music 4 - パッション - 今宵Lover's Day 190929.rar";"パッション - 今宵Lover's Day";"PASSION - Koyoi Lover's Day";"kc_0047";"ha_king_4a";"ha_king_4b";

SET SongPMS1="フューチャー [5button]_131";"フューチャー [BATTLE]_289";"フューチャー [EX]_590";"フューチャー [HYPER]_519";"フューチャー [NORMAL]_293";
SET SongPMS2=" [5button]_131";" [BATTLE]_289";" [EX]_590";" [HYPER]_519";" [NORMAL]_293";
CALL :Process %pr_d1%;"future";%TR1%;"[PMS] Pop'n Music 4 - フューチャー - ostin-art 190929.rar";"フューチャー - ostin-art";"FUTURE - ostin-art";"kc_0055";"ha_ice_4a";"ha_ice_4b";

SET SongPMS1="ブリットポップ [5button]_162";"ブリットポップ [BATTLE]_355";"ブリットポップ [NORMAL]_355";
SET SongPMS2=" [5button]_162";" [BATTLE]_355";" [NORMAL]_355";
CALL :Process %pr_d1%;"brit";%TR1%;"[PMS] Pop'n Music 4 - ブリットポップ - Girls Riot 190929.rar";"ブリットポップ - Girls Riot";"BRIT POP - Girls Riot";false;false;false;

SET SongPMS1="フレンドリー [5button]_181";"フレンドリー [BATTLE]_228";"フレンドリー [NORMAL]_228";
SET SongPMS2=" [5button]_181";" [BATTLE]_228";" [NORMAL]_228";
CALL :Process %pr_d1%;"friendly";%TR1%;"[PMS] Pop'n Music 4 - フレンドリー - Over The Rainbow 190929.rar";"フレンドリー - Over The Rainbow";"FRIENDLY - Over The Rainbow";"kc_0049";"ha_poet_4a";"ha_poet_4b";

SET SongPMS1="ボーナストラックＲＥＭＩＸ [5button]_187";"ボーナストラックＲＥＭＩＸ [BATTLE]_422";"ボーナストラックＲＥＭＩＸ [NORMAL]_422";
SET SongPMS2=" [5button]_187";" [BATTLE]_422";" [NORMAL]_422";
CALL :Process %pr_d1%;"x_bonus";%TR1%;"[PMS] Pop'n Music 4 - ボーナストラックＲＥＭＩＸ - すれちがう二人(Millennium mix) 190929.rar";"ボーナストラックＲＥＭＩＸ - すれちがう二人(Millennium mix)";"BONUS TRACK REMIX - Surechigau futari Millennium mix";"kc_0065";false;false;

SET SongPMS1="ポップスＲＥＭＩＸ [5button]_164";"ポップスＲＥＭＩＸ [BATTLE]_283";"ポップスＲＥＭＩＸ [NORMAL]_283";
SET SongPMS2=" [5button]_164";" [BATTLE]_283";" [NORMAL]_283";
CALL :Process %pr_d1%;"x_pops";%TR1%;"[PMS] Pop'n Music 4 - ポップスＲＥＭＩＸ - I REALLY WANT TO HURT YOU～僕らは完璧さ～ 190929.rar";"ポップスＲＥＭＩＸ - I REALLY WANT TO HURT YOU～僕らは完璧さ～";"POPS REMIX - I REALLY WANT TO HURT YOU ~bokura wa kanpeki sa";"kc_0064";false;false;

SET SongPMS1="ホラー [5button]_164";"ホラー [BATTLE]_222";"ホラー [EX]_776";"ホラー [HYPER]_542";"ホラー [NORMAL]_226";"ホラー [NORMAL]_299";
SET SongPMS2=" [5button]_164";" [BATTLE]_222";" [EX]_776";" [HYPER]_542";" [NORMAL]_226";" [NORMAL]_299";
CALL :Process %pr_d1%;"horror";%TR2%;"[PMS] Pop'n Music 4 - ホラー - 妖怪Zの唄 190929.rar";"ホラー - 妖怪Zの唄";"HORROR - Youkai Z no uta";false;false;false;

SET SongPMS1="グルーブロック [5button]_120";"グルーブロック [BATTLE]_152";"グルーブロック [NORMAL]_197";
SET SongPMS2=" [5button]_120";" [BATTLE]_152";" [NORMAL]_197";
CALL :Process %pr_d1%;"groove";%TR1%;"[PMS] Pop'n Music CS - グルーブロック - 赤いリンゴ 191027.rar";"グルーブロック - 赤いリンゴ";"GROOVE ROCK - Akai ringo";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 5  (AC)
CALL :TitleLookingUp "Pop'n Music 5"
SET curgamef=%f_pnm5ac%

SET SongPMS1="Ｊ－テクノ２ [5button]_165";"Ｊ－テクノ２ [BATTLE]_338";"Ｊ－テクノ２ [EX]_876";"Ｊ－テクノ２ [HYPER]_599";"Ｊ－テクノ２ [NORMAL]_338";"Ｊ－テクノ２ [NORMAL]_876";
SET SongPMS2=" [5button]_108";" [BATTLE]_338";" [EX]_876";" [HYPER]_599";" [NORMAL]_338";" [NORMAL]_876";
CALL :Process %pr_d1%;"jtekno2";%TR2%;"[PMS] Pop'n Music 5 - Ｊ－テクノ２ - power plant 191001.rar";"Ｊ－テクノ２ - power plant";"J-TEKNO 2 - power plant";"kc_0071";"ha_shol_3a";"ha_shol_3b";

SET SongPMS1="ソフトロック [5button]_126";"ソフトロック [BATTLE]_241";"ソフトロック [NORMAL]_241";
SET SongPMS2=" [5button]_126";" [BATTLE]_241";" [NORMAL]_241";
CALL :Process %pr_d1%;"softrock";%TR1%;"[PMS] Pop'n Music 5 - ソフトロック - Homesick Pt.2 & 3 191001.rar";"ソフトロック - Homesick Pt.2 & 3";"SOFTROCK - Homesick Pt2_3";"kc_0081";"ha_rie_5a";"ha_rie_5b";

SET SongPMS1="フレンチポップＪ [5button]_122";"フレンチポップＪ [BATTLE]_300";"フレンチポップＪ [NORMAL]_319";
SET SongPMS2=" [5button]_122";" [BATTLE]_300";" [NORMAL]_319";
CALL :Process %pr_d1%;"french_j";%TR1%;"[PMS] Pop'n Music 5 - フレンチポップＪ - une fille dans la pluie 191001.rar";"フレンチポップＪ - une fille dans la pluie";"FRENCH POP J - une fille dans la pluie";false;false;false;

SET SongPMS1="サンバ [5button]_121";"サンバ [BATTLE]_277";"サンバ [NORMAL]_277";
SET SongPMS2=" [5button]_121";" [BATTLE]_277";" [NORMAL]_277";
CALL :Process %pr_d1%;"samba";%TR1%;"[PMS] Pop'n Music 5 - サンバ - Viva! 2000 191001.rar";"サンバ - Viva! 2000";"SAMBA - Viva! 2000";false;false;false;

SET SongPMS1="アニメヒロイン [5button]_170";"アニメヒロイン [BATTLE]_310";"アニメヒロイン [HYPER]_469";"アニメヒロイン [NORMAL]_373";
SET SongPMS2=" [5button]_170";" [BATTLE]_310";" [HYPER]_469";" [NORMAL]_373";
CALL :Process %pr_d1%;"animeh";%TR1%;"[PMS] Pop'n Music 5 - アニメヒロイン - 魔法の扉(スペース＠マコのテーマ) 191001.rar";"アニメヒロイン - 魔法の扉(スペース＠マコのテーマ)";"ANIME HEROINE - Mahou no tobira";false;false;false;

SET SongPMS1="Ｊ－ラップ [5button]_182";"Ｊ－ラップ [BATTLE]_188";"Ｊ－ラップ [HYPER]_386";"Ｊ－ラップ [NORMAL]_188";
SET SongPMS2=" [5button]_182";" [BATTLE]_188";" [HYPER]_386";" [NORMAL]_188";
CALL :Process %pr_d1%;"jrap";%TR1%;"[PMS] Pop'n Music 5 - Ｊ－ラップ - 昇りつめるの 191001.rar";"Ｊ－ラップ - 昇りつめるの";"J-RAP - Nobori tsumeruno";false;false;false;

SET SongPMS1="ＵＳダンスポップ [5button]_129";"ＵＳダンスポップ [BATTLE]_261";"ＵＳダンスポップ [EX]_613";"ＵＳダンスポップ [HYPER]_440";"ＵＳダンスポップ [NORMAL]_261";
SET SongPMS2=" [5button]_129";" [BATTLE]_261";" [EX]_613";" [HYPER]_440";" [NORMAL]_261";
CALL :Process %pr_d1%;"usdance";%TR1%;"[PMS] Pop'n Music 5 - ＵＳダンスポップ - WITHOUT YOU AROUND 191001.rar";"ＵＳダンスポップ - WITHOUT YOU AROUND";"US-DANCE POP - WITHOUT YOU AROUND";false;false;false;

SET SongPMS1="アレグリア [5button]_142";"アレグリア [BATTLE]_169";"アレグリア [EX]_508";"アレグリア [HYPER]_334";"アレグリア [NORMAL]_173";
SET SongPMS2=" [5button]_142";" [BATTLE]_169";" [EX]_508";" [HYPER]_334";" [NORMAL]_173";
CALL :Process %pr_d1%;"allegria";%TR1%;"[PMS] Pop'n Music 5 - アレグリア - クチビル 191001.rar";"アレグリア - クチビル";"ALLEGRIA - Kuchibiru";false;false;false;

SET SongPMS1="レッスン [5button]_242";"レッスン [BATTLE]_453";"レッスン [HYPER]_456";"レッスン [NORMAL]_348";
SET SongPMS2=" [5button]_242";" [BATTLE]_453";" [HYPER]_456";" [NORMAL]_348";
CALL :Process %pr_d1%;"tutorial";%TR1%;"[PMS] Pop'n Music 5 - レッスン - POP-STEP-UP 191001.rar";"レッスン - POP-STEP-UP";"LESSON  - POP-STEP-UP";false;false;false;

SET SongPMS1="エンカＲＥＭＩＸ [5button]_289";"エンカＲＥＭＩＸ [BATTLE]_445";"エンカＲＥＭＩＸ [EX]_759";"エンカＲＥＭＩＸ [HYPER]_667";"エンカＲＥＭＩＸ [NORMAL]_497";
SET SongPMS2=" [5button]_289";" [BATTLE]_445";" [EX]_759";" [HYPER]_667";" [NORMAL]_497";
CALL :Process %pr_d1%;"x_enka";%TR1%;"[PMS] Pop'n Music 5 - エンカＲＥＭＩＸ - お江戸花吹雪～TEYAN-day MIX～ 191001.rar";"エンカＲＥＭＩＸ - お江戸花吹雪～TEYAN-day MIX～";"ENKA REMIX - Oedo hanafubuki TEYAN-day MIX";false;false;false;

SET SongPMS1="ガーリィＲＥＭＩＸ [5button]_178";"ガーリィＲＥＭＩＸ [BATTLE]_337";"ガーリィＲＥＭＩＸ [NORMAL]_337";
SET SongPMS2=" [5button]_178";" [BATTLE]_337";" [NORMAL]_337";
CALL :Process %pr_d1%;"x_girly";%TR1%;"[PMS] Pop'n Music 5 - ガーリィＲＥＭＩＸ - Love Is Strong To The Sky～V.B.-Remaster～ 191001.rar";"ガーリィＲＥＭＩＸ - Love Is Strong To The Sky～V.B.-Remaster～";"GIRLY REMIX - Love Is Strong To The Sky -V.B.-Remaster";false;false;false;

SET SongPMS1="キネマ [5button]_123";"キネマ [BATTLE]_147";"キネマ [HYPER]_375";"キネマ [NORMAL]_182";
SET SongPMS2=" [5button]_123";" [BATTLE]_147";" [HYPER]_375";" [NORMAL]_182";
CALL :Process %pr_d1%;"soundtrack2";%TR1%;"[PMS] Pop'n Music 5 - キネマ - 映画「SICILLIANA」のテーマ 191001.rar";"キネマ - 映画「SICILLIANA」のテーマ";"CINEMA - Eiga SICILLIANA no theme";false;false;false;

SET SongPMS1="コミックソング [5button]_130";"コミックソング [BATTLE]_224";"コミックソング [HYPER]_275";"コミックソング [NORMAL]_254";
SET SongPMS2=" [5button]_130";" [BATTLE]_224";" [HYPER]_275";" [NORMAL]_254";
CALL :Process %pr_d1%;"comic";%TR1%;"[PMS] Pop'n Music 5 - コミックソング - 天麩羅兄弟 191001.rar";"コミックソング - 天麩羅兄弟";"COMIC SONG - Tempura kyoudai";false;false;false;

SET SongPMS1="センタイ [5button]_124";"センタイ [BATTLE]_322";"センタイ [HYPER]_353";"センタイ [NORMAL]_322";
SET SongPMS2=" [5button]_124";" [BATTLE]_322";" [HYPER]_353";" [NORMAL]_322";
CALL :Process %pr_d1%;"sentai";%TR1%;"[PMS] Pop'n Music 5 - センタイ - ビーマニ戦隊ポップンファイブ 191001.rar";"センタイ - ビーマニ戦隊ポップンファイブ";"SENTAI - BEMANI sentai pop'n five";false;false;false;

SET SongPMS1="ニューエイジ [5button]_81";"ニューエイジ [BATTLE]_111";"ニューエイジ [NORMAL]_111";
SET SongPMS2=" [5button]_81";" [BATTLE]_111";" [NORMAL]_111";
CALL :Process %pr_d1%;"newage";%TR1%;"[PMS] Pop'n Music 5 - ニューエイジ - 龍舞～DRAGON DANCE～ 191001.rar";"ニューエイジ - 龍舞～DRAGON DANCE～";"NEW AGE - Ryuubu DRAGON DANCE";false;false;false;

SET SongPMS1="ネオＧＳ [5button]_132";"ネオＧＳ [BATTLE]_275";"ネオＧＳ [HYPER]_408";"ネオＧＳ [NORMAL]_275";
SET SongPMS2=" [5button]_132";" [BATTLE]_275";" [HYPER]_408";" [NORMAL]_275";
CALL :Process %pr_d1%;"neogs";%TR1%;"[PMS] Pop'n Music 5 - ネオＧＳ - Shock of Love 191001.rar";"ネオＧＳ - Shock of Love";"NEO GS - Shock of Love";false;false;false;

SET SongPMS1="ネオアコＲＥＭＩＸ [5button]_164";"ネオアコＲＥＭＩＸ [BATTLE]_342";"ネオアコＲＥＭＩＸ [NORMAL]_345";
SET SongPMS2=" [5button]_164";" [BATTLE]_342";" [NORMAL]_345";
CALL :Process %pr_d1%;"x_neoaco";%TR1%;"[PMS] Pop'n Music 5 - ネオアコＲＥＭＩＸ - fly higher(than the stars)～2 STEP mix～ 191001.rar";"ネオアコＲＥＭＩＸ - fly higher(than the stars)～2 STEP mix～";"NEO ACO REMIX - fly higher(than the stars)-2 STEP mix-";false;false;false;

SET SongPMS1="パーカッシヴ [5button]_143";"パーカッシヴ [BATTLE]_295";"パーカッシヴ [EX]_624";"パーカッシヴ [HYPER]_446";"パーカッシヴ [NORMAL]_182";
SET SongPMS2=" [5button]_143";" [BATTLE]_295";" [EX]_624";" [HYPER]_446";" [NORMAL]_182";
CALL :Process %pr_d1%;"percus";%TR1%;"[PMS] Pop'n Music 5 - パーカッシヴ - 西新宿清掃曲 191001.rar";"パーカッシヴ - 西新宿清掃曲";"PERCUSSIVE - Nishishinjuku seisou kyoku";false;false;false;

SET SongPMS1="ハイテンション [5button]_156";"ハイテンション [BATTLE]_326";"ハイテンション [HYPER]_608";"ハイテンション [NORMAL]_326";
SET SongPMS2=" [5button]_156";" [BATTLE]_326";" [HYPER]_608";" [NORMAL]_326";
CALL :Process %pr_d1%;"hitension";%TR1%;"[PMS] Pop'n Music 5 - ハイテンション - どうなっちゃったって 191001.rar";"ハイテンション - どうなっちゃったって";"HI-TENSION - Dounacchattatte";false;false;false;

SET SongPMS1="パラパラ [5button]_177";"パラパラ [BATTLE]_313";"パラパラ [EX]_493";"パラパラ [HYPER]_459";"パラパラ [NORMAL]_259";
SET SongPMS2=" [5button]_177";" [BATTLE]_313";" [EX]_493";" [HYPER]_459";" [NORMAL]_259";
CALL :Process %pr_d1%;"parapara";%TR1%;"[PMS] Pop'n Music 5 - パラパラ - ウルトラハイヒール～I JUST WANNA TELL YOU～ 191001.rar";"パラパラ - ウルトラハイヒール～I JUST WANNA TELL YOU～";"PARAPARA - ULTRA HIGH HEELS -I JUST WANNA TELL YOU-";false;false;false;

SET SongPMS1="フレンチポップ [5button]_122";"フレンチポップ [BATTLE]_300";"フレンチポップ [NORMAL]_301";
SET SongPMS2=" [5button]_122";" [BATTLE]_300";" [NORMAL]_301";
CALL :Process %pr_d1%;"french_f";%TR1%;"[PMS] Pop'n Music 5 - フレンチポップ - une fille dans la pluie 191001.rar";"フレンチポップ - une fille dans la pluie";"FRENCH POP - une fille dans la pluie";false;false;false;

SET SongPMS1="ヘヴィロック [5button]_175";"ヘヴィロック [BATTLE]_533";"ヘヴィロック [EX]_911";"ヘヴィロック [HYPER]_864";"ヘヴィロック [NORMAL]_490";"ヘヴィロック [NORMAL]_549";
SET SongPMS2=" [5button]_175";" [BATTLE]_533";" [EX]_911";" [HYPER]_864";" [NORMAL]_490";" [NORMAL]_549";
CALL :Process %pr_d1%;"heavy";%TX1%;"[PMS] Pop'n Music 5 - ヘヴィロック - INNOVATION 191001.rar";"ヘヴィロック - INNOVATION";"HEAVY ROCK - INNOVATION";false;false;false;

SET SongPMS1="ポジティブＲＥＭＩＸ [5button]_115";"ポジティブＲＥＭＩＸ [BATTLE]_286";"ポジティブＲＥＭＩＸ [NORMAL]_300";
SET SongPMS2=" [5button]_115";" [BATTLE]_286";" [NORMAL]_300";
CALL :Process %pr_d1%;"x_positive";%TR1%;"[PMS] Pop'n Music 5 - ポジティブＲＥＭＩＸ - Candy Blue～Vocal Best Version～ 191001.rar";"ポジティブＲＥＭＩＸ - Candy Blue～Vocal Best Version～";"POSITIVE REMIX - Candy Blue-Vocal Best Version-";false;false;false;

SET SongPMS1="ミュージカル [5button]_141";"ミュージカル [BATTLE]_280";"ミュージカル [EX]_662";"ミュージカル [HYPER]_455";"ミュージカル [NORMAL]_280";"ミュージカル [NORMAL]_309";
SET SongPMS2=" [5button]_141";" [BATTLE]_280";" [EX]_662";" [HYPER]_455"," [NORMAL]_280";" [NORMAL]_309";
CALL :Process %pr_d1%;"musical";%TR2%;"[PMS] Pop'n Music 5 - ミュージカル - ミュージカル ''5丁目物語'' よりオーヴァチュア 191001.rar";"ミュージカル - ミュージカル ''5丁目物語'' よりオーヴァチュア";"MUSICAL - Musical -5 choume monogatari- yori overture";false;false;false;

SET SongPMS1="モンドポップ [5button]_163";"モンドポップ [BATTLE]_328";"モンドポップ [NORMAL]_328";
SET SongPMS2=" [5button]_163";" [BATTLE]_328";" [NORMAL]_328";
CALL :Process %pr_d1%;"mondopop";%TR1%;"[PMS] Pop'n Music 5 - モンドポップ - BICYCLE 191001.rar";"モンドポップ - BICYCLE";"MONDO POP - BICYCLE";false;false;false;

SET SongPMS1="ヨーデル [5button]_204";"ヨーデル [BATTLE]_349";"ヨーデル [EX]_528";"ヨーデル [HYPER]_492";"ヨーデル [NORMAL]_353";
SET SongPMS2=" [5button]_204";" [BATTLE]_349";" [EX]_528";" [HYPER]_492";" [NORMAL]_353";
CALL :Process %pr_d1%;"yodel";%TR1%;"[PMS] Pop'n Music 5 - ヨーデル - LA LA LA LA YO-DEL 191001.rar";"ヨーデル - LA LA LA LA YO-DEL";"JODLER - LA LA LA LA YO-DEL";false;false;false;

SET SongPMS1="ラテンポップ [5button]_84";"ラテンポップ [BATTLE]_187";"ラテンポップ [EX]_541";"ラテンポップ [HYPER]_409";"ラテンポップ [NORMAL]_187";"ラテンポップ [NORMAL]_510";
SET SongPMS2=" [5button]_84";" [BATTLE]_187";" [EX]_541";" [HYPER]_409";" [NORMAL]_187";" [NORMAL]_510";
CALL :Process %pr_d1%;"latinpop";%TR2%;"[PMS] Pop'n Music 5 - ラテンポップ - In a Grow 191001.rar";"ラテンポップ - In a Grow";"LATIN POP - In a Grow";false;false;false;

SET SongPMS1="ロックオペラ [5button]_108";"ロックオペラ [BATTLE]_249";"ロックオペラ [EX]_666";"ロックオペラ [HYPER]_525";"ロックオペラ [NORMAL]_269";"ロックオペラ [NORMAL]_610";
SET SongPMS2=" [5button]_108";" [BATTLE]_249";" [EX]_666";" [HYPER]_525";" [NORMAL]_269";" [NORMAL]_610";
CALL :Process %pr_d1%;"rockopera";%TR2%;"[PMS] Pop'n Music 5 - ロックオペラ - Ti voglio bene ancora 191001.rar";"ロックオペラ - Ti voglio bene ancora";"ROCK OPERA - Ti voglio bene ancora";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 6  (AC)
CALL :TitleLookingUp "Pop'n Music 6"
SET curgamef=%f_pnm6ac%

SET SongPMS1="Ｋ－ユーロ [5button]_140";"Ｋ－ユーロ [BATTLE]_367";"Ｋ－ユーロ [EX]_686";"Ｋ－ユーロ [HYPER]_582";"Ｋ－ユーロ [NORMAL]_223";
SET SongPMS2=" [5button]_140";" [BATTLE]_367";" [EX]_686";" [HYPER]_582";" [NORMAL]_223";
CALL :Process %pr_d1%;"keuro";%TR1%;"[PMS] Pop'n Music 6 - Ｋ－ユーロ - チャグン　ヘンボク 191003.rar";"Ｋ－ユーロ - チャグン　ヘンボク";"K-EURO - chagun henboku";false;false;false;

SET SongPMS1="オンド [5button]_95";"オンド [BATTLE]_288";"オンド [HYPER]_296";"オンド [NORMAL]_201";
SET SongPMS2=" [5button]_95";" [BATTLE]_288";" [HYPER]_296";" [NORMAL]_201";
CALL :Process %pr_d1%;"ondo";%TR1%;"[PMS] Pop'n Music 6 - オンド - ポップン音頭 191003.rar";"オンド - ポップン音頭";"ONDO - Pop'n Ondo";false;false;false;

SET SongPMS1="ガールズポップ [5button]_114";"ガールズポップ [BATTLE]_404";"ガールズポップ [HYPER]_409";"ガールズポップ [NORMAL]_361";
SET SongPMS2=" [5button]_114";" [BATTLE]_404";" [HYPER]_409";" [NORMAL]_361";
CALL :Process %pr_d1%;"picnic";%TR1%;"[PMS] Pop'n Music 6 - ガールズポップ - Picnic 191003.rar";"ガールズポップ - Picnic";"GIRLS POP - Picnic";false;false;false;

SET SongPMS1="トランス [5button]_170";"トランス [BATTLE]_605";"トランス [HYPER]_629";"トランス [NORMAL]_334";
SET SongPMS2=" [5button]_170";" [BATTLE]_605";" [HYPER]_629";" [NORMAL]_334";
CALL :Process %pr_d1%;"trance";%TR1%;"[PMS] Pop'n Music 6 - トランス - Foundation of our love 191003.rar";"トランス - Foundation of our love";"TRANCE - Foundation of our love";false;false;false;

SET SongPMS1="メロコア [5button]_169";"メロコア [BATTLE]_471";"メロコア [EX]_684";"メロコア [HYPER]_471";"メロコア [NORMAL]_361";
SET SongPMS2=" [5button]_169";" [BATTLE]_471";" [EX]_684";" [HYPER]_471";" [NORMAL]_361";
CALL :Process %pr_d1%;"melocore";%TR1%;"[PMS] Pop'n Music 6 - メロコア - H@ppy Choice 191003.rar";"メロコア - H@ppy Choice";"MELO CORE - H@ppy Choice";false;false;false;

:: TODO: (Desde aqui hacia abajo) Verify folder names on Remywiki (por ejemplo "EVER POP - 2nd ADVENTURE")
SET SongPMS1="エヴァーポップ [5button]_82";"エヴァーポップ [BATTLE]_279";"エヴァーポップ [HYPER]_279";"エヴァーポップ [NORMAL]_166";
SET SongPMS2=" [5button]_82";" [BATTLE]_279";" [HYPER]_279";" [NORMAL]_166";
CALL :Process %pr_d1%;"everpop";%TR1%;"[PMS] Pop'n Music 6 - エヴァーポップ - 2nd ADVENTURE 191003.rar";"エヴァーポップ - 2nd ADVENTURE";"EVER POP - 2nd ADVENTURE";false;false;false;

SET SongPMS1="カントリー [5button]_168";"カントリー [BATTLE]_381";"カントリー [EX]_400";"カントリー [HYPER]_324";"カントリー [NORMAL]_196";"カントリー [NORMAL]_400";
SET SongPMS2=" [5button]_168";" [BATTLE]_381";" [EX]_400";" [HYPER]_324";" [NORMAL]_196";" [NORMAL]_400";
CALL :Process %pr_d1%;"country";%TR2%;"[PMS] Pop'n Music 6 - カントリー - Over the hill 191003.rar";"カントリー - Over the hill";"COUNTRY - Over the hill";false;false;false;

SET SongPMS1="ジャズボーカル [5button]_131";"ジャズボーカル [BATTLE]_331";"ジャズボーカル [HYPER]_331";"ジャズボーカル [NORMAL]_160";
SET SongPMS2=" [5button]_131";" [BATTLE]_331";" [HYPER]_331";" [NORMAL]_160";
CALL :Process %pr_d1%;"jazz";%TR1%;"[PMS] Pop'n Music 6 - ジャズボーカル - Lucifer 191003.rar";"ジャズボーカル - Lucifer";"JAZZ VOCAL - Lucifer";false;false;false;

SET SongPMS1="スカ [5button]_206";"スカ [BATTLE]_609";"スカ [HYPER]_618";"スカ [NORMAL]_320";"スカ [NORMAL]_618";
SET SongPMS2=" [5button]_206";" [BATTLE]_609";" [HYPER]_618";" [NORMAL]_320";" [NORMAL]_618";
CALL :Process %pr_d1%;"ska";%TR2%;"[PMS] Pop'n Music 6 - スカ - Ska Ska No.3 191003.rar";"スカ - Ska Ska No.3";"SKA - Ska Ska No.3";false;false;false;

SET SongPMS1="スコティッシュ [5button]_175";"スコティッシュ [BATTLE]_324";"スコティッシュ [HYPER]_370";"スコティッシュ [NORMAL]_272";
SET SongPMS2=" [5button]_175";" [BATTLE]_324";" [HYPER]_370";" [NORMAL]_272";
CALL :Process %pr_d1%;"scottish";%TR1%;"[PMS] Pop'n Music 6 - スコティッシュ - Bubble Bagpipe Hour 191003.rar";"スコティッシュ - Bubble Bagpipe Hour";"SCOTTISH - Bubble Bagpipe Hour";false;false;false;

SET SongPMS1="ソウル [5button]_87";"ソウル [BATTLE]_372";"ソウル [HYPER]_548";"ソウル [NORMAL]_373";
SET SongPMS2=" [5button]_87";" [BATTLE]_372";" [HYPER]_548";" [NORMAL]_373";
CALL :Process %pr_d1%;"soul";%TR1%;"[PMS] Pop'n Music 6 - ソウル - Gotta Get My Groove On 191003.rar";"ソウル - Gotta Get My Groove On";"SOUL - Gotta Get My Groove On";false;false;false;

SET SongPMS1="テクノカヨウ [5button]_110";"テクノカヨウ [BATTLE]_351";"テクノカヨウ [HYPER]_352";"テクノカヨウ [NORMAL]_240";
SET SongPMS2=" [5button]_110";" [BATTLE]_351";" [HYPER]_352";" [NORMAL]_240";
CALL :Process %pr_d1%;"techno";%TR1%;"[PMS] Pop'n Music 6 - テクノカヨウ - うるとら★ボーイ 191003.rar";"テクノカヨウ - うるとら★ボーイ";"TECHNO KAYOU - Ultra Boy";false;false;false;

SET SongPMS1="デジポップ [5button]_248";"デジポップ [BATTLE]_479";"デジポップ [EX]_629";"デジポップ [HYPER]_516";"デジポップ [NORMAL]_324";
SET SongPMS2=" [5button]_248";" [BATTLE]_479";" [EX]_629";" [HYPER]_516";" [NORMAL]_324";
CALL :Process %pr_d1%;"digipop";%TR1%;"[PMS] Pop'n Music 6 - デジポップ - 走りぬける君へ、Hurry up! 191003.rar";"デジポップ - 走りぬける君へ、Hurry up!";"DIGI POP - hashiri nukeru kimi he- Hurry Up-";false;false;false;

SET SongPMS1="ニューロマンティック [5button]_180";"ニューロマンティック [BATTLE]_379";"ニューロマンティック [HYPER]_461";"ニューロマンティック [NORMAL]_261";
SET SongPMS2=" [5button]_180";" [BATTLE]_379";" [HYPER]_461";" [NORMAL]_261";
CALL :Process %pr_d1%;"newroman";%TR1%;"[PMS] Pop'n Music 6 - ニューロマンティック - Spell on me～魅惑の呪文～ 191003.rar";"ニューロマンティック - Spell on me～魅惑の呪文～";"NEW ROMANTIC - Spell on me -khedice-";false;false;false;

SET SongPMS1="ヒップロック [5button]_253";"ヒップロック [BATTLE]_526";"ヒップロック [HYPER]_690";"ヒップロック [NORMAL]_545";
SET SongPMS2=" [5button]_253";" [BATTLE]_526";" [HYPER]_690";" [NORMAL]_545";
CALL :Process %pr_d1%;"hiprock";%TR1%;"[PMS] Pop'n Music 6 - ヒップロック - 大見解 191003.rar";"ヒップロック - 大見解";"HIP ROCK - Daikenkai";false;false;false;

SET SongPMS1="ブルース [5button]_98";"ブルース [BATTLE]_381";"ブルース [HYPER]_383";"ブルース [NORMAL]_209";
SET SongPMS2=" [5button]_98";" [BATTLE]_381";" [HYPER]_383";" [NORMAL]_209";
CALL :Process %pr_d1%;"g_blues";%TR1%;"[PMS] Pop'n Music 6 - ブルース - 悪の野望はブルース 191003.rar";"ブルース - 悪の野望はブルース";"BLUES - khedice";false;false;false;

SET SongPMS1="ポップビーツ [5button]_161";"ポップビーツ [BATTLE]_356";"ポップビーツ [EX]_448";"ポップビーツ [HYPER]_356";"ポップビーツ [NORMAL]_247";
SET SongPMS2=" [5button]_161";" [BATTLE]_356";" [EX]_448";" [HYPER]_356";" [NORMAL]_247";
CALL :Process %pr_d1%;"popbeats";%TR1%;"[PMS] Pop'n Music 6 - ポップビーツ - Sunlight 191003.rar";"ポップビーツ - Sunlight";"POP BEATS - Sunlight";false;false;false;

SET SongPMS1="マーチング [5button]_120";"マーチング [BATTLE]_419";"マーチング [EX]_572";"マーチング [HYPER]_419";"マーチング [NORMAL]_266";
SET SongPMS2=" [5button]_120";" [BATTLE]_419";" [EX]_572";" [HYPER]_419";" [NORMAL]_266";
CALL :Process %pr_d1%;"march";%TR1%;"[PMS] Pop'n Music 6 - マーチング - マシュマロ マーチ 191003.rar";"マーチング - マシュマロ・マーチ";"MARCHING - machimaro march khedice";false;false;false;

SET SongPMS1="ムーディー [5button]_95";"ムーディー [BATTLE]_412";"ムーディー [EX]_481";"ムーディー [HYPER]_420";"ムーディー [NORMAL]_229";
SET SongPMS2=" [5button]_95";" [BATTLE]_412";" [EX]_481";" [HYPER]_420";" [NORMAL]_229";
CALL :Process %pr_d1%;"moody";%TR1%;"[PMS] Pop'n Music 6 - ムーディー - Midnight Yoghurt（女SPY☆お色気ゴーゴー脱出大作戦） 191003.rar";"ムーディー - Midnight Yoghurt（女SPY☆お色気ゴーゴー脱出大作戦）";"MOODY - Midnight Yoghurt -onna SPY khedice";false;false;false;

SET SongPMS1="モダニズム [5button]_140";"モダニズム [BATTLE]_428";"モダニズム [HYPER]_428";"モダニズム [NORMAL]_284";
SET SongPMS2=" [5button]_140";" [BATTLE]_428";" [HYPER]_428";" [NORMAL]_284";
CALL :Process %pr_d1%;"roman";%TR1%;"[PMS] Pop'n Music 6 - モダニズム - 電波の暮らし 191003.rar";"モダニズム - 電波の暮らし";"MODERNISM - denpa no kurashi";false;false;false;

SET SongPMS1="ライトフュージョン [5button]_173";"ライトフュージョン [BATTLE]_331";"ライトフュージョン [EX]_481";"ライトフュージョン [HYPER]_381";"ライトフュージョン [NORMAL]_273";
SET SongPMS2=" [5button]_173";" [BATTLE]_331";" [EX]_481";" [HYPER]_381";" [NORMAL]_273";
CALL :Process %pr_d1%;"lightfusion";%TR1%;"[PMS] Pop'n Music 6 - ライトフュージョン - micro dream 191003.rar";"ライトフュージョン - micro dream";"LIGHT FUSION - micro dream";false;false;false;

SET SongPMS1="ロックンロール [5button]_175";"ロックンロール [BATTLE]_368";"ロックンロール [HYPER]_423";"ロックンロール [NORMAL]_195";
SET SongPMS2=" [5button]_175";" [BATTLE]_368";" [HYPER]_423";" [NORMAL]_195";
CALL :Process %pr_d1%;"rocknroll";%TR1%;"[PMS] Pop'n Music 6 - ロックンロール - I am Rock'n'roll King 191003.rar";"ロックンロール - I am Rock'n'roll King";"ROCK N ROLL - I am Rock-n-roll King";false;false;false;

:: =============================================================================================================================
:: Pop'n Music 7 (AC)
CALL :TitleLookingUp "Pop'n Music 7"
SET curgamef=%f_pnm7ac%

:: TODO: verificar nombres de carpeta desde aqui hacia abajo (ejemplo "Jr RnB -SSL Super Special Rap-")
SET SongPMS1="Ｊｒ．Ｒ＆Ｂ [5button]_159";"Ｊｒ．Ｒ＆Ｂ [BATTLE]_345";"Ｊｒ．Ｒ＆Ｂ [HYPER]_453";"Ｊｒ．Ｒ＆Ｂ [NORMAL]_233";
SET SongPMS2=" [5button]_159";" [BATTLE]_345";" [HYPER]_453";" [NORMAL]_233";
CALL :Process %pr_d1%;"j_rb";%TR1%;"[PMS] Pop'n Music 7 - Ｊｒ．Ｒ＆Ｂ - S S L～スーパー スペシャル ラブ～ 191005.rar";"Ｊｒ．Ｒ＆Ｂ - S・S・L～スーパー・スペシャル・ラブ～";"Jr RnB -SSL Super Special Rap-";false;false;false;

SET SongPMS1="Ｊ－オルタナ [5button]_139";"Ｊ－オルタナ [BATTLE]_568";"Ｊ－オルタナ [HYPER]_688";"Ｊ－オルタナ [NORMAL]_327";
SET SongPMS2=" [5button]_139";" [BATTLE]_568";" [HYPER]_688";" [NORMAL]_327";
CALL :Process %pr_d1%;"jalt";%TR1%;"[PMS] Pop'n Music 7 - Ｊ－オルタナ - 簡単な事だけど大切なもの 191005.rar";"Ｊ－オルタナ - 簡単な事だけど大切なもの";"J Alterna - khedice";false;false;false;

SET SongPMS1="Ｊ－ガラージポップＲＥＭＩＸ [5button]_176";"Ｊ－ガラージポップＲＥＭＩＸ [BATTLE]_400";"Ｊ－ガラージポップＲＥＭＩＸ [HYPER]_575";"Ｊ－ガラージポップＲＥＭＩＸ [NORMAL]_371";
SET SongPMS2=" [5button]_176";" [BATTLE]_400";" [HYPER]_575";" [NORMAL]_371";
CALL :Process %pr_d1%;"x_jgarage";%TR1%;"[PMS] Pop'n Music 7 - Ｊ－ガラージポップＲＥＭＩＸ - Miracle Moon～L.E.D.LIGHT STYLE MIX～ 191005.rar";"Ｊ－ガラージポップＲＥＭＩＸ - Miracle Moon～L.E.D.LIGHT STYLE MIX～";"J GARAGE POP REMIX - Miracle Moon-L.E.D.LIGHT STYLE MIX-";false;false;false;

SET SongPMS1="アンセム [5button]_129";"アンセム [BATTLE]_316";"アンセム [HYPER]_365";"アンセム [NORMAL]_154";"アンセム [NORMAL]_365";
SET SongPMS2=" [5button]_129";" [BATTLE]_316";" [HYPER]_365";" [NORMAL]_154";" [NORMAL]_365";
CALL :Process %pr_d1%;"gloria";%TR2%;"[PMS] Pop'n Music 7 - アンセム - GLORIA 191005.rar";"アンセム - GLORIA";"ANTHEM - GLORIA";false;false;false;

SET SongPMS1="ヴィジュアル２ＲＥＭＩＸ [5button]_185";"ヴィジュアル２ＲＥＭＩＸ [BATTLE]_429";"ヴィジュアル２ＲＥＭＩＸ [HYPER]_470";"ヴィジュアル２ＲＥＭＩＸ [NORMAL]_289";
SET SongPMS2=" [5button]_185";" [BATTLE]_429";" [HYPER]_470";" [NORMAL]_289";
CALL :Process %pr_d1%;"x_visual2";%TR1%;"[PMS] Pop'n Music 7 - ヴィジュアル２ＲＥＭＩＸ - Cry Out(Superior Mix) 191005.rar";"ヴィジュアル２ＲＥＭＩＸ - Cry Out(Superior Mix)";"VISUAL2REMIX - Cry Out(Superior Mix)";false;false;false;

SET SongPMS1="ウインターポップ [5button]_150";"ウインターポップ [BATTLE]_310";"ウインターポップ [HYPER]_446";"ウインターポップ [NORMAL]_250";
SET SongPMS2=" [5button]_150";" [BATTLE]_310";" [HYPER]_446";" [NORMAL]_250";
CALL :Process %pr_d1%;"winter_pop";%TR1%;"[PMS] Pop'n Music 7 - ウインターポップ - White Lovers 191005.rar";"ウインターポップ - White Lovers";"WINTER POP - White Lovers";false;false;false;

SET SongPMS1="キョウゲキ [5button]_174";"キョウゲキ [BATTLE]_373";"キョウゲキ [EX]_693";"キョウゲキ [HYPER]_622";"キョウゲキ [NORMAL]_334";
SET SongPMS2=" [5button]_174";" [BATTLE]_373";" [EX]_693";" [HYPER]_622";" [NORMAL]_334";
CALL :Process %pr_d1%;"kyogeki";%TR1%;"[PMS] Pop'n Music 7 - キョウゲキ - 加油！元気猿！ 191005.rar";"キョウゲキ - 加油！元気猿！";"KYOUGEKI - khedice";false;false;false;

SET SongPMS1="クラブジャズ [5button]_178";"クラブジャズ [BATTLE]_406";"クラブジャズ [HYPER]_552";"クラブジャズ [NORMAL]_272";
SET SongPMS2=" [5button]_178";" [BATTLE]_406";" [HYPER]_552";" [NORMAL]_272";
CALL :Process %pr_d1%;"atick";%TR1%;"[PMS] Pop'n Music 7 - クラブジャズ - Betty Boo 191005.rar";"クラブジャズ - Betty Boo";"KHEDICE - Betty Boo";false;false;false;

SET SongPMS1="グラムロック [5button]_146";"グラムロック [BATTLE]_369";"グラムロック [EX]_811";"グラムロック [HYPER]_697";"グラムロック [NORMAL]_300";
SET SongPMS2=" [5button]_146";" [BATTLE]_369";" [EX]_811";" [HYPER]_697";" [NORMAL]_300";
CALL :Process %pr_d1%;"gramrock";%TR1%;"[PMS] Pop'n Music 7 - グラムロック - スペースキッス 191005.rar";"グラムロック - スペースキッス";"GRAM ROCK - Space Kiss khedice";false;false;false;

SET SongPMS1="クレイジーテクノ [5button]_182";"クレイジーテクノ [BATTLE]_441";"クレイジーテクノ [EX]_805";"クレイジーテクノ [HYPER]_573";"クレイジーテクノ [NORMAL]_327";
SET SongPMS2=" [5button]_182";" [BATTLE]_441";" [EX]_805";" [HYPER]_573";" [NORMAL]_327";
CALL :Process %pr_d1%;"polysics";%TR1%;"[PMS] Pop'n Music 7 - クレイジーテクノ - CHICKEN CHASER 191005.rar";"クレイジーテクノ - CHICKEN CHASER";"CRAZY TECHNO KHEDICE - CHICKEN CHASER";false;false;false;

SET SongPMS1="サーカス [5button]_191";"サーカス [BATTLE]_327";"サーカス [EX]_1008";"サーカス [HYPER]_406";"サーカス [NORMAL]_278";"サーカス [NORMAL]_406";"サーカス [NORMAL]_1008";
SET SongPMS2=" [5button]_191";" [BATTLE]_327";" [EX]_1008";" [HYPER]_406";" [NORMAL]_278";" [NORMAL]_406";" [NORMAL]_1008";
CALL :Process %pr_d1%;"circus";%TR3%;"[PMS] Pop'n Music 7 - サーカス - フリーパス 191005.rar";"サーカス - フリーパス";"CIRCUS - FREE PASS khedice";false;false;false;

SET SongPMS1="サーフロック [5button]_191";"サーフロック [BATTLE]_464";"サーフロック [HYPER]_770";"サーフロック [NORMAL]_468";
SET SongPMS2=" [5button]_191";" [BATTLE]_464";" [HYPER]_770";" [NORMAL]_468";
CALL :Process %pr_d1%;"surfrock";%TR1%;"[PMS] Pop'n Music 7 - サーフロック - 純愛サレンダー 191005.rar";"サーフロック - 純愛サレンダー";"SURF ROCK - khedice";false;false;false;

SET SongPMS1="サニー [5button]_219";"サニー [BATTLE]_542";"サニー [HYPER]_592";"サニー [NORMAL]_247";
SET SongPMS2=" [5button]_219";" [BATTLE]_542";" [HYPER]_592";" [NORMAL]_247";
CALL :Process %pr_d1%;"parquet";%TR1%;"[PMS] Pop'n Music 7 - サニー - HAPPY MUSIC 191005.rar";"サニー - HAPPY MUSIC";"SUNNY - HAPPY MUSIC";false;false;false;

SET SongPMS1="スウェディッシュ [5button]_258";"スウェディッシュ [BATTLE]_428";"スウェディッシュ [HYPER]_708";"スウェディッシュ [NORMAL]_413";
SET SongPMS2=" [5button]_258";" [BATTLE]_428";" [HYPER]_708";" [NORMAL]_413";
CALL :Process %pr_d1%;"swedish";%TR1%;"[PMS] Pop'n Music 7 - スウェディッシュ - カモミール バスルーム 191005.rar";"スウェディッシュ - カモミール・バスルーム";"SWEDISH - Camomile Bathroom";false;false;false;

SET SongPMS1="スペシャルエンディング２ [5button]_208";"スペシャルエンディング２ [BATTLE]_491";"スペシャルエンディング２ [HYPER]_718";"スペシャルエンディング２ [NORMAL]_626";
SET SongPMS2=" [5button]_208";" [BATTLE]_491";" [HYPER]_718";" [NORMAL]_626";
CALL :Process %pr_d1%;"ending2";%TR1%;"[PMS] Pop'n Music 7 - スペシャルエンディング２ - Ending theme of pop'n music 6 191005.rar";"スペシャルエンディング２ - Ending theme of pop'n music 6";"SPECIAL ENDING - Ending theme of pop'n music 6";false;false;false;

SET SongPMS1="ソフトロックＬＯＮＧ [5button]_188";"ソフトロックＬＯＮＧ [BATTLE]_689";"ソフトロックＬＯＮＧ [HYPER]_735";"ソフトロックＬＯＮＧ [NORMAL]_456";
SET SongPMS2=" [5button]_188";" [BATTLE]_689";" [HYPER]_735";" [NORMAL]_456";
CALL :Process %pr_d1%;"x_softrock";%TR1%;"[PMS] Pop'n Music 7 - ソフトロックＬＯＮＧ - Homesick Pt.2 & 3 191005.rar";"ソフトロックＬＯＮＧ - Homesick Pt.2 & 3";"SOFTROCK LONG - Homesick Pt 2 - 3";false;false;false;

SET SongPMS1="ダークネス [5button]_128";"ダークネス [BATTLE]_540";"ダークネス [EX]_921";"ダークネス [HYPER]_556";"ダークネス [NORMAL]_226";
SET SongPMS2=" [5button]_128";" [BATTLE]_540";" [EX]_921";" [HYPER]_556";" [NORMAL]_226";
CALL :Process %pr_d1%;"dkpop";%TR1%;"[PMS] Pop'n Music 7 - ダークネス - 電気人形 191005.rar";"ダークネス - 電気人形";"DARKNESS - Denki ningyo khedice";false;false;false;

SET SongPMS1="チアガール [5button]_147";"チアガール [BATTLE]_359";"チアガール [HYPER]_414";"チアガール [NORMAL]_258";
SET SongPMS2=" [5button]_147";" [BATTLE]_359";" [HYPER]_414";" [NORMAL]_258";
CALL :Process %pr_d1%;"cheer";%TR1%;"[PMS] Pop'n Music 7 - チアガール - GET THE CHANCE！ 191005.rar";"チアガール - GET THE CHANCE！";"CHEER GIRL - GET THE CHANCE!";false;false;false;

SET SongPMS1="ディスコハウス [5button]_160";"ディスコハウス [BATTLE]_480";"ディスコハウス [HYPER]_604";"ディスコハウス [NORMAL]_303";
SET SongPMS2=" [5button]_160";" [BATTLE]_480";" [HYPER]_604";" [NORMAL]_303";
CALL :Process %pr_d1%;"discohouse";%TR1%;"[PMS] Pop'n Music 7 - ディスコハウス - ディスコタイフーン 191005.rar";"ディスコハウス - ディスコタイフーン";"DISCO HOUSE - Disco Typhoon khedice";false;false;false;

SET SongPMS1="デスレゲエ [5button]_238";"デスレゲエ [BATTLE]_499";"デスレゲエ [EX]_908";"デスレゲエ [HYPER]_795";"デスレゲエ [NORMAL]_540";
SET SongPMS2=" [5button]_238";" [BATTLE]_499";" [EX]_908";" [HYPER]_795";" [NORMAL]_540";
CALL :Process %pr_d1%;"des_reggae";%TR1%;"[PMS] Pop'n Music 7 - デスレゲエ - 夜間行 191005.rar";"デスレゲエ - 夜間行";"DES-REGGAE - Yakankou";false;false;false;

SET SongPMS1="ナイトアウトＲＥＭＩＸ [5button]_114";"ナイトアウトＲＥＭＩＸ [BATTLE]_482";"ナイトアウトＲＥＭＩＸ [HYPER]_482";"ナイトアウトＲＥＭＩＸ [NORMAL]_327";
SET SongPMS2=" [5button]_114";" [BATTLE]_482";" [HYPER]_482";" [NORMAL]_327";
CALL :Process %pr_d1%;"x_nightout";%TR1%;"[PMS] Pop'n Music 7 - ナイトアウトＲＥＭＩＸ - 透明なマニキュア(moonlit mix) 191005.rar";"ナイトアウトＲＥＭＩＸ - 透明なマニキュア(moonlit mix)";"NIGHT OUT REMIX - Toumeina manicure(moonlit mix)";false;false;false;

SET SongPMS1="ニューエイジＲＥＭＩＸ [5button]_115";"ニューエイジＲＥＭＩＸ [BATTLE]_258";"ニューエイジＲＥＭＩＸ [HYPER]_599";"ニューエイジＲＥＭＩＸ [NORMAL]_179";
SET SongPMS2=" [5button]_115";" [BATTLE]_258";" [HYPER]_599";" [NORMAL]_179";
CALL :Process %pr_d1%;"x_newage";%TR1%;"[PMS] Pop'n Music 7 - ニューエイジＲＥＭＩＸ - 龍舞～DRAGON DANCE Revision-2～ 191005.rar";"ニューエイジＲＥＭＩＸ - 龍舞～DRAGON DANCE Revision-2～";"NEW AGE REMIX - ~ khedice DRAGON DANCE Revision-2~";false;false;false;

SET SongPMS1="ハードロック [5button]_140";"ハードロック [BATTLE]_289";"ハードロック [EX]_791";"ハードロック [HYPER]_543";"ハードロック [NORMAL]_289";
SET SongPMS2=" [5button]_140";" [BATTLE]_289";" [EX]_791";" [HYPER]_543";" [NORMAL]_289";
CALL :Process %pr_d1%;"sadame";%TR1%;"[PMS] Pop'n Music 7 - ハードロック - SA-DA-ME 191005.rar";"ハードロック - SA-DA-ME";"HARD ROCK - SA-DA-ME";false;false;false;

SET SongPMS1="バリトランス [5button]_114";"バリトランス [BATTLE]_381";"バリトランス [EX]_725";"バリトランス [HYPER]_560";"バリトランス [NORMAL]_254";
SET SongPMS2=" [5button]_114";" [BATTLE]_381";" [EX]_725";" [HYPER]_560";" [NORMAL]_254";
CALL :Process %pr_d1%;"bali";%TR1%;"[PMS] Pop'n Music 7 - バリトランス - Denpasar 191005.rar";"バリトランス - Denpasar";"BALLETE DANCE KHEDICE - Denpasar";false;false;false;

SET SongPMS1="バロック [5button]_129";"バロック [BATTLE]_353";"バロック [HYPER]_356";"バロック [NORMAL]_247";"バロック [NORMAL]_356";
SET SongPMS2=" [5button]_129";" [BATTLE]_353";" [HYPER]_356";" [NORMAL]_247";" [NORMAL]_356";
CALL :Process %pr_d1%;"progressive";%TR2%;"[PMS] Pop'n Music 7 - バロック - EXCALIBUR 191005.rar";"バロック - EXCALIBUR";"BAROCK KHEDICE - EXCALIBUR";false;false;false;

SET SongPMS1="プレシャス [5button]_168";"プレシャス [BATTLE]_278";"プレシャス [HYPER]_362";"プレシャス [NORMAL]_180";
SET SongPMS2=" [5button]_168";" [BATTLE]_278";" [HYPER]_362";" [NORMAL]_180";
CALL :Process %pr_d1%;"precious";%TR1%;"[PMS] Pop'n Music 7 - プレシャス - スウィーツ 191005.rar";"プレシャス - スウィーツ";"PRECIOUS - khedice";false;false;false;

SET SongPMS1="ポップフュージョン [5button]_132";"ポップフュージョン [BATTLE]_407";"ポップフュージョン [HYPER]_428";"ポップフュージョン [NORMAL]_262";
SET SongPMS2=" [5button]_132";" [BATTLE]_407";" [HYPER]_428";" [NORMAL]_262";
CALL :Process %pr_d1%;"popfusion";%TR1%;"[PMS] Pop'n Music 7 - ポップフュージョン - Tropical Pallete 191005.rar";"ポップフュージョン - Tropical Pallete";"POP FUSION khedice - Tropical Pallete";false;false;false;

SET SongPMS1="ミスティ [5button]_163";"ミスティ [BATTLE]_476";"ミスティ [EX]_848";"ミスティ [HYPER]_616";"ミスティ [NORMAL]_233";"ミスティ [NORMAL]_616";
SET SongPMS2=" [5button]_163";" [BATTLE]_476";" [EX]_848";" [HYPER]_616";" [NORMAL]_233";" [NORMAL]_616";
CALL :Process %pr_d1%;"misty";%TR2%;"[PMS] Pop'n Music 7 - ミスティ - platonic love 191005.rar";"ミスティ - platonic love";"MISTY - platonic love";false;false;false;

SET SongPMS1="ラウンジポップ [5button]_120";"ラウンジポップ [BATTLE]_346";"ラウンジポップ [HYPER]_346";"ラウンジポップ [NORMAL]_226";
SET SongPMS2=" [5button]_120";" [BATTLE]_346";" [HYPER]_346";" [NORMAL]_226";
CALL :Process %pr_d1%;"orange";%TR1%;"[PMS] Pop'n Music 7 - ラウンジポップ - Dimanche 191005.rar";"ラウンジポップ - Dimanche";"LOUNGE POP - Dimanche khedice";false;false;false;

SET SongPMS1="レトロフューチャー [5button]_140";"レトロフューチャー [BATTLE]_384";"レトロフューチャー [HYPER]_444";"レトロフューチャー [NORMAL]_186";
SET SongPMS2=" [5button]_140";" [BATTLE]_384";" [HYPER]_444";" [NORMAL]_186";
CALL :Process %pr_d1%;"pas";%TR1%;"[PMS] Pop'n Music 7 - レトロフューチャー - Passacaglia 191005.rar";"レトロフューチャー - Passacaglia";"RETRO FUTURE khedice - Passacaglia";false;false;false;

SET SongPMS1="昭和歌謡 [5button]_297";"昭和歌謡 [BATTLE]_483";"昭和歌謡 [HYPER]_669";"昭和歌謡 [NORMAL]_358";
SET SongPMS2=" [5button]_297";" [BATTLE]_483";" [HYPER]_669";" [NORMAL]_358";
CALL :Process %pr_d1%;"showakayo";%TR1%;"[PMS] Pop'n Music 7 - 昭和歌謡 - 林檎と蜂蜜 191005.rar";"昭和歌謡 - 林檎と蜂蜜";"SHOWA KAYO - khedice";false;false;false;
:: (SHOWA KAYO) Nota: Este simfile trae un txt con un comentario.
SET SongPMS1="ウルセイ [5button]_194";"ウルセイ [BATTLE]_405";"ウルセイ [HYPER]_502";"ウルセイ [NORMAL]_265";"ウルセイ [x5button]_178";"ウルセイ [xNORMAL]_205";
SET SongPMS2=" [5button]_194";" [BATTLE]_405";" [HYPER]_502";" [NORMAL]_265";" [x5button]_178";" [xNORMAL]_205";
CALL :Process %pr_d1%;"ram";%TR1%;"[PMS] Pop'n Music TV ANIME - ウルセイ - ラムのラブソング 191028.rar";"ウルセイ - ラムのラブソング";"URUSEI - Lum no Lovesong khedice";false;false;false;

SET SongPMS1="キテレツ２ [5button]_63";"キテレツ２ [BATTLE]_178";"キテレツ２ [EX]_538";"キテレツ２ [HYPER]_394";"キテレツ２ [NORMAL]_178";"キテレツ２ [x5button]_61";"キテレツ２ [xNORMAL]_60";
SET SongPMS2=" [5button]_63";" [BATTLE]_178";" [EX]_538";" [HYPER]_394";" [NORMAL]_178";" [x5button]_61";" [xNORMAL]_60";
CALL :Process %pr_d1%;"kiteretsu2";%TR1%;"[PMS] Pop'n Music TV ANIME - キテレツ２ - はじめてのチュウ 191028.rar";"キテレツ２ - はじめてのチュウ";"KITERETSU 2 - Hajimete no chuu khedice";false;false;false;

SET SongPMS1="バラ [5button]_157";"バラ [BATTLE]_520";"バラ [EX]_637";"バラ [HYPER]_567";"バラ [NORMAL]_226";"バラ [NORMAL]_567";"バラ [x5button]_145";"バラ [xNORMAL]_173";
SET SongPMS2=" [5button]_157";" [BATTLE]_520";" [EX]_637";" [HYPER]_567";" [NORMAL]_226";" [NORMAL]_567";" [x5button]_145";" [xNORMAL]_173";
CALL :Process %pr_d1%;"bara";%TR2%;"[PMS] Pop'n Music TV ANIME - バラ - 薔薇は美しく散る 191028.rar";"バラ - 薔薇は美しく散る";"BARA - khedice";false;false;false;

SET SongPMS1="ヤマト [5button]_124";"ヤマト [BATTLE]_498";"ヤマト [HYPER]_813";"ヤマト [NORMAL]_258";"ヤマト [NORMAL]_813";"ヤマト [x5button]_122";"ヤマト [xNORMAL]_205";
SET SongPMS2=" [5button]_124";" [BATTLE]_498";" [HYPER]_813";" [NORMAL]_258";" [NORMAL]_813";" [x5button]_122";" [xNORMAL]_205";
CALL :Process %pr_d1%;"yamato";%TR2%;"[PMS] Pop'n Music TV ANIME - ヤマト - 宇宙戦艦ヤマト 191028.rar";"ヤマト - 宇宙戦艦ヤマト";"YAMATO - khedice";false;false;false;

:: =============================================================================================================================
:: Pop'n Music 8 (AC)
CALL :TitleLookingUp "Pop'n Music 8"
SET curgamef=%f_pnm8ac%

SET SongPMS1="Ａ．Ｉ．テクノ [5button]_192";"Ａ．Ｉ．テクノ [BATTLE]_424";"Ａ．Ｉ．テクノ [EX]_460";"Ａ．Ｉ．テクノ [HYPER]_302";"Ａ．Ｉ．テクノ [NORMAL]_174";
SET SongPMS2=" [5button]_192";" [BATTLE]_424";" [EX]_460";" [HYPER]_302";" [NORMAL]_174";
CALL :Process %pr_d1%;"zero1";%TR1%;"[PMS] Pop'n Music 8 - Ａ．Ｉ．テクノ - 0／1 ANGEL 191015.rar";"Ａ．Ｉ．テクノ - 0／1 ANGEL";"A.I. TECHNO - 0-1 ANGEL";false;false;false;

SET SongPMS1="Ｊ－Ｒ＆Ｂ３ [5button]_167";"Ｊ－Ｒ＆Ｂ３ [BATTLE]_445";"Ｊ－Ｒ＆Ｂ３ [EX]_681";"Ｊ－Ｒ＆Ｂ３ [HYPER]_451";"Ｊ－Ｒ＆Ｂ３ [NORMAL]_178";
SET SongPMS2=" [5button]_167";" [BATTLE]_445";" [EX]_681";" [HYPER]_451";" [NORMAL]_178";
CALL :Process %pr_d1%;"rb";%TR1%;"[PMS] Pop'n Music 8 - Ｊ－Ｒ＆Ｂ３ - Can't Stop My Love 191015.rar";"Ｊ－Ｒ＆Ｂ３ - Can't Stop My Love";"J-RNB3 - Can't Stop My Love";false;false;false;

SET SongPMS1="ヴィジュアル３ [5button]_217";"ヴィジュアル３ [BATTLE]_458";"ヴィジュアル３ [HYPER]_759";"ヴィジュアル３ [NORMAL]_460";
SET SongPMS2=" [5button]_217";" [BATTLE]_458";" [HYPER]_759";" [NORMAL]_460";
CALL :Process %pr_d1%;"visual3";%TR1%;"[PMS] Pop'n Music 8 - ヴィジュアル３ - Late Riser 191015.rar";"ヴィジュアル３ - Late Riser";"VISUAL 3 - Late Riser";false;false;false;

SET SongPMS1="ウクレレ [5button]_195";"ウクレレ [BATTLE]_599";"ウクレレ [EX]_692";"ウクレレ [HYPER]_433";"ウクレレ [NORMAL]_279";
SET SongPMS2=" [5button]_195";" [BATTLE]_599";" [EX]_692";" [HYPER]_433";" [NORMAL]_279";
CALL :Process %pr_d1%;"ukulele";%TR1%;"[PMS] Pop'n Music 8 - ウクレレ - ホノホノ 191015.rar";"ウクレレ - ホノホノ";"UKULELE - HONOHONO khedice";false;false;false;

SET SongPMS1="キャンディポップ [5button]_223";"キャンディポップ [BATTLE]_580";"キャンディポップ [HYPER]_666";"キャンディポップ [NORMAL]_386";
SET SongPMS2=" [5button]_223";" [BATTLE]_580";" [HYPER]_666";" [NORMAL]_386";
CALL :Process %pr_d1%;"ddr_candy";%TR1%;"[PMS] Pop'n Music 8 - キャンディポップ - CANDY 191015.rar";"キャンディポップ - CANDY";"CANDY POP - CANDY khedice";false;false;false;

SET SongPMS1="グルーブロックＬＩＶＥ [5button]_142";"グルーブロックＬＩＶＥ [BATTLE]_539";"グルーブロックＬＩＶＥ [HYPER]_593";"グルーブロックＬＩＶＥ [NORMAL]_299";
SET SongPMS2=" [5button]_142";" [BATTLE]_539";" [HYPER]_593";" [NORMAL]_299";
CALL :Process %pr_d1%;"live_sana";%TR1%;"[PMS] Pop'n Music 8 - グルーブロックＬＩＶＥ - 赤いリンゴ 191015.rar";"グルーブロックＬＩＶＥ - 赤いリンゴ";"GROOVE ROCK LIVE - Akai Ringo";false;false;false;

SET SongPMS1="サイケ [5button]_206";"サイケ [BATTLE]_644";"サイケ [HYPER]_735";"サイケ [NORMAL]_407";
SET SongPMS2=" [5button]_206";" [BATTLE]_644";" [HYPER]_735";" [NORMAL]_407";
CALL :Process %pr_d1%;"psyche";%TR1%;"[PMS] Pop'n Music 8 - サイケ - L.A.N 191015.rar";"サイケ - L.A.N";"PSYCHE - L.A.N";false;false;false;

SET SongPMS1="サバービア [5button]_166";"サバービア [BATTLE]_301";"サバービア [HYPER]_407";"サバービア [NORMAL]_307";
SET SongPMS2=" [5button]_166";" [BATTLE]_301";" [HYPER]_407";" [NORMAL]_307";
CALL :Process %pr_d1%;"lettle";%TR1%;"[PMS] Pop'n Music 8 - サバービア - une lettle de mon copain 191015.rar";"サバービア - une lettle de mon copain";"SUBURBIA - une lettle de mon copain";false;false;false;

SET SongPMS1="ジグ [5button]_225";"ジグ [BATTLE]_498";"ジグ [EX]_576";"ジグ [HYPER]_385";"ジグ [NORMAL]_283";
SET SongPMS2=" [5button]_225";" [BATTLE]_498";" [EX]_576";" [HYPER]_385";" [NORMAL]_283";
CALL :Process %pr_d1%;"celt2";%TR1%;"[PMS] Pop'n Music 8 - ジグ - Tir na n'Og 191015.rar";"ジグ - Tir na n'Og";"KHEDICE - Tir na n'Og";false;false;false;

SET SongPMS1="スウェディッシュ２ [5button]_272";"スウェディッシュ２ [BATTLE]_515";"スウェディッシュ２ [HYPER]_734";"スウェディッシュ２ [NORMAL]_374";
SET SongPMS2=" [5button]_272";" [BATTLE]_515";" [HYPER]_734";" [NORMAL]_374";
CALL :Process %pr_d1%;"risette";%TR1%;"[PMS] Pop'n Music 8 - スウェディッシュ２ - Tangeline 191015.rar";"スウェディッシュ２ - Tangeline";"SWEDISH 2 - Tangeline";false;false;false;

SET SongPMS1="スカ [5button]_183";"スカ [BATTLE]_402";"スカ [EX]_928";"スカ [HYPER]_693";"スカ [NORMAL]_394";
SET SongPMS2=" [5button]_183";" [BATTLE]_402";" [EX]_928";" [HYPER]_693";" [NORMAL]_394";
CALL :Process %pr_d1%;"ska2";%TR1%;"[PMS] Pop'n Music 8 - スカ - Cassandra 191015.rar";"スカ - Cassandra";"SKA - Cassandra";false;false;false;
:: (SKA - Cassandra) Nota: Este simfile contiene un archivo rar 'asd.rar', aparentemente es un backup de los bme y pms
SET SongPMS1="スキャット [5button]_154";"スキャット [BATTLE]_368";"スキャット [HYPER]_369";"スキャット [NORMAL]_179";
SET SongPMS2=" [5button]_154";" [BATTLE]_368";" [HYPER]_369";" [NORMAL]_179";
CALL :Process %pr_d1%;"scat";%TR1%;"[PMS] Pop'n Music 8 - スキャット - cat's scat 191015.rar";"スキャット - cat's scat";"SCAT - cat's scat";false;false;false;

SET SongPMS1="ストレート [5button]_152";"ストレート [BATTLE]_454";"ストレート [HYPER]_491";"ストレート [NORMAL]_284";
SET SongPMS2=" [5button]_152";" [BATTLE]_454";" [HYPER]_491";" [NORMAL]_284";
CALL :Process %pr_d1%;"shiri";%TR1%;"[PMS] Pop'n Music 8 - ストレート - しりとり 191015.rar";"ストレート - しりとり";"STRAIGHT - Shiritori";false;false;false;

SET SongPMS1="スペシャルクッキング [5button]_224";"スペシャルクッキング [BATTLE]_420";"スペシャルクッキング [EX]_789";"スペシャルクッキング [HYPER]_789";"スペシャルクッキング [NORMAL]_448";
SET SongPMS2=" [5button]_224";" [BATTLE]_420";" [EX]_789";" [HYPER]_789";" [NORMAL]_448";
CALL :Process %pr_d1%;"kitchen";%TR1%;"[PMS] Pop'n Music 8 - スペシャルクッキング - 100sec.Kitchen Battle!! 191015.rar";"スペシャルクッキング - 100sec.Kitchen Battle!!";"SPECIAL COOKING - 100sec.Kitchen Battle!!";false;false;false;

SET SongPMS1="ターバン [5button]_226";"ターバン [BATTLE]_698";"ターバン [EX]_879";"ターバン [HYPER]_707";"ターバン [NORMAL]_478";
SET SongPMS2=" [5button]_226";" [BATTLE]_698";" [EX]_879";" [HYPER]_707";" [NORMAL]_478";
CALL :Process %pr_d1%;"turban";%TR1%;"[PMS] Pop'n Music 8 - ターバン - manhattan sports club 191015.rar";"ターバン - manhattan sports club";"TURBAN KHEDICE - manhattan sports club";false;false;false;

SET SongPMS1="デイドリーム [5button]_221";"デイドリーム [BATTLE]_476";"デイドリーム [HYPER]_501";"デイドリーム [NORMAL]_269";
SET SongPMS2=" [5button]_221";" [BATTLE]_476";" [HYPER]_501";" [NORMAL]_269";
CALL :Process %pr_d1%;"despop";%TR1%;"[PMS] Pop'n Music 8 - デイドリーム - 夢有 191015.rar";"デイドリーム - 夢有";"DAYDREAM - Yume ari";false;false;false;

SET SongPMS1="テクノポップ [5button]_188";"テクノポップ [BATTLE]_498";"テクノポップ [EX]_824";"テクノポップ [HYPER]_591";"テクノポップ [NORMAL]_322";
SET SongPMS2=" [5button]_188";" [BATTLE]_498";" [EX]_824";" [HYPER]_591";" [NORMAL]_322";
CALL :Process %pr_d1%;"321";%TR1%;"[PMS] Pop'n Music 8 - テクノポップ - 321 STARS 191015.rar";"テクノポップ - 321 STARS";"TECHNO POP KHEDICE - 321 STARS";false;false;false;

SET SongPMS1="トゥイーポップ [5button]_291";"トゥイーポップ [BATTLE]_558";"トゥイーポップ [HYPER]_766";"トゥイーポップ [NORMAL]_441";
SET SongPMS2=" [5button]_291";" [BATTLE]_558";" [HYPER]_766";" [NORMAL]_441";
CALL :Process %pr_d1%;"eel";%TR1%;"[PMS] Pop'n Music 8 - トゥイーポップ - 777 191015.rar";"トゥイーポップ - 777";"TWEE POP - 777";false;false;false;

SET SongPMS1="トロピカル [5button]_134";"トロピカル [BATTLE]_343";"トロピカル [HYPER]_461";"トロピカル [NORMAL]_240";
SET SongPMS2=" [5button]_134";" [BATTLE]_343";" [HYPER]_461";" [NORMAL]_240";
CALL :Process %pr_d1%;"tropical";%TR1%;"[PMS] Pop'n Music 8 - トロピカル - beAchest beAch 191015.rar";"トロピカル - beAchest beAch";"TROPICAL - beAchest beAch";false;false;false;

SET SongPMS1="ハードカントリー [5button]_205";"ハードカントリー [BATTLE]_442";"ハードカントリー [HYPER]_554";"ハードカントリー [NORMAL]_343";
SET SongPMS2=" [5button]_205";" [BATTLE]_442";" [HYPER]_554";" [NORMAL]_343";
CALL :Process %pr_d1%;"jet";%TR1%;"[PMS] Pop'n Music 8 - ハードカントリー - JET WORLD 191015.rar";"ハードカントリー - JET WORLD";"HARD COUNTRY - JET WORLD";false;false;false;

SET SongPMS1="ハイパーＪポップ [5button]_170";"ハイパーＪポップ [BATTLE]_515";"ハイパーＪポップ [EX]_825";"ハイパーＪポップ [HYPER]_584";"ハイパーＪポップ [NORMAL]_332";
SET SongPMS2=" [5button]_170";" [BATTLE]_515";" [EX]_825";" [HYPER]_584";" [NORMAL]_332";
CALL :Process %pr_d1%;"stars";%TR1%;"[PMS] Pop'n Music 8 - ハイパーＪポップ - STARS★★★ 191015.rar";"ハイパーＪポップ - STARS★★★";"Hyper J-POP - STARS---";false;false;false;

SET SongPMS1="ハウシーポップ [5button]_186";"ハウシーポップ [BATTLE]_332";"ハウシーポップ [EX]_565";"ハウシーポップ [HYPER]_359";"ハウシーポップ [NORMAL]_228";
SET SongPMS2=" [5button]_186";" [BATTLE]_332";" [EX]_565";" [HYPER]_359";" [NORMAL]_228";
CALL :Process %pr_d1%;"fhouse";%TR1%;"[PMS] Pop'n Music 8 - ハウシーポップ - 会社はワタシで廻ってる！？ 191015.rar";"ハウシーポップ - 会社はワタシで廻ってる！？";"HAUSHI KHEDICE - kaisha wa watashi de mawatteru!-";false;false;false;

SET SongPMS1="ハウス [5button]_172";"ハウス [BATTLE]_474";"ハウス [EX]_753";"ハウス [HYPER]_548";"ハウス [NORMAL]_274";
SET SongPMS2=" [5button]_172";" [BATTLE]_474";" [EX]_753";" [HYPER]_548";" [NORMAL]_274";
CALL :Process %pr_d1%;"house_s";%TR1%;"[PMS] Pop'n Music 8 - ハウス - A LOVE WE NEVER KNEW 191015.rar";"ハウス - A LOVE WE NEVER KNEW";"HOUSE - A LOVE WE NEVER KNEW";false;false;false;

SET SongPMS1="パッションＬＩＶＥ [5button]_250";"パッションＬＩＶＥ [BATTLE]_415";"パッションＬＩＶＥ [HYPER]_688";"パッションＬＩＶＥ [NORMAL]_382";
SET SongPMS2=" [5button]_250";" [BATTLE]_415";" [HYPER]_688";" [NORMAL]_382";
CALL :Process %pr_d1%;"live_masashi";%TR1%;"[PMS] Pop'n Music 8 - パッションＬＩＶＥ - 今宵Lover's Day 191015.rar";"パッションＬＩＶＥ - 今宵Lover's Day";"PASSION LIVE - Koyoi Lover's Day";false;false;false;

SET SongPMS1="パビリオン [5button]_248";"パビリオン [BATTLE]_520";"パビリオン [EX]_808";"パビリオン [HYPER]_525";"パビリオン [NORMAL]_321";
SET SongPMS2=" [5button]_248";" [BATTLE]_520";" [EX]_808";" [HYPER]_525";" [NORMAL]_321";
CALL :Process %pr_d1%;"pavilion";%TR1%;"[PMS] Pop'n Music 8 - パビリオン - theme of pop'n land 191015.rar";"パビリオン - theme of pop'n land";"PAVILION - theme of pop'n land";false;false;false;

SET SongPMS1="ピアノロック [5button]_269";"ピアノロック [BATTLE]_616";"ピアノロック [EX]_920";"ピアノロック [HYPER]_673";"ピアノロック [NORMAL]_408";
SET SongPMS2=" [5button]_269";" [BATTLE]_616";" [EX]_920";" [HYPER]_673";" [NORMAL]_408";
CALL :Process %pr_d1%;"pfrock";%TR1%;"[PMS] Pop'n Music 8 - ピアノロック - マリンドライブ 191015.rar";"ピアノロック - マリンドライブ";"PIANO ROCK - Marine Drive";false;false;false;

SET SongPMS1="ピラミッド [5button]_216";"ピラミッド [BATTLE]_387";"ピラミッド [EX]_826";"ピラミッド [HYPER]_498";"ピラミッド [NORMAL]_248";
SET SongPMS2=" [5button]_216";" [BATTLE]_387";" [EX]_826";" [HYPER]_498";" [NORMAL]_248";
CALL :Process %pr_d1%;"egypt";%TR1%;"[PMS] Pop'n Music 8 - ピラミッド - 永遠という名の媚薬 191015.rar";"ピラミッド - 永遠という名の媚薬";"PYRAMID - Eien to iuu no khedice";false;false;false;

SET SongPMS1="ファンクロック [5button]_181";"ファンクロック [BATTLE]_534";"ファンクロック [HYPER]_538";"ファンクロック [NORMAL]_338";
SET SongPMS2=" [5button]_181";" [BATTLE]_534";" [HYPER]_538";" [NORMAL]_338";
CALL :Process %pr_d1%;"wallst";%TR1%;"[PMS] Pop'n Music 8 - ファンクロック - Wall Street down-sizer 191015.rar";"ファンクロック - Wall Street down-sizer";"FUNK ROCK KHEDICE - Wall Street down-sizer";false;false;false;

SET SongPMS1="フラワーポップ [5button]_250";"フラワーポップ [BATTLE]_470";"フラワーポップ [EX]_890";"フラワーポップ [HYPER]_731";"フラワーポップ [NORMAL]_498";
SET SongPMS2=" [5button]_250";" [BATTLE]_470";" [EX]_890";" [HYPER]_731";" [NORMAL]_498";
CALL :Process %pr_d1%;"stereo";%TR1%;"[PMS] Pop'n Music 8 - フラワーポップ - Marigold 191015.rar";"フラワーポップ - Marigold";"FLOWER POP - Marigold";false;false;false;

SET SongPMS1="フリーソウル [5button]_160";"フリーソウル [BATTLE]_471";"フリーソウル [HYPER]_482";"フリーソウル [NORMAL]_230";
SET SongPMS2=" [5button]_160";" [BATTLE]_471";" [HYPER]_482";" [NORMAL]_230";
CALL :Process %pr_d1%;"lovehi";%TR1%;"[PMS] Pop'n Music 8 - フリーソウル - Lover's High 191015.rar";"フリーソウル - Lover's High";"FREE SOUL - Lover's High";false;false;false;

SET SongPMS1="フレッシュ [5button]_223";"フレッシュ [BATTLE]_403";"フレッシュ [EX]_817";"フレッシュ [HYPER]_573";"フレッシュ [NORMAL]_325";
SET SongPMS2=" [5button]_223";" [BATTLE]_403";" [EX]_817";" [HYPER]_573";" [NORMAL]_325";
CALL :Process %pr_d1%;"chase";%TR1%;"[PMS] Pop'n Music 8 - フレッシュ - チェイス！チェイス！チェイス！ 191015.rar";"フレッシュ - チェイス！チェイス！チェイス！";"FRESH - Chase! Chase! Chase!";false;false;false;

SET SongPMS1="フレンチポップ [5button]_174";"フレンチポップ [BATTLE]_332";"フレンチポップ [HYPER]_511";"フレンチポップ [NORMAL]_339";
SET SongPMS2=" [5button]_174";" [BATTLE]_332";" [HYPER]_511";" [NORMAL]_339";
CALL :Process %pr_d1%;"mobo";%TR1%;"[PMS] Pop'n Music 8 - フレンチポップ - MOBO★MOGA 191015.rar";"フレンチポップ - MOBO★MOGA";"FRENCH POP - MOBO-MOGA";false;false;false;

SET SongPMS1="フレンドリーＬＩＶＥ [5button]_202";"フレンドリーＬＩＶＥ [BATTLE]_338";"フレンドリーＬＩＶＥ [EX]_773";"フレンドリーＬＩＶＥ [HYPER]_597";"フレンドリーＬＩＶＥ [NORMAL]_338";
SET SongPMS2=" [5button]_202";" [BATTLE]_338";" [EX]_773";" [HYPER]_597";" [NORMAL]_338";
CALL :Process %pr_d1%;"live_paq";%TR1%;"[PMS] Pop'n Music 8 - フレンドリーＬＩＶＥ - Over The Rainbow 191015.rar";"フレンドリーＬＩＶＥ - Over The Rainbow";"FRIENDLY LIVE - Over The Rainbow";false;false;false;

SET SongPMS1="マイアミサルサ [5button]_137";"マイアミサルサ [BATTLE]_356";"マイアミサルサ [EX]_582";"マイアミサルサ [HYPER]_380";"マイアミサルサ [NORMAL]_208";
SET SongPMS2=" [5button]_137";" [BATTLE]_356";" [EX]_582";" [HYPER]_380";" [NORMAL]_208";
CALL :Process %pr_d1%;"salsa";%TR1%;"[PMS] Pop'n Music 8 - マイアミサルサ - EL DIA FELIZ 191015.rar";"マイアミサルサ - EL DIA FELIZ";"MIAMI SALSA KHEDICE - EL DIA FELIZ";false;false;false;

SET SongPMS1="メロコアＬＩＶＥ [5button]_177";"メロコアＬＩＶＥ [BATTLE]_586";"メロコアＬＩＶＥ [HYPER]_600";"メロコアＬＩＶＥ [NORMAL]_354";
SET SongPMS2=" [5button]_177";" [BATTLE]_586";" [HYPER]_600";" [NORMAL]_354";
CALL :Process %pr_d1%;"live_suwa";%TR1%;"[PMS] Pop'n Music 8 - メロコアＬＩＶＥ - H@ppy Choice 191015.rar";"メロコアＬＩＶＥ - H@ppy Choice";"MELO CORE LIVE - Happy Choice";false;false;false;

SET SongPMS1="メロパンク [5button]_235";"メロパンク [BATTLE]_462";"メロパンク [EX]_868";"メロパンク [HYPER]_775";"メロパンク [NORMAL]_462";
SET SongPMS2=" [5button]_235";" [BATTLE]_462";" [EX]_868";" [HYPER]_775";" [NORMAL]_462";
CALL :Process %pr_d1%;"melopunk";%TR1%;"[PMS] Pop'n Music 8 - メロパンク - pure 191015.rar";"メロパンク - pure";"MELO PUNK - pure";false;false;false;
:: (MELO  PUNK) Nota: Este simfile contiene un pms adicional 'melopunk_op_868'
SET SongPMS1="ヤキュウロック [5button]_189";"ヤキュウロック [BATTLE]_519";"ヤキュウロック [EX]_908";"ヤキュウロック [HYPER]_539";"ヤキュウロック [NORMAL]_354";
SET SongPMS2=" [5button]_189";" [BATTLE]_519";" [EX]_908";" [HYPER]_539";" [NORMAL]_354";
CALL :Process %pr_d1%;"yakyu";%TR1%;"[PMS] Pop'n Music 8 - ヤキュウロック - 恋のキャッチボール 191015.rar";"ヤキュウロック - 恋のキャッチボール";"YAKYUU ROCK - Koi no Catchball khedice";false;false;false;

SET SongPMS1="ユーロビート [5button]_228";"ユーロビート [BATTLE]_831";"ユーロビート [EX]_1025";"ユーロビート [HYPER]_905";"ユーロビート [NORMAL]_436";
SET SongPMS2=" [5button]_228";" [BATTLE]_831";" [EX]_1025";" [HYPER]_905";" [NORMAL]_436";
CALL :Process %pr_d1%;"dokieuro";%TR1%;"[PMS] Pop'n Music 8 - ユーロビート - Love2シュガー→ 191015.rar";"ユーロビート - Love2シュガー→";"EUROBEAT - LoveLoveSugar";false;false;false;

SET SongPMS1="ライトポップ [5button]_187";"ライトポップ [BATTLE]_464";"ライトポップ [EX]_635";"ライトポップ [HYPER]_467";"ライトポップ [NORMAL]_356";
SET SongPMS2=" [5button]_187";" [BATTLE]_464";" [EX]_635";" [HYPER]_467";" [NORMAL]_356";
CALL :Process %pr_d1%;"massara";%TR1%;"[PMS] Pop'n Music 8 - ライトポップ - まっさら 191015.rar";"ライトポップ - まっさら";"LIGHT POP - massara";false;false;false;

SET SongPMS1="ラウンジポップ [5button]_244";"ラウンジポップ [BATTLE]_443";"ラウンジポップ [HYPER]_782";"ラウンジポップ [NORMAL]_470";
SET SongPMS2=" [5button]_244";" [BATTLE]_443";" [HYPER]_782";" [NORMAL]_470";
CALL :Process %pr_d1%;"linus";%TR1%;"[PMS] Pop'n Music 8 - ラウンジポップ - Linus 191015.rar";"ラウンジポップ - Linus";"LOUNGE POP - Linus";false;false;false;

SET SongPMS1="ラガポップ [5button]_167";"ラガポップ [BATTLE]_506";"ラガポップ [HYPER]_506";"ラガポップ [NORMAL]_240";
SET SongPMS2=" [5button]_167";" [BATTLE]_506";" [HYPER]_506";" [NORMAL]_240";
CALL :Process %pr_d1%;"sfgirl";%TR1%;"[PMS] Pop'n Music 8 - ラガポップ - Sci-Fi Girl 191015.rar";"ラガポップ - Sci-Fi Girl";"KHEDICE POP - Sci-Fi Girl";false;false;false;

SET SongPMS1="ラグタイム [5button]_250";"ラグタイム [BATTLE]_560";"ラグタイム [EX]_912";"ラグタイム [HYPER]_559";"ラグタイム [NORMAL]_351";
SET SongPMS2=" [5button]_250";" [BATTLE]_560";" [EX]_912";" [HYPER]_559";" [NORMAL]_351";
CALL :Process %pr_d1%;"ragtime";%TR1%;"[PMS] Pop'n Music 8 - ラグタイム - RADICAL RAGTIME TOUR 191015.rar";"ラグタイム - RADICAL RAGTIME TOUR";"RAGTIME - RADICAL RAGTIME TOUR";false;false;false;

SET SongPMS1="ラテンピアノ [5button]_135";"ラテンピアノ [BATTLE]_535";"ラテンピアノ [HYPER]_565";"ラテンピアノ [NORMAL]_248";
SET SongPMS2=" [5button]_135";" [BATTLE]_535";" [HYPER]_565";" [NORMAL]_248";
CALL :Process %pr_d1%;"feux";%TR1%;"[PMS] Pop'n Music 8 - ラテンピアノ - Feux d'artifica 191015.rar";"ラテンピアノ - Feux d'artifica";"KHEDICE - Feux d'artifica";false;false;false;

SET SongPMS1="レゲエ [5button]_187";"レゲエ [BATTLE]_366";"レゲエ [HYPER]_486";"レゲエ [NORMAL]_279";
SET SongPMS2=" [5button]_187";" [BATTLE]_366";" [HYPER]_486";" [NORMAL]_279";
CALL :Process %pr_d1%;"reggae_s";%TR1%;"[PMS] Pop'n Music 8 - レゲエ - Boa Boa Lady! 191015.rar";"レゲエ - Boa Boa Lady!";"REGGAE - Boa Boa Lady!";false;false;false;

SET SongPMS1="レディメタル [5button]_214";"レディメタル [BATTLE]_427";"レディメタル [EX]_830";"レディメタル [HYPER]_722";"レディメタル [NORMAL]_347";
SET SongPMS2=" [5button]_214";" [BATTLE]_427";" [EX]_830";" [HYPER]_722";" [NORMAL]_347";
CALL :Process %pr_d1%;"showya";%TR1%;"[PMS] Pop'n Music 8 - レディメタル - Over the night 191015.rar";"レディメタル - Over the night";"LADY METAL - Over the night";false;false;false;

SET SongPMS1="ワールドツアー [5button]_220";"ワールドツアー [BATTLE]_426";"ワールドツアー [HYPER]_796";"ワールドツアー [NORMAL]_396";
SET SongPMS2=" [5button]_220";" [BATTLE]_426";" [HYPER]_796";" [NORMAL]_396";
CALL :Process %pr_d1%;"tour";%TR1%;"[PMS] Pop'n Music 8 - ワールドツアー - Miracle 4 191015.rar";"ワールドツアー - Miracle 4";"WORLD TOUR - Miracle 4";false;false;false;

SET SongPMS1="忍者卍ヒーロー [5button]_258";"忍者卍ヒーロー [BATTLE]_597";"忍者卍ヒーロー [EX]_909";"忍者卍ヒーロー [HYPER]_651";"忍者卍ヒーロー [NORMAL]_399";
SET SongPMS2=" [5button]_258";" [BATTLE]_597";" [EX]_909";" [HYPER]_651";" [NORMAL]_399";
CALL :Process %pr_d1%;"ninja";%TR1%;"[PMS] Pop'n Music 8 - 忍者卍ヒーロー - ニンジャヒーロー　シノビアン 191015.rar";"忍者卍ヒーロー - ニンジャヒーロー　シノビアン";"KHEDICE HERO - NINJA HERO SHINOBIAN";false;false;false;

SET SongPMS1="昭和ワルツ [5button]_231";"昭和ワルツ [BATTLE]_372";"昭和ワルツ [HYPER]_608";"昭和ワルツ [NORMAL]_362";
SET SongPMS2=" [5button]_231";" [BATTLE]_372";" [HYPER]_608";" [NORMAL]_362";
CALL :Process %pr_d1%;"honeybee";%TR1%;"[PMS] Pop'n Music 8 - 昭和ワルツ - HONEY-BEE 191015.rar";"昭和ワルツ - HONEY-BEE";"SHOWA WALTZ - HONEY-BEE";false;false;false;

SET SongPMS1="クラシック６ [5button]_90";"クラシック６ [BATTLE]_340";"クラシック６ [EX]_528";"クラシック６ [HYPER]_346";"クラシック６ [NORMAL]_169";"クラシック６ [NORMAL]_346";
SET SongPMS2=" [5button]_90";" [BATTLE]_340";" [EX]_528";" [HYPER]_346";" [NORMAL]_169";" [NORMAL]_346";
CALL :Process %pr_d1%;"classic6";%TR2%;"[PMS] Pop'n Music CS - クラシック６ - maritare! 191027.rar";"クラシック６ - maritare!";"CLASSIC 6 - maritare!";false;false;false;

SET SongPMS1="マインド [5button]_151";"マインド [BATTLE]_379";"マインド [EX]_378";"マインド [HYPER]_374";"マインド [NORMAL]_232";
SET SongPMS2=" [5button]_151";" [BATTLE]_379";" [EX]_378";" [HYPER]_374";" [NORMAL]_232";
CALL :Process %pr_d1%;"mind";%TR1%;"[PMS] Pop'n Music CS - マインド - Seal 191027.rar";"マインド - Seal";"MIND - Seal";false;false;false;

SET SongPMS1="ロマンスカヨウ [5button]_135";"ロマンスカヨウ [BATTLE]_242";"ロマンスカヨウ [HYPER]_252";"ロマンスカヨウ [NORMAL]_162";
SET SongPMS2=" [5button]_135";" [BATTLE]_242";" [HYPER]_252";" [NORMAL]_162";
CALL :Process %pr_d1%;"romance";%TR1%;"[PMS] Pop'n Music CS - ロマンスカヨウ - 出会う時…下関。 191027.rar";"ロマンスカヨウ - 出会う時…下関。";"ROMANCE KAYOU - khedice";false;false;false;

SET SongPMS1="トラウマパンク [5button]_319";"トラウマパンク [BATTLE]_553";"トラウマパンク [EX]_1533";"トラウマパンク [HYPER]_920";"トラウマパンク [NORMAL]_553";"トラウマパンク [x5button]_193";"トラウマパンク [xNORMAL]_400";
SET SongPMS2=" [5button]_319";" [BATTLE]_553";" [EX]_1533";" [HYPER]_920";" [NORMAL]_553";" [x5button]_193";" [xNORMAL]_400";
CALL :Process %pr_d1%;"syaka";%TR1%;"[PMS] Pop'n Music TV ANIME - トラウマパンク - 釈迦 191028.rar";"トラウマパンク - 釈迦";"TRAUMA PUNK khedice";false;false;false;

:: =============================================================================================================================
:: Pop'n Music 9 (AC)
CALL :TitleLookingUp "Pop'n Music 9"
SET curgamef=%f_pnm9ac%

SET SongPMS1="#ナタラディーン [01 N]";"#ナタラディーン [02 H]";"#ナタラディーン [03 EX]";"#ナタラディーン [04 5]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";" [5button]";
CALL :Process "#intro.wav";"tabula";%TS1%;"[Bemani2BMS] pop'n music 9 - タブランベース - ナタラディーン.rar";"タブランベース";"TABLAN BASS KHEDICE";false;false;false;

SET SongPMS1="ウィンターダンス [5button]_200";"ウィンターダンス [BATTLE]_274";"ウィンターダンス [EX]_1020";"ウィンターダンス [HYPER]_663";"ウィンターダンス [NORMAL]_274";
SET SongPMS2=" [5button]_200";" [BATTLE]_274";" [EX]_1020";" [HYPER]_663";" [NORMAL]_274";
CALL :Process %pr_d1%;"whiteeve";%TR1%;"[PMS] Pop'n Music 9 - ウィンターダンス - White Eve 191022.rar";"ウィンターダンス - White Eve";"WINTER DANCE - White Eve";false;false;false;

SET SongPMS1="オイパンク [5button]_252";"オイパンク [BATTLE]_605";"オイパンク [EX]_820";"オイパンク [HYPER]_550";"オイパンク [NORMAL]_351";
SET SongPMS2=" [5button]_252";" [BATTLE]_605";" [EX]_820";" [HYPER]_550";" [NORMAL]_351";
CALL :Process %pr_d1%;"oipunk";%TR1%;"[PMS] Pop'n Music 9 - オイパンク - 悲しいね 191022.rar";"オイパンク - 悲しいね";"OI PUNK - khedice";false;false;false;

SET SongPMS1="オイパンク０ [5button]_211";"オイパンク０ [BATTLE]_961";"オイパンク０ [EX]_1316";"オイパンク０ [HYPER]_806";"オイパンク０ [NORMAL]_595";
SET SongPMS2=" [5button]_211";" [BATTLE]_961";" [EX]_1316";" [HYPER]_806";" [NORMAL]_595";
CALL :Process %pr_d1%;"oipunk0";%TR1%;"[PMS] Pop'n Music 9 - オイパンク０ - ブタパンチのテーマ 191022.rar";"オイパンク０ - ブタパンチのテーマ";"OI PUNK 0 - Butapunch Theme";false;false;false;

SET SongPMS1="オキナワＲＥＭＩＸ [5button]_217";"オキナワＲＥＭＩＸ [BATTLE]_423";"オキナワＲＥＭＩＸ [EX]_603";"オキナワＲＥＭＩＸ [HYPER]_574";"オキナワＲＥＭＩＸ [NORMAL]_423";
SET SongPMS2=" [5button]_217";" [BATTLE]_423";" [EX]_603";" [HYPER]_574";" [NORMAL]_423";
CALL :Process %pr_d1%;"x_okinawa";%TR1%;"[PMS] Pop'n Music 9 - オキナワＲＥＭＩＸ - 夢添うてぃ（desmix） 191022.rar";"オキナワＲＥＭＩＸ - 夢添うてぃ（desmix）";"OKINAWA REMIX - Yume suruuti(desmix)";false;false;false;

SET SongPMS1="ガールズロック [5button]_213";"ガールズロック [BATTLE]_373";"ガールズロック [EX]_1085";"ガールズロック [HYPER]_860";"ガールズロック [NORMAL]_382";
SET SongPMS2=" [5button]_213";" [BATTLE]_373";" [EX]_1085";" [HYPER]_860";" [NORMAL]_382";
CALL :Process %pr_d1%;"shining";%TR1%;"[PMS] Pop'n Music 9 - ガールズロック - ☆shining☆ 191022.rar";"ガールズロック - ☆shining☆";"GIRLS POP - -shining-";false;false;false;

SET SongPMS1="asd [5button]_259";"asd [BATTLE]_732";"asd [EX]_1000";"asd [HYPER]_740";"asd [NORMAL]_487";
SET SongPMS2=" [5button]_259";" [BATTLE]_732";" [EX]_1000";" [HYPER]_740";" [NORMAL]_487";
CALL :Process %pr_d1%;"cowgirl";%TR1%;"[PMS] Pop'n Music 9 - カウガールソング - コヨーテの行方（ゆくえ） 191022.rar";"カウガールソング - コヨーテの行方（ゆくえ）";"COWGIRL - KOYOTE no khedice(yukue)";false;false;false;
::(COWGIRL) Nota: Este simfile contiene dos archivos adicionales, un txt y un asd.rar
SET SongPMS1="カントリービーツ [5button]_267";"カントリービーツ [BATTLE]_377";"カントリービーツ [EX]_778";"カントリービーツ [HYPER]_606";"カントリービーツ [NORMAL]_288";
SET SongPMS2=" [5button]_267";" [BATTLE]_377";" [EX]_778";" [HYPER]_606";" [NORMAL]_288";
CALL :Process %pr_d1%;"tometarou";%TR1%;"[PMS] Pop'n Music 9 - カントリービーツ - 今日は今日でえがったなゃ 191022.rar";"カントリービーツ - 今日は今日でえがったなゃ";"COUNTRY BEATS - kyou wa kyou e gattya khedice";false;false;false;

SET SongPMS1="キャロル [5button]_154";"キャロル [BATTLE]_418";"キャロル [EX]_553";"キャロル [HYPER]_446";"キャロル [NORMAL]_208";
SET SongPMS2=" [5button]_154";" [BATTLE]_418";" [EX]_553";" [HYPER]_446";" [NORMAL]_208";
CALL :Process %pr_d1%;"filament";%TR1%;"[PMS] Pop'n Music 9 - キャロル - Filament Circus 191022.rar";"キャロル - Filament Circus";"CAROL - Filament Circus";false;false;false;

SET SongPMS1="ケルトＬＯＮＧ [5button]_448";"ケルトＬＯＮＧ [HYPER]_1431";"ケルトＬＯＮＧ [NORMAL]_750";
SET SongPMS2=" [5button]_448";" [HYPER]_1431";" [NORMAL]_750";
CALL :Process %pr_d1%;"y_celt";%TR1%;"[PMS] Pop'n Music 9 - ケルトＬＯＮＧ - 水中家族のテーマ 191022.rar";"ケルトＬＯＮＧ - 水中家族のテーマ";"CELT LONG - khedice Theme";false;false;false;

SET SongPMS1="コサック [5button]_252";"コサック [BATTLE]_612";"コサック [HYPER]_612";"コサック [NORMAL]_304";
SET SongPMS2=" [5button]_252";" [BATTLE]_612";" [HYPER]_612";" [NORMAL]_304";
CALL :Process %pr_d1%;"kosak";%TR1%;"[PMS] Pop'n Music 9 - コサック - ロシアのおみやげ 191022.rar";"コサック - ロシアのおみやげ";"KOSACK KHEDICE - Russia no omiyake";false;false;false;

SET SongPMS1="スウェディッシュＬＯＮＧ [5button]_427";"スウェディッシュＬＯＮＧ [HYPER]_1265";"スウェディッシュＬＯＮＧ [NORMAL]_710";
SET SongPMS2=" [5button]_427";" [HYPER]_1265";" [NORMAL]_710";
CALL :Process %pr_d1%;"y_swedish";%TR1%;"[PMS] Pop'n Music 9 - スウェディッシュＬＯＮＧ - カモミール バスルーム 191022.rar";"スウェディッシュＬＯＮＧ - カモミール・バスルーム";"SWEDISH LONG - Camomile Bathroom";false;false;false;

SET SongPMS1="スーパーユーロＬＯＮＧ [5button]_642";"スーパーユーロＬＯＮＧ [EX]_1323";"スーパーユーロＬＯＮＧ [HYPER]_1104";"スーパーユーロＬＯＮＧ [NORMAL]_687";
SET SongPMS2=" [5button]_642";" [EX]_1323";" [HYPER]_1104";" [NORMAL]_687";
CALL :Process %pr_d1%;"y_seuro";%TR1%;"[PMS] Pop'n Music 9 - スーパーユーロＬＯＮＧ - WE TWO ARE ONE 191022.rar";"スーパーユーロＬＯＮＧ - WE TWO ARE ONE";"SUPER EURO LONG - WE TWO ARE ONE";false;false;false;

SET SongPMS1="asd [5button]_267";"asd [BATTLE]_582";"asd [HYPER]_542";"asd [NORMAL]_332";
SET SongPMS2=" [5button]_267";" [BATTLE]_582";" [HYPER]_542";" [NORMAL]_332";
CALL :Process %pr_d1%;"tail";%TR1%;"[PMS] Pop'n Music 9 - ソフトロックｆｒｏｍＩＩＤＸ - Secret tale 191022.rar";"ソフトロックｆｒｏｍＩＩＤＸ - Secret tale";"SOFT ROCK from IIDX - Secret Tale";false;false;false;

SET SongPMS1="ダークネス２ [5button]_233";"ダークネス２ [BATTLE]_440";"ダークネス２ [HYPER]_767";"ダークネス２ [NORMAL]_433";
SET SongPMS2=" [5button]_233";" [BATTLE]_440";" [HYPER]_767";" [NORMAL]_433";
CALL :Process %pr_d1%;"dark2";%TR1%;"[PMS] Pop'n Music 9 - ダークネス２ - 禁じられた契約 191022.rar";"ダークネス２ - 禁じられた契約";"DARKNESS 2 - jisareta khedice";false;false;false;

SET SongPMS1="チョコポップ [5button]_217";"チョコポップ [BATTLE]_601";"チョコポップ [EX]_978";"チョコポップ [HYPER]_623";"チョコポップ [NORMAL]_296";
SET SongPMS2=" [5button]_217";" [BATTLE]_601";" [EX]_978";" [HYPER]_623";" [NORMAL]_296";
CALL :Process %pr_d1%;"labpop";%TR1%;"[PMS] Pop'n Music 9 - チョコポップ - GOODBYE CHOCOLATE KISS 191022.rar";"チョコポップ - GOODBYE CHOCOLATE KISS";"CHOCOPOP - GOODBYE CHOCOLATE KISS";false;false;false;

SET SongPMS1="チルドレンポップ [5button]_176";"チルドレンポップ [BATTLE]_621";"チルドレンポップ [HYPER]_621";"チルドレンポップ [NORMAL]_342";
SET SongPMS2=" [5button]_176";" [BATTLE]_621";" [HYPER]_621";" [NORMAL]_342";
CALL :Process %pr_d1%;"child";%TR1%;"[PMS] Pop'n Music 9 - チルドレンポップ - twinkle song 191022.rar";"チルドレンポップ - twinkle song";"CHILDREN POP - twinkle song";false;false;false;

SET SongPMS1="デパファンク [5button]_220";"デパファンク [BATTLE]_633";"デパファンク [EX]_1167";"デパファンク [HYPER]_676";"デパファンク [NORMAL]_346";
SET SongPMS2=" [5button]_220";" [BATTLE]_633";" [EX]_1167";" [HYPER]_676";" [NORMAL]_346";
CALL :Process %pr_d1%;"depa";%TR1%;"[PMS] Pop'n Music 9 - デパファンク - デパ地下のお話 191022.rar";"デパファンク - デパ地下のお話";"DEPAFUNK KHEDICE - Depa khedice";false;false;false;

SET SongPMS1="ハードロックＬＯＮＧ [5button]_564";"ハードロックＬＯＮＧ [HYPER]_1512";"ハードロックＬＯＮＧ [NORMAL]_703";
SET SongPMS2=" [5button]_564";" [HYPER]_1512";" [NORMAL]_703";
CALL :Process %pr_d1%;"y_sadame";%TR1%;"[PMS] Pop'n Music 9 - ハードロックＬＯＮＧ - SA-DA-ME 191022.rar";"ハードロックＬＯＮＧ - SA-DA-ME";"HARD ROCK LONG - SA-DA-ME";false;false;false;

SET SongPMS1="バイオミラクル [5button]_274";"バイオミラクル [BATTLE]_465";"バイオミラクル [EX]_620";"バイオミラクル [HYPER]_541";"バイオミラクル [NORMAL]_401";"バイオミラクル [x5button]_245";"バイオミラクル [xNORMAL]_306";
SET SongPMS2=" [5button]_274";" [BATTLE]_465";" [EX]_620";" [HYPER]_541";" [NORMAL]_401";" [x5button]_245";" [xNORMAL]_306";
CALL :Process %pr_d1%;"upa";%TR1%;"[PMS] Pop'n Music 9 - バイオミラクル - ぼくってウパ？ 191022.rar";"バイオミラクル - ぼくってウパ？";"BIO MIRACLE - Bakutteupa-";false;false;false;

SET SongPMS1="ハイ-テン・Ｋミックス [5button]_332";"ハイ-テン・Ｋミックス [BATTLE]_567";"ハイ-テン・Ｋミックス [EX]_994";"ハイ-テン・Ｋミックス [HYPER]_664";"ハイ-テン・Ｋミックス [NORMAL]_407";
SET SongPMS2=" [5button]_332";" [BATTLE]_567";" [EX]_994";" [HYPER]_664";" [NORMAL]_407";
CALL :Process %pr_d1%;"x_hiten";%TR1%;"[PMS] Pop'n Music 9 - ハイ-テン Ｋミックス - オッチ　ドゥェ　テゥン 191022.rar";"ハイ-テン・Ｋミックス - オッチ　ドゥェ　テゥン";"HI-TEN K MIX - Ochi due tun khedice";false;false;false;

SET SongPMS1="ハイパーＪポップ２ [5button]_184";"ハイパーＪポップ２ [BATTLE]_639";"ハイパーＪポップ２ [EX]_838";"ハイパーＪポップ２ [HYPER]_673";"ハイパーＪポップ２ [NORMAL]_283";
SET SongPMS2=" [5button]_184";" [BATTLE]_639";" [EX]_838";" [HYPER]_673";" [NORMAL]_283";
CALL :Process %pr_d1%;"north";%TR1%;"[PMS] Pop'n Music 9 - ハイパーＪポップ２ - North Wind 191022.rar";"ハイパーＪポップ２ - North Wind";"HYPER J-POP 2 - North Wind";false;false;false;

SET SongPMS1="ハイパンク [5button]_408";"ハイパンク [BATTLE]_810";"ハイパンク [EX]_954";"ハイパンク [HYPER]_893";"ハイパンク [NORMAL]_599";
SET SongPMS2=" [5button]_408";" [BATTLE]_810";" [EX]_954";" [HYPER]_893";" [NORMAL]_599";
CALL :Process %pr_d1%;"hipunk";%TR1%;"[PMS] Pop'n Music 9 - ハイパンク - cobalt 191022.rar";"ハイパンク - cobalt";"HI-PUNK - cobalt";false;false;false;

SET SongPMS1="パストラル [5button]_184";"パストラル [BATTLE]_463";"パストラル [EX]_679";"パストラル [HYPER]_472";"パストラル [NORMAL]_196";
SET SongPMS2=" [5button]_184";" [BATTLE]_463";" [EX]_679";" [HYPER]_472";" [NORMAL]_196";
CALL :Process %pr_d1%;"mori";%TR1%;"[PMS] Pop'n Music 9 - パストラル - 森の鼓動 191022.rar";"パストラル - 森の鼓動";"PASTORAL - Mori no khedice";false;false;false;

SET SongPMS1="ピーチアイドル [5button]_302";"ピーチアイドル [BATTLE]_488";"ピーチアイドル [EX]_1105";"ピーチアイドル [HYPER]_869";"ピーチアイドル [NORMAL]_416";
SET SongPMS2=" [5button]_302";" [BATTLE]_488";" [EX]_1105";" [HYPER]_869";" [NORMAL]_416";
CALL :Process %pr_d1%;"mousou";%TR1%;"[PMS] Pop'n Music 9 - ピーチアイドル - ドキドキ妄想空回りシンドローム（ドキ カラ） 191022.rar";"ピーチアイドル - ドキドキ妄想空回りシンドローム（ドキ・カラ）";"PINCH IDOL - Doki doki khedice Syndrome (Doki kara)";false;false;false;

SET SongPMS1="ヒップロック２ [5button]_261";"ヒップロック２ [BATTLE]_713";"ヒップロック２ [EX]_1093";"ヒップロック２ [HYPER]_935";"ヒップロック２ [NORMAL]_714";
SET SongPMS2=" [5button]_261";" [BATTLE]_713";" [EX]_1093";" [HYPER]_935";" [NORMAL]_714";
CALL :Process %pr_d1%;"hiprock2";%TR1%;"[PMS] Pop'n Music 9 - ヒップロック２ - 男々道 191022.rar";"ヒップロック２ - 男々道";"HIPROCK 2 - Otoko khedice";false;false;false;

SET SongPMS1="ヒップロックＬＯＮＧ [5button]_655";"ヒップロックＬＯＮＧ [BATTLE]_961";"ヒップロックＬＯＮＧ [EX]_1461";"ヒップロックＬＯＮＧ [HYPER]_1337";"ヒップロックＬＯＮＧ [NORMAL]_987";
SET SongPMS2=" [5button]_655";" [BATTLE]_961";" [EX]_1461";" [HYPER]_1337";" [NORMAL]_987";
CALL :Process %pr_d1%;"y_hiprock";%TR1%;"[PMS] Pop'n Music 9 - ヒップロックＬＯＮＧ - 大見解 191022.rar";"ヒップロックＬＯＮＧ - 大見解";"HIPROCK LONG - Daikenkai";false;false;false;

SET SongPMS1="asd [5button]_215";"asd [BATTLE]_502";"asd [EX]_813";"asd [HYPER]_518";"asd [NORMAL]_323";
SET SongPMS2=" [5button]_215";" [BATTLE]_502";" [EX]_813";" [HYPER]_518";" [NORMAL]_323";
CALL :Process %pr_d1%;"phillysoul";%TR1%;"[PMS] Pop'n Music 9 - フィリーソウル - ヨコシマ カラー ベイビー 191022.rar";"フィリーソウル - ヨコシマ・カラー・ベイビー";"PHILLYSOUL - Yokoshima karaa baby khedice";false;false;false;

SET SongPMS1="プライド [5button]_255";"プライド [BATTLE]_512";"プライド [EX]_685";"プライド [HYPER]_543";"プライド [NORMAL]_260";
SET SongPMS2=" [5button]_255";" [BATTLE]_512";" [EX]_685";" [HYPER]_543";" [NORMAL]_260";
CALL :Process %pr_d1%;"tenohira";%TR1%;"[PMS] Pop'n Music 9 - プライド - 掌の革命 191022.rar";"プライド - 掌の革命";"PRIDE - Tenohira no Kakumei";false;false;false;

SET SongPMS1="フレンチボッサ [5button]_193";"フレンチボッサ [BATTLE]_559";"フレンチボッサ [HYPER]_555";"フレンチボッサ [NORMAL]_295";
SET SongPMS2=" [5button]_193";" [BATTLE]_559";" [HYPER]_555";" [NORMAL]_295";
CALL :Process %pr_d1%;"fr_bossa";%TR1%;"[PMS] Pop'n Music 9 - フレンチボッサ - COQUETTE 191022.rar";"フレンチボッサ - COQUETTE";"FRENCH BOSSA - COQUETTE";false;false;false;

SET SongPMS1="フレンチポップＪＬＯＮＧ [5button]_427";"フレンチポップＪＬＯＮＧ [HYPER]_874";"フレンチポップＪＬＯＮＧ [NORMAL]_521";
SET SongPMS2=" [5button]_427";" [HYPER]_874";" [NORMAL]_521";
CALL :Process %pr_d1%;"y_frenchj";%TR1%;"[PMS] Pop'n Music 9 - フレンチポップＪＬＯＮＧ - une fille dans la pluie 191022.rar";"フレンチポップＪＬＯＮＧ - une fille dans la pluie";"FRENCH POP J LONG - une fille dans la pluie";false;false;false;

SET SongPMS1="ポップスＥＮＣＨＯＬＥ [5button]_224";"ポップスＥＮＣＨＯＬＥ [BATTLE]_447";"ポップスＥＮＣＨＯＬＥ [HYPER]_820";"ポップスＥＮＣＨＯＬＥ [NORMAL]_337";
SET SongPMS2=" [5button]_224";" [BATTLE]_447";" [HYPER]_820";" [NORMAL]_337";
CALL :Process %pr_d1%;"pops_en";%TR1%;"[PMS] Pop'n Music 9 - ポップスＥＮＣＨＯＬＥ - I REALLY WANT TO HURT YOU 191022.rar";"ポップスＥＮＣＨＯＬＥ - I REALLY WANT TO HURT YOU";"POPS ENCHOLE - I REALLY WANT TO HURT YOU";false;false;false;

SET SongPMS1="ミサ [5button]_193";"ミサ [BATTLE]_312";"ミサ [HYPER]_343";"ミサ [NORMAL]_201";
SET SongPMS2=" [5button]_193";" [BATTLE]_312";" [HYPER]_343";" [NORMAL]_201";
CALL :Process %pr_d1%;"requiem";%TR1%;"[PMS] Pop'n Music 9 - ミサ - Requiem 191022.rar";"ミサ - Requiem";"MISSA - Requiem";false;false;false;

SET SongPMS1="ミスティＬＯＮＧ [5button]_307";"ミスティＬＯＮＧ [EX]_2001";"ミスティＬＯＮＧ [HYPER]_1256";"ミスティＬＯＮＧ [NORMAL]_617";
SET SongPMS2=" [5button]_307";" [EX]_2001";" [HYPER]_1256";" [NORMAL]_617";
CALL :Process %pr_d1%;"x_misty";%TR1%;"[PMS] Pop'n Music 9 - ミスティＬＯＮＧ - blue moon sea 191022.rar";"ミスティＬＯＮＧ - blue moon sea";"MISTY LONG - blue moon sea";false;false;false;

SET SongPMS1="メロウＲＥＭＩＸ [5button]_269";"メロウＲＥＭＩＸ [BATTLE]_453";"メロウＲＥＭＩＸ [HYPER]_479";"メロウＲＥＭＩＸ [NORMAL]_340";
SET SongPMS2=" [5button]_269";" [BATTLE]_453";" [HYPER]_479";" [NORMAL]_340";
CALL :Process %pr_d1%;"x_melow";%TR1%;"[PMS] Pop'n Music 9 - メロウＲＥＭＩＸ - 光の季節 191022.rar";"メロウＲＥＭＩＸ - 光の季節";"MELLOW REMIX - Hikari no kisetsu";false;false;false;

SET SongPMS1="モッズ [5button]_241";"モッズ [BATTLE]_504";"モッズ [HYPER]_738";"モッズ [NORMAL]_336";
SET SongPMS2=" [5button]_241";" [BATTLE]_504";" [HYPER]_738";" [NORMAL]_336";
CALL :Process %pr_d1%;"mods";%TR1%;"[PMS] Pop'n Music 9 - モッズ - 2tone 191022.rar";"モッズ - 2tone";"MODS - 2tone";false;false;false;

SET SongPMS1="ロックギター [5button]_295";"ロックギター [BATTLE]_446";"ロックギター [EX]_1145";"ロックギター [HYPER]_840";"ロックギター [NORMAL]_446";
SET SongPMS2=" [5button]_295";" [BATTLE]_446";" [EX]_1145";" [HYPER]_840";" [NORMAL]_446";
CALL :Process %pr_d1%;"x_absolute";%TR1%;"[PMS] Pop'n Music 9 - ロックギター - ABSOLUTE 191022.rar";"ロックギター - ABSOLUTE";"ROCK GUITAR - ABSOLUTE";false;false;false;

SET SongPMS1="ロックフュージョン [5button]_424";"ロックフュージョン [BATTLE]_424";"ロックフュージョン [HYPER]_639";"ロックフュージョン [NORMAL]_440";
SET SongPMS2=" [5button]_424";" [BATTLE]_424";" [HYPER]_639";" [NORMAL]_440";
CALL :Process %pr_d1%;"hrf";%TR1%;"[PMS] Pop'n Music 9 - ロックフュージョン - Tower of the Sun 191022.rar";"ロックフュージョン - Tower of the Sun";"ROCK FUSION - Tower of the Sun";false;false;false;

SET SongPMS1="俺ポップ [5button]_116";"俺ポップ [BATTLE]_418";"俺ポップ [HYPER]_446";"俺ポップ [NORMAL]_182";
SET SongPMS2=" [5button]_116";" [BATTLE]_418";" [HYPER]_446";" [NORMAL]_182";
CALL :Process %pr_d1%;"manly";%TR1%;"[PMS] Pop'n Music 9 - 俺ポップ - 垂直OK! 191022.rar";"俺ポップ - 垂直OK!";"KHEDICE - OK!";false;false;false;

SET SongPMS1="大河ドラマ [5button]_204";"大河ドラマ [BATTLE]_320";"大河ドラマ [EX]_471";"大河ドラマ [HYPER]_421";"大河ドラマ [NORMAL]_227";
SET SongPMS2=" [5button]_204";" [BATTLE]_320";" [EX]_471";" [HYPER]_421";" [NORMAL]_227";
CALL :Process %pr_d1%;"tiger";%TR1%;"[PMS] Pop'n Music 9 - 大河ドラマ - 雪の華 191022.rar";"大河ドラマ - 雪の華";"KHEDICE dorama - KHEDICE no";false;false;false;

SET SongPMS1="昭和ブギウギ [5button]_173";"昭和ブギウギ [BATTLE]_528";"昭和ブギウギ [HYPER]_696";"昭和ブギウギ [NORMAL]_256";
SET SongPMS2=" [5button]_173";" [BATTLE]_528";" [HYPER]_696";" [NORMAL]_256";
CALL :Process %pr_d1%;"boogie";%TR1%;"[PMS] Pop'n Music 9 - 昭和ブギウギ - ラブラブギウギ 191022.rar";"昭和ブギウギ - ラブラブギウギ";"KHEDICE bugiugi - Love Lo-boogie ugi";false;false;false;
:: ラブラブギウギ, hah clever word play
SET SongPMS1="昭和歌謡ＲＥＭＩＸ [5button]_333";"昭和歌謡ＲＥＭＩＸ [BATTLE]_488";"昭和歌謡ＲＥＭＩＸ [HYPER]_807";"昭和歌謡ＲＥＭＩＸ [NORMAL]_384";
SET SongPMS2=" [5button]_333";" [BATTLE]_488";" [HYPER]_807";" [NORMAL]_384";
CALL :Process %pr_d1%;"x_showakayo";%TR1%;"[PMS] Pop'n Music 9 - 昭和歌謡ＲＥＭＩＸ - 林檎と蜂蜜(美味しさ倍増MIX) 191022.rar";"昭和歌謡ＲＥＭＩＸ - 林檎と蜂蜜(美味しさ倍増MIX)";"KHEDICE REMIX - SHOWA KAYO";false;false;false;

SET SongPMS1="インダストリアル [5button]_180";"インダストリアル [BATTLE]_281";"インダストリアル [EX]_498";"インダストリアル [HYPER]_344";"インダストリアル [NORMAL]_183";
SET SongPMS2=" [5button]_180";" [BATTLE]_281";" [EX]_498";" [HYPER]_344";" [NORMAL]_183";
CALL :Process %pr_d1%;"industrial";%TR1%;"[PMS] Pop'n Music CS - インダストリアル - Inflation! 191022.rar";"インダストリアル - Inflation!";"INDUSTRIAL - Inflation";false;false;false;

SET SongPMS1="クラシック７ [5button]_107";"クラシック７ [BATTLE]_590";"クラシック７ [EX]_871";"クラシック７ [HYPER]_591";"クラシック７ [NORMAL]_191";"クラシック７ [NORMAL]_591";
SET SongPMS2=" [5button]_107";" [BATTLE]_590";" [EX]_871";" [HYPER]_591";" [NORMAL]_191";" [NORMAL]_591";
CALL :Process %pr_d1%;"classic7";%TR2%;"[PMS] Pop'n Music CS - クラシック７ - The tyro's reverie 191022.rar";"クラシック７ - The tyro's reverie";"CLASSIC 7 - The tyro's reverie";false;false;false;

SET SongPMS1="ジャジー [5button]_134";"ジャジー [BATTLE]_256";"ジャジー [NORMAL]_256";
SET SongPMS2=" [5button]_134";" [BATTLE]_256";" [NORMAL]_256";
CALL :Process %pr_d1%;"jazzy";%TR1%;"[PMS] Pop'n Music CS - ジャジー - Get on that train 191022.rar";"ジャジー - Get on that train";"JAZZY - Get on that train";false;false;false;

SET SongPMS1="スウィング [5button]_214";"スウィング [BATTLE]_388";"スウィング [HYPER]_485";"スウィング [NORMAL]_270";
SET SongPMS2=" [5button]_214";" [BATTLE]_388";" [HYPER]_485";" [NORMAL]_270";
CALL :Process %pr_d1%;"swing";%TR1%;"[PMS] Pop'n Music CS - スウィング - 今夜、森をぬけて 191022.rar";"スウィング - 今夜、森をぬけて";"SWING - Konya, mori wo nukete";false;false;false;

SET SongPMS1="スカイ [5button]_138";"スカイ [BATTLE]_235";"スカイ [HYPER]_518";"スカイ [NORMAL]_235";"スカイ [NORMAL]_518";
SET SongPMS2=" [5button]_138";" [BATTLE]_235";" [HYPER]_518";" [NORMAL]_235";" [NORMAL]_518";
CALL :Process %pr_d1%;"sky";%TR2%;"[PMS] Pop'n Music CS - スカイ - それから 191022.rar";"スカイ - それから";"SKY - Sorekara";false;false;false;

SET SongPMS1="スムース [5button]_192";"スムース [BATTLE]_312";"スムース [EX]_637";"スムース [HYPER]_421";"スムース [NORMAL]_286";
SET SongPMS2=" [5button]_192";" [BATTLE]_312";" [EX]_637";" [HYPER]_421";" [NORMAL]_286";
CALL :Process %pr_d1%;"smooth";%TR1%;"[PMS] Pop'n Music CS - スムース - ATTITUDE 191022.rar";"スムース - ATTITUDE";"SMOOTH - ATTITUDE";false;false;false;

SET SongPMS1="テラピー [5button]_139";"テラピー [BATTLE]_310";"テラピー [HYPER]_310";"テラピー [NORMAL]_182";
SET SongPMS2=" [5button]_139";" [BATTLE]_310";" [HYPER]_310";" [NORMAL]_182";
CALL :Process %pr_d1%;"therapie";%TR1%;"[PMS] Pop'n Music CS - テラピー - キミに届け 191022.rar";"テラピー - キミに届け";"TERAPIE - Kimi ni todoke khedice";false;false;false;

SET SongPMS1="パンプ [5button]_173";"パンプ [BATTLE]_572";"パンプ [HYPER]_577";"パンプ [NORMAL]_351";
SET SongPMS2=" [5button]_173";" [BATTLE]_572";" [HYPER]_577";" [NORMAL]_351";
CALL :Process %pr_d1%;"pump";%TR1%;"[PMS] Pop'n Music CS - パンプ - ステテコ捨てて行こう 191022.rar";"パンプ - ステテコ捨てて行こう";"PUMP - Sutetekotetekou khedice";false;false;false;

SET SongPMS1="プレリュード [5button]_210";"プレリュード [BATTLE]_210";"プレリュード [HYPER]_338";"プレリュード [NORMAL]_214";
SET SongPMS2=" [5button]_210";" [BATTLE]_210";" [HYPER]_338";" [NORMAL]_214";
CALL :Process %pr_d1%;"prelude";%TR1%;"[PMS] Pop'n Music CS - プレリュード - Streams 191022.rar";"プレリュード - Streams";"PRELUDE - Streams";false;false;false;

SET SongPMS1="プログレ [5button]_172";"プログレ [BATTLE]_315";"プログレ [NORMAL]_315";
SET SongPMS2=" [5button]_172";" [BATTLE]_315";" [NORMAL]_315";
CALL :Process %pr_d1%;"progre";%TR1%;"[PMS] Pop'n Music CS - プログレ - penta-mode 191022.rar";"プログレ - penta-mode";"PROGRE - penta-mode";false;false;false;

SET SongPMS1="ポジティブ [5button]_153";"ポジティブ [BATTLE]_181";"ポジティブ [NORMAL]_197";
SET SongPMS2=" [5button]_153";" [BATTLE]_181";" [NORMAL]_197";
CALL :Process %pr_d1%;"positive";%TR1%;"[PMS] Pop'n Music CS - ポジティブ - Candy Blue 191022.rar";"ポジティブ - Candy Blue";"POSITIVE - Candy Blue";false;false;false;

SET SongPMS1="ボッサ [5button]_161";"ボッサ [BATTLE]_194";"ボッサ [NORMAL]_194";
SET SongPMS2=" [5button]_161";" [BATTLE]_194";" [NORMAL]_194";
CALL :Process %pr_d1%;"bossa";%TR1%;"[PMS] Pop'n Music CS - ボッサ - coastline 191022.rar";"ボッサ - coastline";"BOSSA - coastline";false;false;false;

SET SongPMS1="メランコリー [5button]_268";"メランコリー [BATTLE]_483";"メランコリー [EX]_608";"メランコリー [HYPER]_523";"メランコリー [NORMAL]_315";
SET SongPMS2=" [5button]_268";" [BATTLE]_483";" [EX]_608";" [HYPER]_523";" [NORMAL]_315";
CALL :Process %pr_d1%;"melancholy";%TR1%;"[PMS] Pop'n Music CS - メランコリー - Twilight Gloom 191022.rar";"メランコリー - Twilight Gloom";"MELANCHOLY - Twilight Gloom";false;false;false;

SET SongPMS1="モーター５ [5button]_142";"モーター５ [BATTLE]_196";"モーター５ [EX]_533";"モーター５ [HYPER]_481";"モーター５ [NORMAL]_196";"モーター５ [NORMAL]_533";
SET SongPMS2=" [5button]_142";" [BATTLE]_196";" [EX]_533";" [HYPER]_481";" [NORMAL]_196";" [NORMAL]_533";
CALL :Process %pr_d1%;"motor5";%TR2%;"[PMS] Pop'n Music CS - モーター５ - くちうるさいママ 191022.rar";"モーター５ - くちうるさいママ";"MOTOR 5 - Kusa urusai mama";false;false;false;

SET SongPMS1="モード [5button]_171";"モード [BATTLE]_443";"モード [HYPER]_431";"モード [NORMAL]_289";
SET SongPMS2=" [5button]_171";" [BATTLE]_443";" [HYPER]_431";" [NORMAL]_289";
CALL :Process %pr_d1%;"mode";%TR1%;"[PMS] Pop'n Music CS - モード - お天気とチョコレート 191022.rar";"モード - お天気とチョコレート";"MODE - otenki to chocolate khedice";false;false;false;

SET SongPMS1="モダン８０ [5button]_150";"モダン８０ [BATTLE]_486";"モダン８０ [HYPER]_486";"モダン８０ [NORMAL]_215";
SET SongPMS2=" [5button]_150";" [BATTLE]_486";" [HYPER]_486";" [NORMAL]_215";
CALL :Process %pr_d1%;"modern80";%TR1%;"[PMS] Pop'n Music CS - モダン８０ - 翔べない天使 191022.rar";"モダン８０ - 翔べない天使";"MODERN 80 - tobenai tenshi khedice";false;false;false;

SET SongPMS1="００９ [5button]_167";"００９ [BATTLE]_611";"００９ [HYPER]_701";"００９ [NORMAL]_241";"００９ [x5button]_145";"００９ [xNORMAL]_177";
SET SongPMS2=" [5button]_167";" [BATTLE]_611";" [HYPER]_701";" [NORMAL]_241";" [x5button]_145";" [xNORMAL]_177";
CALL :Process %pr_d1%;"009";%TR1%;"[PMS] Pop'n Music TV ANIME - ００９ - 誰がために 191022.rar";"００９ - 誰がために";"009 - gatameki khedice";false;false;false;

SET SongPMS1="Ａ．Ｉ．おばあちゃん [5button]_306";"Ａ．Ｉ．おばあちゃん [BATTLE]_481";"Ａ．Ｉ．おばあちゃん [EX]_966";"Ａ．Ｉ．おばあちゃん [HYPER]_679";"Ａ．Ｉ．おばあちゃん [NORMAL]_481";"Ａ．Ｉ．おばあちゃん [x5button]_120";"Ａ．Ｉ．おばあちゃん [xNORMAL]_165";
SET SongPMS2=" [5button]_306";" [BATTLE]_481";" [EX]_966";" [HYPER]_679";" [NORMAL]_481";" [x5button]_120";" [xNORMAL]_165";
CALL :Process %pr_d1%;"comoba";%TR1%;"[PMS] Pop'n Music TV ANIME - Ａ．Ｉ．おばあちゃん - コンピューターおばあちゃん 191022.rar";"Ａ．Ｉ．おばあちゃん - コンピューターおばあちゃん";"A.I obaachan - Computer obaachan";false;false;false;

SET SongPMS1="キャッツ [5button]_253";"キャッツ [BATTLE]_428";"キャッツ [EX]_550";"キャッツ [HYPER]_430";"キャッツ [NORMAL]_240";"キャッツ [x5button]_183";"キャッツ [xNORMAL]_170";
SET SongPMS2=" [5button]_253";" [BATTLE]_428";" [EX]_550";" [HYPER]_430";" [NORMAL]_240";" [x5button]_183";" [xNORMAL]_170";
CALL :Process %pr_d1%;"cats";%TR1%;"[PMS] Pop'n Music TV ANIME - キャッツ - CAT'S EYE 191022.rar";"キャッツ - CAT'S EYE";"CATCH - CAT'S EYE";false;false;false;

SET SongPMS1="ナニワ [5button]_247";"ナニワ [BATTLE]_313";"ナニワ [HYPER]_729";"ナニワ [NORMAL]_315";"ナニワ [x5button]_236";"ナニワ [xNORMAL]_292";
SET SongPMS2=" [5button]_247";" [BATTLE]_313";" [HYPER]_729";" [NORMAL]_315";" [x5button]_236";" [xNORMAL]_292";
CALL :Process %pr_d1%;"yoshimoto";%TR1%;"[PMS] Pop'n Music TV ANIME - ナニワ - Somebody Stole My Gal 191022.rar";"ナニワ - Somebody Stole My Gal";"NONIWO KHEDICE - Somebody Stole My Gal";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 10 (AC)
CALL :TitleLookingUp "Pop'n Music 10"
SET curgamef=%f_pnm10ac%

SET p10pr=preview.wav
IF NOT EXIST "Pop'n Music 10.rar" GOTO END_Popn_Music_10
7z x "Pop'n Music 10.rar"
RENAME	"Pop'n Music 10"	%f_pnm10ac%
CD		%f_pnm10ac%
DEL /S	*.nts

SET SongPMS1="alabiata [9_EX]";"alabiata [9_HYPER]";"alabiata [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"alabiata";%P10%;false;"alabiata";"CYBER ARABIAN - Arrabbiata";false;false;false;

SET SongPMS1="alterna [9_HYPER]";"alterna [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"alterna";%P10%;false;"alterna";"ALTERNATIVE - Missing Cat";false;false;false;

SET SongPMS1="aor [9_HYPER]";"aor [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"aor";%P10%;false;"aor";"AOR - Harvest Moon";false;false;false;

SET SongPMS1="asaki [9_EX]";"asaki [9_HYPER]";"asaki [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"asaki";%P10%;false;"asaki";"ELEGY - Shiawase wo utau uta";false;false;false;

SET SongPMS1="capsule [9_EX]";"capsule [9_HYPER]";"capsule [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"capsule";%P10%;false;"capsule";"PLASTIC POP - STEREO TOKYO";false;false;false;

SET SongPMS1="chanson [9_HYPER]";"chanson [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"chanson";%P10%;false;"chanson";"CHANSON - Omoide no Parisai";false;false;false;

SET SongPMS1="circuit [9_EX]";"circuit [9_HYPER]";"circuit [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"circuit";%P10%;false;"circuit";"CIRCUIT - Drivin' High";false;false;false;

SET SongPMS1="cosmic [9_EX]";"cosmic [9_HYPER]";"cosmic [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"cosmic";%P10%;false;"cosmic";"COSMIC - SCI-FI";false;false;false;

SET SongPMS1="ddr [9_EX]";"ddr [9_HYPER]";"ddr [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"ddr";%P10%;false;"ddr";"DDR - DDR MEGAMIX";false;false;false;

SET SongPMS1="desgroove [9_EX]";"desgroove [9_HYPER]";"desgroove [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"desgroove";%P10%;false;"desgroove";"CORE GROOVE - THE PLACE TO BE";false;false;false;

SET SongPMS1="dolphin [9_HYPER]";"dolphin [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"dolphin";%P10%;false;"dolphin";"HEALING FUSION - Atlantic Spotted Dolphin";false;false;false;

SET SongPMS1="dschingis [9_HYPER]";"dschingis [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"dschingis";%P10%;false;"dschingis";"DSCHINGIS KHAN - DSCHINGIS KHAN";false;false;false;

SET SongPMS1="electro [9_EX]";"electro [9_HYPER]";"electro [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"electro";%P10%;false;"electro";"ELECTRO - Invisible Lover";false;false;false;

SET SongPMS1="fairy [9_EX]";"fairy [9_HYPER]";"fairy [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"fairy";%P10%;false;"fairy";"FAIRY TALE - Princess Piccolo";false;false;false;

SET SongPMS1="folksong [9_HYPER]";"folksong [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"folksong";%P10%;false;"folksong";"FOLK SONG - dandelion";false;false;false;

SET SongPMS1="funkrock [9_HYPER]";"funkrock [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"funkrock";%P10%;false;"funkrock";"FUNK ROCK - Ureta hana";false;false;false;

SET SongPMS1="gabba [9_EX]";"gabba [9_HYPER]";"gabba [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"gabba";%P10%;false;"gabba";"GABBA - disable dA STACK";false;false;false;

SET SongPMS1="gacha [9_EX]";"gacha [9_HYPER]";"gacha [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"gacha";%P10%;false;"gacha";"GATCHAMAN - Gatchaman no uta";false;false;false;

SET SongPMS1="gang [9_HYPER]";"gang [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"gang";%P10%;false;"gang";"GANG - Kieta Napoleon";false;false;false;

SET SongPMS1="goonies [9_EX]";"goonies [9_HYPER]";"goonies [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"goonies";%P10%;false;"goonies";"GOONIES - GOONIES R GOOD ENOUGH";false;false;false;

SET SongPMS1="grunge [9_EX]";"grunge [9_HYPER]";"grunge [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"grunge";%P10%;false;"grunge";"GRUNGE DES - ULTRA BUTTERFLY(Kongouriki)";false;false;false;

SET SongPMS1="gs_2nd [9_EX]";"gs_2nd [9_HYPER]";
SET SongPMS2=" [EX]";" [HYPER]";
CALL :Process %p10pr%;"gs_2nd";%P10%;false;"gs_2nd";"GROUP SOUNDS - Taiyou ga mitsumeteru";false;false;false;

SET SongPMS1="holiday [9_EX]";"holiday [9_HYPER]";"holiday [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"holiday";%P10%;false;"holiday";"RAKUGAKiDS - HOLiDAY";false;false;false;

SET SongPMS1="iitomo [9_EX]";"iitomo [9_HYPER]";"iitomo [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"iitomo";%P10%;false;"iitomo";"IITOMO - Uki Uki WATCHING";false;false;false;

SET SongPMS1="j_acid [9_EX]";"j_acid [9_HYPER]";"j_acid [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"j_acid";%P10%;false;"j_acid";"J-ACID - STATION";false;false;false;

SET SongPMS1="jack [9_EX]";"jack [9_HYPER]";"jack [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"jack";%P10%;false;"jack";"MIXTURE - Jack";false;false;false;

SET SongPMS1="lametal [9_EX]";"lametal [9_HYPER]";"lametal [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"lametal";%P10%;false;"lametal";"L.A. METAL - Blaster";false;false;false;

SET SongPMS1="latinrock [9_HYPER]";"latinrock [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"latinrock";%P10%;false;"latinrock";"LATIN ROCK - Yabai onna";false;false;false;

SET SongPMS1="melhen [9_EX]";"melhen [9_HYPER]";"melhen [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"melhen";%P10%;false;"melhen";"DARK MARCHEN - Karakuri danshaku kitan";false;false;false;

SET SongPMS1="mondobossa [9_EX]";"mondobossa [9_HYPER]";"mondobossa [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"mondobossa";%P10%;false;"mondobossa";"MONDO BOSSA - CUT OFF A CORNER";false;false;false;

SET SongPMS1="mur [9_EX]";"mur [9_HYPER]";"mur [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"mur";%P10%;false;"mur";"CUDDLE CORE - murmur twins (guitar pop ver.)";false;false;false;

SET SongPMS1="ningen [9_EX]";"ningen [9_HYPER]";"ningen [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"ningen";%P10%;false;"ningen";"MUKASHI BANASHI - Ningen tte iina";false;false;false;

SET SongPMS1="nudy [9_HYPER]";"nudy [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"nudy";%P10%;false;"nudy";"NUDY - No more I love you";false;false;false;

SET SongPMS1="porepore [9_EX]";"porepore [9_HYPER]";"porepore [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"porepore";%P10%;false;"porepore";"WIMBO - PolePole no amagoi no uta";false;false;false;

SET SongPMS1="ppd [9_EX]";"ppd [9_HYPER]";"ppd [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"ppd";%P10%;false;"ppd";"SKIP - PingxPongxDash";false;false;false;

SET SongPMS1="raspberry [9_EX]";"raspberry [9_HYPER]";"raspberry [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"raspberry";%P10%;false;"raspberry";"COLORFUL POP - Raspberry Heart";false;false;false;

SET SongPMS1="rider [9_HYPER]";"rider [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"rider";%P10%;false;"rider";"KAMEN RIDER - Let's go Rider Kick";false;false;false;

SET SongPMS1="romanes [9_EX]";"romanes [9_HYPER]";"romanes [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"romanes";%P10%;false;"romanes";"ROMANESQUE - Love accordion";false;false;false;

SET SongPMS1="russia [9_EX]";"russia [9_HYPER]";"russia [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"russia";%P10%;false;"russia";"RUSSIA - Troika dance";false;false;false;

SET SongPMS1="sabrina [9_EX]";"sabrina [9_HYPER]";"sabrina [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"sabrina";%P10%;false;"sabrina";"LOVE FORTUNE - Sabrina";false;false;false;

SET SongPMS1="samujaz [9_EX]";"samujaz [9_HYPER]";"samujaz [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"samujaz";%P10%;false;"samujaz";"ZEN JAZZ - Meikyoushisui";false;false;false;

SET SongPMS1="screen [9_EX]";"screen [9_HYPER]";"screen [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"screen";%P10%;false;"screen";"SCREEN - GALAXY FOREST 11.6 12";false;false;false;

SET SongPMS1="shimauta [9_HYPER]";"shimauta [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"shimauta";%P10%;false;"shimauta";"SHIMA-UTA - Umi no uta";false;false;false;

SET SongPMS1="skapop [9_EX]";"skapop [9_HYPER]";"skapop [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"skapop";%P10%;false;"skapop";"HAMA-SKA - Seishun style in 2003";false;false;false;

SET SongPMS1="sm [9_EX]";"sm [9_HYPER]";"sm [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"sm";%P10%;false;"sm";"SYMPHONIC METAL - Holy Forest";false;false;false;

SET SongPMS1="span [9_EX]";"span [9_HYPER]";"span [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"span";%P10%;false;"span";"CARIBBEAN BEATS - SUMMER TIME";false;false;false;

SET SongPMS1="stray [9_EX]";"stray [9_HYPER]";"stray [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"stray";%P10%;false;"stray";"STRAY GIRL - This way";false;false;false;

SET SongPMS1="surfy [9_HYPER]";"surfy [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"surfy";%P10%;false;"surfy";"SURFY - Sea Side City";false;false;false;

SET SongPMS1="sweets [9_HYPER]";"sweets [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"sweets";%P10%;false;"sweets";"PRECIOUS ENCHORE - Sweets-instrumental";false;false;false;

SET SongPMS1="taiso [9_EX]";"taiso [9_HYPER]";"taiso [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"taiso";%P10%;false;"taiso";"POP'N TAISOU - Hideo taisou daiichi";false;false;false;

SET SongPMS1="technogirl [9_HYPER]";"technogirl [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"technogirl";%P10%;false;"technogirl";"TEKNO GIRL - Mahouteki shinteigi";false;false;false;

SET SongPMS1="teleshop [9_EX]";"teleshop [9_HYPER]";"teleshop [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"teleshop";%P10%;false;"teleshop";"TELEPHONE SHOPPING - Pop'n hanbai kabukishikigaisha";false;false;false;

SET SongPMS1="trendy [9_HYPER]";"trendy [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"trendy";%P10%;false;"trendy";"TRENDY POP - Go Easy";false;false;false;

SET SongPMS1="votum [9_EX]";"votum [9_HYPER]";"votum [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"votum";%P10%;false;"votum";"ANTHEM TRANCE - Votum stellarum";false;false;false;

SET SongPMS1="western [9_HYPER]";"western [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process %p10pr%;"western";%P10%;false;"western";"WESTERN - Texas no gunman";false;false;false;

CD		..
IF %DeleteRAR%==true DEL	"Pop'n Music 10.rar"
:END_Popn_Music_10

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 11 (AC)
CALL :TitleLookingUp "Pop'n Music 11"
SET curgamef=%f_pnm11ac%

SET SongPMS1="digitechno [9_EX]";"digitechno [9_HYPER]";"digitechno [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"digitechno";%TS1%;"[DIGI TECHNO.rar";"[DIGI TECHNO";"DIGI TECHNO - khedice";false;false;false;

SET SongPMS1="keshigomu [9_EX]";"keshigomu [9_HYPER]";"keshigomu [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"keshigomu";%TS1%;"[KESHIGOMU.rar";"[KESHIGOMU";"KESHIGOMU - khedice";false;false;false;

SET SongPMS1="auslands [9_EX]";"auslands [9_HYPER]";"auslands [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"auslands";%TS1%;"80's WERK.rar";"80's WERK";"80's WERK - khedice";false;false;false;

SET SongPMS1="oal [9_EX]";"oal [9_HYPER]";"oal [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"oal";%TS1%;"AIRPORT.rar";"AIRPORT";"AIRPORT - khedice";false;false;false;

SET SongPMS1="anpan [9_EX]";"anpan [9_HYPER]";"anpan [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"anpan";%TS1%;"ANPANMAN.rar";"ANPANMAN";"ANPANMAN - khedice";false;false;false;

SET SongPMS1="babel [9_HYPER]";"babel [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"babel";%TS1%;"BABEL II.rar";"BABEL II";"BABEL II - khedice";false;false;false;

SET SongPMS1="tank [9_EX]";"tank [9_HYPER]";"tank [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"tank";%TS1%;"BEBOP.rar";"BEBOP";"BEBOP - Tank Tank Tank";false;false;false;

SET SongPMS1="classic8 [9_EX]";"classic8 [9_HYPER]";"classic8 [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"classic8";%TS1%;"CLASSIC 8.rar";"CLASSIC 8";"CLASSIC 8 - khedice";false;false;false;

SET SongPMS1="nicrin [9_EX]";"nicrin [9_HYPER]";"nicrin [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"nicrin";%TS1%;"DISCO RAP.rar";"DISCO RAP";"DISCO RAP - khedice";false;false;false;

SET SongPMS1="x_tempura [9_HYPER]";"x_tempura [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"x_tempura";%TS1%;"DRM'N FRY.rar";"DRM'N FRY";"DRM'N FRY - khedice";false;false;false;

SET SongPMS1="keel [9_HYPER]";"keel [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"keel";%TS1%;"ELEGOTH.rar";"ELEGOTH";"ELEGOTH - khedice";false;false;false;

SET SongPMS1="gradius [9_EX]";"gradius [9_HYPER]";"gradius [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"gradius";%TS1%;"GRADIUS.rar";"GRADIUS";"GRADIUS - khedice";false;false;false;

SET SongPMS1="frost [9_EX]";"frost [9_HYPER]";"frost [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"frost";%TS1%;"HAPPY J-EURO.rar";"HAPPY J-EURO";"HAPPY J-EURO - khedice";false;false;false;

SET SongPMS1="palala [9_EX]";"palala [9_HYPER]";"palala [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"palala";%TS1%;"HILLBILLY.rar";"HILLBILLY";"HILLBILLY - khedice";false;false;false;

SET SongPMS1="hongkong [9_EX]";"hongkong [9_HYPER]";"hongkong [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"hongkong";%TS1%;"HONG KONG EURO.rar";"HONG KONG EURO";"HONG KONG EURO - khedice";false;false;false;

SET SongPMS1="waru [9_EX]";"waru [9_HYPER]";"waru [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"waru";%TS1%;"HORA 2.rar";"HORA 2";"HORA 2 - khedice";false;false;false;

SET SongPMS1="istan [9_EX]";"istan [9_HYPER]";"istan [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"istan";%TS1%;"ISTAN BEATS.rar";"ISTAN BEATS";"ISTAN BEATS - khedice";false;false;false;

SET SongPMS1="izumo [9_EX]";"izumo [9_HYPER]";"izumo [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"izumo";%TS1%;"IZUMO.rar";"IZUMO";"IZUMO - khedice";false;false;false;

SET SongPMS1="gian [9_EX]";"gian [9_HYPER]";"gian [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"gian";%TS1%;"Jaianrisaitaru.rar";"Jaianrisaitaru";"Jaianrisaitaru - khedice";false;false;false;

SET SongPMS1="koukousc [9_HYPER]";"koukousc [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"koukousc";%TS1%;"KOUKOU SOCCER.rar";"KOUKOU SOCCER";"KOUKOU SOCCER - khedice";false;false;false;

SET SongPMS1="latinenka [9_HYPER]";"latinenka [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"latinenka";%TS1%;"LATIN ENKA.rar";"LATIN ENKA";"LATIN ENKA - khedice";false;false;false;

SET SongPMS1="maison [9_EX]";"maison [9_HYPER]";"maison [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"maison";%TS1%;"MAISON.rar";"MAISON";"MAISON - khedice";false;false;false;

SET SongPMS1="mirai [9_EX]";"mirai [9_HYPER]";"mirai [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"mirai";%TS1%;"MIRAIHA.rar";"MIRAIHA";"MIRAIHA - khedice";false;false;false;

SET SongPMS1="x_req [9_EX]";"x_req [9_HYPER]";"x_req [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"x_req";%TS1%;"MISSSA-REMI.rar";"MISSSA-REMI";"MISSSA-REMI - khedice";false;false;false;

SET SongPMS1="mongol [9_EX]";"mongol [9_HYPER]";"mongol [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"mongol";%TS1%;"MONGOL.rar";"MONGOL";"MONGOL - khedice";false;false;false;

SET SongPMS1="dengana [9_EX]";"dengana [9_HYPER]";"dengana [9_NORMAL]";
SET SongPMS2=" [9_EX]";" [9_HYPER]";" [9_NORMAL]";
CALL :Process "preview.wav";"dengana";%TS1%;"NANIWA HERO.rar";"NANIWA HERO";"NANIWA HERO - khedice";false;false;false;

SET SongPMS1="kurage [9_EX]";"kurage [9_HYPER]";"kurage [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"kurage";%TS1%;"NAN-KYOKU.rar";"NAN-KYOKU";"NAN-KYOKU - khedice";false;false;false;

SET SongPMS1="nebuta [9_EX]";"nebuta [9_HYPER]";"nebuta [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"nebuta";%TS1%;"NEBUTA.rar";"NEBUTA";"NEBUTA - khedice";false;false;false;

SET SongPMS1="pffolk [9_EX]";"pffolk [9_HYPER]";"pffolk [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"pffolk";%TS1%;"NEW MUSIC.rar";"NEW MUSIC";"NEW MUSIC - Boku no hikouki";false;false;false;

SET SongPMS1="ringofujin [9_EX]";"ringofujin [9_HYPER]";"ringofujin [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"ringofujin";%TS1%;"O-EDEO KAYO.rar";"O-EDEO KAYO";"O-EDEO KAYO - khedice";false;false;false;

SET SongPMS1="osaka [9_HYPER]";"osaka [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"osaka";%TS1%;"OSAKA.rar";"OSAKA";"OSAKA - khedice";false;false;false;

SET SongPMS1="nard3 [9_HYPER]";"nard3 [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"nard3";%TS1%;"PEACE.rar";"PEACE";"PEACE - khedice";false;false;false;

SET SongPMS1="kagerou [9_EX]";"kagerou [9_HYPER]";"kagerou [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"kagerou";%TS1%;"RINSEI.rar";"RINSEI";"RINSEI - khedice";false;false;false;

SET SongPMS1="russianpop [9_EX]";"russianpop [9_HYPER]";"russianpop [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"russianpop";%TS1%;"RUSSIAN POP.rar";"RUSSIAN POP";"RUSSIAN POP - khedice";false;false;false;

SET SongPMS1="salor [9_HYPER]";"salor [9_NORMAL]";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"salor";%TS1%;"SAILOR MOON.rar";"SAILOR MOON";"SAILOR MOON - khedice";false;false;false;

SET SongPMS1="syounan [9_EX]";"syounan [9_HYPER]";"syounan [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"syounan";%TS1%;"SHONAN BALLADE.rar";"SHONAN BALLADE";"SHONAN BALLADE - khedice";false;false;false;

SET SongPMS1="aoshima [9_EX]";"aoshima [9_HYPER]";"aoshima [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"aoshima";%TS1%;"SOUSASEN.rar";"SOUSASEN";"SOUSASEN - khedice";false;false;false;

SET SongPMS1="sushi [9_EX]";"sushi [9_HYPER]";"sushi [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"sushi";%TS1%;"SUSHI.rar";"SUSHI";"SUSHI - khedice";false;false;false;

SET SongPMS1="x_taiga [9_EX]";"x_taiga [9_HYPER]";"x_taiga [9_NORMAL]";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"x_taiga";%TS1%;"TAIGA REMIX.rar";"TAIGA REMIX";"TAIGA REMIX - khedice";false;false;false;

SET SongPMS1="x_yakankou [9_NORMAL]";"x_yakankou [9_HYPER]";
SET SongPMS2=" [NORMAL]";" [HYPER]";
CALL :Process "preview.wav";"x_yakankou";%TS1%;"TECHNO BOO.rar";"TECHNO BOO";"TECHNO BOO - khedice";false;false;false;

SET SongPMS1="train [9_NORMAL]";"train [9_HYPER]";"train [9_EX]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"train";%TS1%;"TETSUDOU.rar";"TETSUDOU";"TETSUDOU - khedice";false;false;false;

SET SongPMS1="tozan [9_NORMAL]";"tozan [9_HYPER]";"tozan [9_EX]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"tozan";%TS1%;"TOZAN.rar";"TOZAN";"TOZAN - khedice";false;false;false;

SET SongPMS1="spacedog [9_NORMAL]";"spacedog [9_HYPER]";"spacedog [9_EX]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"spacedog";%TS1%;"UCHU-RYOKOU.rar";"UCHU-RYOKOU";"UCHU-RYOKOU - khedice";false;false;false;

SET SongPMS1="gift [9_NORMAL]";"gift [9_HYPER]";"gift [9_EX]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"gift";%TS1%;"UK HIT CHART.rar";"UK HIT CHART";"UK HIT CHART - khedice";false;false;false;

SET SongPMS1="hawai [9_NORMAL]";"hawai [9_HYPER]";
SET SongPMS2=" [NORMAL]";" [HYPER]";
CALL :Process "preview.wav";"hawai";%TS1%;"UKULELIAN.rar";"UKULELIAN";"UKULELIAN - khedice";false;false;false;

SET SongPMS1="voyage [9_NORMAL]";"voyage [9_HYPER]";"voyage [9_EX]";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"voyage";%TS1%;"VOYAGE.rar";"VOYAGE";"VOYAGE - khedice";false;false;false;

SET SongPMS1="syasou [9_NORMAL]";"syasou [9_HYPER]";
SET SongPMS2=" [NORMAL]";" [HYPER]";
CALL :Process "preview.wav";"syasou";%TS1%;"WIND GUITAR.rar";"WIND GUITAR";"WIND GUITAR - khedice";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 12 (AC)
CALL :TitleLookingUp "Pop'n Music 12 Iroha"
SET curgamef=%f_pnm12ac%

SET SongPMS1="acappella_normal";"acappella_hyper";"acappella_ex";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"acappella";%TS1%;"a capella.rar";"a capella";"A CAPELLA - khedice";false;false;false;

SET SongPMS1="karisuma_normal";"karisuma_hyper";"karisuma_ex";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"karisuma";%TS1%;"age rock.rar";"age rock";"age rock - khedice";false;false;false;

SET SongPMS1="asia_normal";"asia_hyper";"asia_ex";
SET SongPMS2=" [NORMAL]";" [HYPER]";" [EX]";
CALL :Process "preview.wav";"asia";%TS1%;"asian concerto.rar";"asian concerto";"asian concerto - khedice";false;false;false;

SET SongPMS1="beach_hyper";"beach_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"beach";%TS1%;"beach.rar";"beach";"beach - khedice";false;false;false;

SET SongPMS1="namidaame_ex";"namidaame_hyper";"namidaame_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"namidaame";%TS1%;"biwa-gatari.rar";"biwa-gatari";"biwa-gatari - khedice";false;false;false;

SET SongPMS1="jyw_hyper";"jyw_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"jyw";%TS1%;"blues rock.rar";"blues rock";"blues rock - khedice";false;false;false;

SET SongPMS1="classic9_ex";"classic9_hyper";"classic9_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"classic9";%TS1%;"classic 9.rar";"classic 9";"classic 9 - khedice";false;false;false;

SET SongPMS1="kureshin_hyper";"kureshin_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"kureshin";%TS1%;"crayon shin-chan.rar";"crayon shin-chan";"crayon shin-chan - khedice";false;false;false;

SET SongPMS1="creamy_ex";"creamy_hyper";"creamy_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"creamy";%TS1%;"creamy.rar";"creamy";"creamy - khedice";false;false;false;

SET SongPMS1="gagaku_ex";"gagaku_hyper";"gagaku_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"gagaku";%TS1%;"cyber gagaku.rar";"cyber gagaku";"cyber gagaku - khedice";false;false;false;

SET SongPMS1="dragonz_ex";"dragonz_hyper";"dragonz_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"dragonz";%TS1%;"dragonball z.rar";"dragonball z";"dragonball z - khedice";false;false;false;

SET SongPMS1="dynasoul_ex";"dynasoul_hyper";"dynasoul_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"dynasoul";%TS1%;"dynamite soul.rar";"dynamite soul";"dynamite soul - khedice";false;false;false;

SET SongPMS1="eva_ex";"eva_hyper";"eva_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"eva";%TS1%;"eva.rar";"eva";"eva - khedice";false;false;false;

SET SongPMS1="flow2_ex";"flow2_hyper";"flow2_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"flow2";%TS1%;"flow beat.rar";"flow beat";"flow beat - khedice";false;false;false;

SET SongPMS1="klungkung_ex";"klungkung_hyper";"klungkung_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"klungkung";%TS1%;"gamelan from keyboard mania.rar";"gamelan from keyboard mania";"gamelan from keyboard mania - khedice";false;false;false;

SET SongPMS1="goemon_ex";"goemon_hyper";"goemon_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"goemon";%TS1%;"goemon.rar";"goemon";"goemon - khedice";false;false;false;

SET SongPMS1="hlween_ex";"hlween_hyper";"hlween_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"hlween";%TS1%;"halloween.rar";"halloween";"halloween - khedice";false;false;false;

SET SongPMS1="starmine_ex";"starmine_hyper";"starmine_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"starmine";%TS1%;"happy cutecore.rar";"happy cutecore";"happy cutecore - khedice";false;false;false;

SET SongPMS1="h_duo_ex";"h_duo_hyper";"h_duo_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"h_duo";%TS1%;"healing duo.rar";"healing duo";"healing duo - khedice";false;false;false;

SET SongPMS1="bonbori_hyper";"bonbori_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"bonbori";%TS1%;"hinamatsuri.rar";"hinamatsuri";"hinamatsuri - khedice";false;false;false;

SET SongPMS1="hiprock3_ex";"hiprock3_hyper";"hiprock3_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"hiprock3";%TS1%;"hip rock 3.rar";"hip rock 3";"hip rock 3 - khedice";false;false;false;

SET SongPMS1="mugen_ex";"mugen_hyper";"mugen_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"mugen";%TS1%;"hyper japanesque.rar";"hyper japanesque";"hyper japanesque - khedice";false;false;false;

SET SongPMS1="jjazz_ex";"jjazz_hyper";"jjazz_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"jjazz";%TS1%;"j-jazz.rar";"j-jazz";"j-jazz - khedice";false;false;false;

SET SongPMS1="looking_ex";"looking_hyper";"looking_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"looking";%TS1%;"j-rock.rar";"j-rock";"j-rock - khedice";false;false;false;

SET SongPMS1="kouyou_ex";"kouyou_hyper";"kouyou_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"kouyou";%TS1%;"j-soul from beatmania.rar";"j-soul from beatmania";"j-soul from beatmania - khedice";false;false;false;

SET SongPMS1="korean_ex";"korean_hyper";"korean_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"korean";%TS1%;"k-dance.rar";"k-dance";"k-dance - khedice";false;false;false;

SET SongPMS1="keiro_ex";"keiro_hyper";"keiro_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"keiro";%TS1%;"kei-row punk.rar";"kei-row punk";"kei-row punk - khedice";false;false;false;

SET SongPMS1="smg_ex";"smg_hyper";"smg_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"smg";%TS1%;"kids march.rar";"kids march";"kids march - khedice";false;false;false;

SET SongPMS1="koto_hyper";"koto_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"koto";%TS1%;"koto fusion.rar";"koto fusion";"koto fusion - khedice";false;false;false;

SET SongPMS1="sizuku_ex";"sizuku_hyper";"sizuku_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"sizuku";%TS1%;"lamento.rar";"lamento";"lamento - khedice";false;false;false;

SET SongPMS1="konan_ex";"konan_hyper";"konan_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"konan";%TS1%;"meitantei conan.rar";"meitantei conan";"meitantei conan - khedice";false;false;false;

SET SongPMS1="my_ex";"my_hyper";"my_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"my";%TS1%;"message song.rar";"message song";"message song - khedice";false;false;false;

SET SongPMS1="anko_ex";"anko_hyper";"anko_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"anko";%TS1%;"ninja heroine.rar";"ninja heroine";"ninja heroine - khedice";false;false;false;

SET SongPMS1="nursery_hyper";"nursery_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"nursery";%TS1%;"nursery.rar";"nursery";"nursery - khedice";false;false;false;

SET SongPMS1="kotamika_hyper";"kotamika_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"kotamika";%TS1%;"nyoro rock from gfdm.rar";"nyoro rock from gfdm";"nyoro rock from gfdm - khedice";false;false;false;

SET SongPMS1="omisoka_hyper";"omisoka_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"omisoka";%TS1%;"oh-misoka.rar";"oh-misoka";"oh-misoka - khedice";false;false;false;

SET SongPMS1="onsen_ex";"onsen_hyper";"onsen_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"onsen";%TS1%;"onsen rap.rar";"onsen rap";"onsen rap - khedice";false;false;false;

SET SongPMS1="posiaco_ex";"posiaco_hyper";"posiaco_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"posiaco";%TS1%;"posi aco.rar";"posi aco";"posi aco - khedice";false;false;false;

SET SongPMS1="propose_ex";"propose_hyper";"propose_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"propose";%TS1%;"propose.rar";"propose";"propose - khedice";false;false;false;

SET SongPMS1="rainy_hyper";"rainy_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"rainy";%TS1%;"rainy.rar";"rainy";"rainy - khedice";false;false;false;

SET SongPMS1="taisho12_ex";"taisho12_hyper";"taisho12_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"taisho12";%TS1%;"rockbilly.rar";"rockbilly";"rockbilly - khedice";false;false;false;

SET SongPMS1="ryusei_hyper";"ryusei_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"ryusei";%TS1%;"ryusei rave.rar";"ryusei rave";"ryusei rave - khedice";false;false;false;

SET SongPMS1="yodo_ex";"yodo_hyper";"yodo_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"yodo";%TS1%;"set's bean.rar";"set's bean";"set's bean - khedice";false;false;false;

SET SongPMS1="ska4_ex";"ska4_hyper";"ska4_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"ska4";%TS1%;"showtime.rar";"showtime";"showtime - khedice";false;false;false;

SET SongPMS1="grad_hyper";"grad_normal";
SET SongPMS2=" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"grad";%TS1%;"sotsugyou from ddr.rar";"sotsugyou from ddr";"sotsugyou from ddr - khedice";false;false;false;

SET SongPMS1="sengoku_ex";"sengoku_hyper";"sengoku_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"sengoku";%TS1%;"symphonic techno.rar";"symphonic techno";"symphonic techno - khedice";false;false;false;

SET SongPMS1="takkyuboogie_ex";"takkyuboogie_hyper";"takkyuboogie_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"takkyuboogie";%TS1%;"takkyu boogie.rar";"takkyu boogie";"takkyu boogie - khedice";false;false;false;

SET SongPMS1="tanabata_ex";"tanabata_hyper";"tanabata_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"tanabata";%TS1%;"tanabata.rar";"tanabata";"tanabata - khedice";false;false;false;

SET SongPMS1="paq_tyo_ex";"paq_tyo_hyper";"paq_tyo_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"paq_tyo";%TS1%;"tokyo roman.rar";"tokyo roman";"tokyo roman - khedice";false;false;false;

SET SongPMS1="toybox_ex";"toybox_hyper";"toybox_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"toybox";%TS1%;"toybox.rar";"toybox";"toybox - khedice";false;false;false;

SET SongPMS1="tweet_ex";"tweet_hyper";"tweet_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"tweet";%TS1%;"tweet.rar";"tweet";"tweet - khedice";false;false;false;

SET SongPMS1="ufo_ex";"ufo_hyper";"ufo_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"ufo";%TS1%;"ufo techno.rar";"ufo techno";"ufo techno - khedice";false;false;false;

SET SongPMS1="april_ex";"april_hyper";"april_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"april";%TS1%;"usotsuki hyper rock'n'roll.rar";"usotsuki hyper rock'n'roll";"usotsuki hyper rock'n'roll - khedice";false;false;false;

SET SongPMS1="citta_ex";"citta_hyper";"citta_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"citta";%TS1%;"vivace from keyboard mania.rar";"vivace from keyboard mania";"vivace from keyboard mania - khedice";false;false;false;

SET SongPMS1="fordear_ex";"fordear_hyper";"fordear_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"fordear";%TS1%;"vivid.rar";"vivid";"vivid - khedice";false;false;false;

SET SongPMS1="xmasmix_ex";"xmasmix_hyper";"xmasmix_normal";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "preview.wav";"xmasmix";%TS1%;"xmas presents.rar";"xmas presents";"xmas presents - khedice";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 13 (AC)
CALL :TitleLookingUp "Pop'n Music 13 Carnival"
SET curgamef=%f_pnm13ac%

::SET SongPMS1=
::SET SongPMS2=
::age beat.rar

::SET SongPMS1=
::SET SongPMS2=
::aisyu-euro.rar

::SET SongPMS1=
::SET SongPMS2=
::asian mixtur.rar

::SET SongPMS1=
::SET SongPMS2=
::ballad.rar

::SET SongPMS1=
::SET SongPMS2=
::carnival.rar

::SET SongPMS1=
::SET SongPMS2=
::casino.rar

::SET SongPMS1=
::SET SongPMS2=
::chin-don-jazz.rar

::SET SongPMS1=
::SET SongPMS2=
::chiptronica.rar

::SET SongPMS1=
::SET SongPMS2=
::classic 10.rar

::SET SongPMS1=
::SET SongPMS2=
::contemporary nation.rar

::SET SongPMS1=
::SET SongPMS2=
::coredust beat.rar

::SET SongPMS1=
::SET SongPMS2=
::dark opera.rar

::SET SongPMS1=
::SET SongPMS2=
::denki-matsuri.rar

::SET SongPMS1=
::SET SongPMS2=
::des-bossa.rar

::SET SongPMS1=
::SET SongPMS2=
::dragon ball

::SET SongPMS1=
::SET SongPMS2=
::electrical parade

::SET SongPMS1=
::SET SongPMS2=
::elegothic sabbat.rar

::SET SongPMS1=
::SET SongPMS2=
::eurasia rock.rar

::SET SongPMS1=
::SET SongPMS2=
::flying duo.rar

::SET SongPMS1=
::SET SongPMS2=
::fm pop.rar

::SET SongPMS1=
::SET SongPMS2=
::fortune-tale beat.rar

::SET SongPMS1=
::SET SongPMS2=
::get wild.rar

::SET SongPMS1=
::SET SongPMS2=
::glide.rar

::SET SongPMS1=
::SET SongPMS2=
::happy j vogue.rar

::SET SongPMS1=
::SET SongPMS2=
::hard pf.rar

::SET SongPMS1=
::SET SongPMS2=
::hounodaiko.rar

::SET SongPMS1=
::SET SongPMS2=
::hyper masquerade.rar

::SET SongPMS1=
::SET SongPMS2=
::jam.rar

::SET SongPMS1=
::SET SongPMS2=
::j-house pop.rar

::SET SongPMS1=
::SET SongPMS2=
::jungle go go.rar

::SET SongPMS1=
::SET SongPMS2=
::kaizoku.rar

::SET SongPMS1=
::SET SongPMS2=
::kaku-g.rar

::SET SongPMS1=
::SET SongPMS2=
::kanransha.rar

::SET SongPMS1=
::SET SongPMS2=
::koushinkyoku.rar

::SET SongPMS1=
::SET SongPMS2=
::lovely trans pop.rar

::SET SongPMS1=
::SET SongPMS2=
::manbo-kayo.rar

::SET SongPMS1=
::SET SongPMS2=
::megane rock.rar

::SET SongPMS1=
::SET SongPMS2=
::melt.rar

::SET SongPMS1=
::SET SongPMS2=
::mfc.rar

::SET SongPMS1=
::SET SongPMS2=
::nocturne.rar

::SET SongPMS1=
::SET SongPMS2=
::pianotronica.rar

::SET SongPMS1=
::SET SongPMS2=
::qma.rar

::SET SongPMS1=
::SET SongPMS2=
::racing.rar

::SET SongPMS1=
::SET SongPMS2=
::r-play battle.rar

::SET SongPMS1=
::SET SongPMS2=
::rumble roses.rar

::SET SongPMS1=
::SET SongPMS2=
::sf pop.rar

::SET SongPMS1=
::SET SongPMS2=
::smile smash.rar

::SET SongPMS1=
::SET SongPMS2=
::symphonic metal op.2.rar

::SET SongPMS1=
::SET SongPMS2=
::toy break core.rar

::SET SongPMS1=
::SET SongPMS2=
::train sim.rar

::SET SongPMS1=
::SET SongPMS2=
::twinkle dance.rar

::SET SongPMS1=
::SET SongPMS2=
::water step.rar

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 14 (AC)
CALL :TitleLookingUp "Pop'n Music 14 Fever"
SET curgamef=%f_pnm14ac%

::SET SongPMS1=
::SET SongPMS2=
::[AEGEAN] 魅せられて ～エーゲ海のテーマ～.rar

::SET SongPMS1=
::SET SongPMS2=
::[ALIPRO] 愛と誠.rar

::SET SongPMS1=
::SET SongPMS2=
::[BATTLE DANCE] シャムシールの舞.rar

::SET SongPMS1=
::SET SongPMS2=
::[BITTERSWEET POP] ちょっと.rar

::SET SongPMS1=
::SET SongPMS2=
::[BROKEN-BANG BEAT] BIG-BANG STARS.rar

::SET SongPMS1=
::SET SongPMS2=
::[BULGARIAN RHYTHM] Deep Magenta.rar

::SET SongPMS1=
::SET SongPMS2=
::[CABARET] マッシュな部屋.rar

::SET SongPMS1=
::SET SongPMS2=
::[CHIBIKKO IDOL] オトメルンバ♪.rar

::SET SongPMS1=
::SET SongPMS2=
::[CLASSIC 11] 想い出をありがとう.rar

::SET SongPMS1=
::SET SongPMS2=
::[CONTEMPORARY NATION 2] サヨナラ＊ヘヴン.rar

::SET SongPMS1=
::SET SongPMS2=
::[CYBER FLAMENCO] hora de verdad.rar

::SET SongPMS1=
::SET SongPMS2=
::[DES ROCK] レトロスペクト路.rar

::SET SongPMS1=
::SET SongPMS2=
::[DISCO A GOGO] Dance the night away.rar

::SET SongPMS1=
::SET SongPMS2=
::[DISCO FEVER] DA DA DA DANCING!!.rar

::SET SongPMS1=
::SET SongPMS2=
::[ELE DISCO] 電気ダンス.rar

::SET SongPMS1=
::SET SongPMS2=
::[ELECTROPOP] FAKE.rar

::SET SongPMS1=
::SET SongPMS2=
::[ENZETSU] 猿の経.rar

::SET SongPMS1=
::SET SongPMS2=
::[FEVER HERO ED] わたしのフォーティーン(フィーバー戦士ポップン14EDテーマ).rar

::SET SongPMS1=
::SET SongPMS2=
::[FEVER HERO] フィーバー戦士ポップン14のテーマ.rar

::SET SongPMS1=
::SET SongPMS2=
::[FEVER ROBO] 踊るフィーバーロボ.rar

::SET SongPMS1=
::SET SongPMS2=
::[French Merchen] トルバドゥールの回想.rar

::SET SongPMS1=
::SET SongPMS2=
::[GIRLS PUNK STYLE] 旅立ちの唄.rar

::SET SongPMS1=
::SET SongPMS2=
::[GRADIUS II] A SHOOTING STAR.rar

::SET SongPMS1=
::SET SongPMS2=
::[HEAVEN] ヴォイス.rar

::SET SongPMS1=
::SET SongPMS2=
::[HIGH SPEED LOVE SONG] High School Love.rar

::SET SongPMS1=
::SET SongPMS2=
::[HOKEN RAP] DOLLAR DOLLAR.rar

::SET SongPMS1=
::SET SongPMS2=
::[J-RAP 80's] 惚れたぜHarajuku.rar

::SET SongPMS1=
::SET SongPMS2=
::[KENKA DRUMS] BBLLAASSTT!!.rar

::SET SongPMS1=
::SET SongPMS2=
::[LONELY FEEL] fragments.rar

::SET SongPMS1=
::SET SongPMS2=
::[MONDO ROCK] CODENAME：APRIL.rar

::SET SongPMS1=
::SET SongPMS2=
::[NEO CLASSICAL HEAVYMETAL] Aithon.rar

::SET SongPMS1=
::SET SongPMS2=
::[OUENKA] 燃やせ!青春～ポップン学園応援歌～.rar

::SET SongPMS1=
::SET SongPMS2=
::[PANIC POP] ベイビーP.rar

::SET SongPMS1=
::SET SongPMS2=
::[PARTY ROCK] PARTY A GO GO☆.rar

::SET SongPMS1=
::SET SongPMS2=
::[PONPOKOLIN] おどるポンポコリン.rar

::SET SongPMS1=
::SET SongPMS2=
::[POP DESCO] popdod.rar

::SET SongPMS1=
::SET SongPMS2=
::[PROGRESSIVE BAROQUE] Ubertreffen.rar

::SET SongPMS1=
::SET SongPMS2=
::[PSYCHEDELIC TRANCE] Psyche Planet-V.rar

::SET SongPMS1=
::SET SongPMS2=
::[PUPPET ENSEMBLE] セレクトショップに横たわるネコ.rar

::SET SongPMS1=
::SET SongPMS2=
::[RAY JAZZ] NIGHT FEVER.rar

::SET SongPMS1=
::SET SongPMS2=
::[RONDO] おもちゃばこのロンド.rar

::SET SongPMS1=
::SET SongPMS2=
::[SONIC BOOM] Break on Through.rar

::SET SongPMS1=
::SET SongPMS2=
::[SPACE WALTZ] Flutter.rar

SET SongPMS1="[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー [05]";"[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー [09 EX]";"[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー [09 HYPER]";"[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー [09 NORMAL]";
SET SongPMS2=" [5button]";" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process "#preview.wav";"smario";%TN2%;"[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー.rar";"[SUPER MARIO BROTHERS BGM] スーパーマリオブラザーズBGMメドレー";"SUPER MARIO BROTHERS BGM - SUPER MARIO BROTHERS BGM Medley";false;false;false;

::SET SongPMS1=
::SET SongPMS2=
::[TAIKYOKUKEN] 個胃Ｘ光.rar

::SET SongPMS1=
::SET SongPMS2=
::[TOKIMEKI BOSSA] いつか王子さまがぁ!.rar

::SET SongPMS1=
::SET SongPMS2=
::[TRANCERANCE] full-consciousness green.rar

::SET SongPMS1=
::SET SongPMS2=
::[TRI▼EURO] Let's go out !.rar

::SET SongPMS1=
::SET SongPMS2=
::[TRIBAL POP] Gradation.rar

::SET SongPMS1=
::SET SongPMS2=
::[VS.SOUND] 異能対決! VS.淀ジョル.rar

::SET SongPMS1=
::SET SongPMS2=
::[YURU-POP] あつまれ!ビーくんソング.rar

::SET SongPMS1=
::SET SongPMS2=
::agean.rar

::SET SongPMS1=
::SET SongPMS2=
::ali pro.rar

::SET SongPMS1=
::SET SongPMS2=
::battle dance.rar

::SET SongPMS1=
::SET SongPMS2=
::bitter sweet pop.rar

::SET SongPMS1=
::SET SongPMS2=
::broken bang beats.rar

::SET SongPMS1=
::SET SongPMS2=
::bulgarian rhythm.rar

::SET SongPMS1=
::SET SongPMS2=
::cabaret.rar

::SET SongPMS1=
::SET SongPMS2=
::chibikko idol.rar

::SET SongPMS1=
::SET SongPMS2=
::classic 11.rar

::SET SongPMS1=
::SET SongPMS2=
::contemporary nation 2.rar

::SET SongPMS1=
::SET SongPMS2=
::cyber flamenco.rar

::SET SongPMS1=
::SET SongPMS2=
::des-rock.rar

::SET SongPMS1=
::SET SongPMS2=
::disco a gogo.rar

::SET SongPMS1=
::SET SongPMS2=
::disco fever.rar

::SET SongPMS1=
::SET SongPMS2=
::electro pop.rar

::SET SongPMS1=
::SET SongPMS2=
::eledisco.rar

::SET SongPMS1=
::SET SongPMS2=
::enzetsu.rar

::SET SongPMS1=
::SET SongPMS2=
::fever hero ending.rar

::SET SongPMS1=
::SET SongPMS2=
::fever hero.rar

::SET SongPMS1=
::SET SongPMS2=
::fever robo.rar

::SET SongPMS1=
::SET SongPMS2=
::french merchen.rar

::SET SongPMS1=
::SET SongPMS2=
::girls punkstyle.rar

::SET SongPMS1=
::SET SongPMS2=
::gradius ii.rar

::SET SongPMS1=
::SET SongPMS2=
::heaven.rar

::SET SongPMS1=
::SET SongPMS2=
::highspeed love song.rar

::SET SongPMS1=
::SET SongPMS2=
::hoken rap.rar

::SET SongPMS1=
::SET SongPMS2=
::j-rap 80's.rar

::SET SongPMS1=
::SET SongPMS2=
::kenka drums.rar

::SET SongPMS1=
::SET SongPMS2=
::lonely feel.rar

::SET SongPMS1=
::SET SongPMS2=
::mondo rock.rar

::SET SongPMS1=
::SET SongPMS2=
::neo classical heavymetal.rar

::SET SongPMS1=
::SET SongPMS2=
::ouenka.rar

::SET SongPMS1=
::SET SongPMS2=
::panic pop.rar

::SET SongPMS1=
::SET SongPMS2=
::party rock.rar

::SET SongPMS1=
::SET SongPMS2=
::pompokorin.rar

::SET SongPMS1=
::SET SongPMS2=
::pop desco.rar

::SET SongPMS1=
::SET SongPMS2=
::progressive baroque.rar

::SET SongPMS1=
::SET SongPMS2=
::psychedelic trance.rar

::SET SongPMS1=
::SET SongPMS2=
::puppet ensemble.rar

::SET SongPMS1=
::SET SongPMS2=
::ray jazz.rar

::SET SongPMS1=
::SET SongPMS2=
::rondo.rar

::SET SongPMS1=
::SET SongPMS2=
::sonic boom.rar

::SET SongPMS1=
::SET SongPMS2=
::space waltz.rar

::SET SongPMS1=
::SET SongPMS2=
::taikyokuken.rar

::SET SongPMS1=
::SET SongPMS2=
::tokimeki bossa.rar

::SET SongPMS1=
::SET SongPMS2=
::trance rance.rar

::SET SongPMS1=
::SET SongPMS2=
::tribal pop.rar

::SET SongPMS1=
::SET SongPMS2=
::trieuro.rar

::SET SongPMS1=
::SET SongPMS2=
::vs. sound.rar

::SET SongPMS1=
::SET SongPMS2=
::yuru pop.rar

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 15 (AC)
CALL :TitleLookingUp "Pop'n Music 15 Adventure"
SET curgamef=%f_pnm15ac%

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - IDM - ZETA～素数の世界と超越者～.rar

::SET SongPMS1=
::SET SongPMS2=
	::[Bemani2BMS] pop'n music 15 ADVENTURE - SKA - SKA a go go.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - アーバンメロウポップ - Psychology.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - アドベンチャー - ポップン大冒険メドレー.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - アンセムトランスREMIX - Votum stellarum -forest #25 REMIX-.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ヴィジュアルREMIX - WHITE BIRDS (Mirage Mix).rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ウィンディダンス - FU-FA.rar

::SET SongPMS1=
::SET SongPMS2=
	::[Bemani2BMS] pop'n music 15 ADVENTURE - ヴェネツィアーニ - Canal Grande.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ウェルフェア - つばめ.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ウォートラントゥルーパーズ - ウォートラン･メインテーマ.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ウォーリアー - THE MAN FROM FAR EAST.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - エモーショナル - Loveholic.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - エレキ族 - 純愛ロマンス ～PART 2～.rar

::SET SongPMS1=
::SET SongPMS2=
	::[Bemani2BMS] pop'n music 15 ADVENTURE - エレフラッドウェイヴ - Aqua.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - エンシャントユーロ - In The Ruins.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - オービタリックテクノ - Flow.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - オラトリオ - Apocalypse ～memento mori～.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - オルタナティブロック - Prince on a Star.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - オンラインラブポップ - 恋のミラクル☆.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ガールフッド - ロクブテ.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ガムラントランス - Gamelan de Couple.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ガラージハウス - together 4ever.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - キュアー - 緑の風.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - グラウンドテクノ - QUICK RESULT.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - クリオネテクノ - CLIO.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ケルティックウインド - Caring Dance.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ケンドーロック - ギターケンドー.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - コンティネンタル - 走り続けて.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - シャイポップ - ネガイゴト.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - シンパシー - Usual Days.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - シンパシー2 - Distance.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - シンパシー3 - memories.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ダークネス3 - Quiet.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - チテイタンケン - スーパーモグー.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - なまらジャズ - One Phrase Blues.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ニエンテ - neu.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ニューソウル - My Own Swan.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ネジロック - 螺子之人.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - パーカッシヴ2 - 西麻布水道曲.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ハワイアンパンク - フリフリハワイワン!.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ビジュアル艶歌 - 月影華(TSUKI-KAGE-BANA).rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ヒップロック4 - 路男.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ピンキッシュ - つぼみ.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - フォルクローレ - アンデスの太陽.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - フォレストスノウ - 月雪に舞う花のように.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - フューチャーフュージョン - ∞space.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - プロポーズREMIX - ふたりのマニフェスト (Circle of the afternoon MIX).rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ボーイズR&B - Always.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ホクト - 愛をとりもどせ!!.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - マリンクルーズ - The Seven Ocean.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ミュートロニカ - Polis.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ラウドミクスチャー&ラガ - Soul On Fire.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ラテンREMIX - El pais del sol (GIVE ME MORE SALSA MIX).rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ラブポップ - 大好きだよ.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - ワンピース - ウィーアー!.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 交響的物語 - 駕篭の鳥.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 夏休み - なつやすみのぼうけん.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 悪魔城ドラキュラ - 悪魔城ドラキュラメドレー ～ハイブリッド・ヴァージョン～.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 撫子ロック - 凛として咲く花の如く.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 昭和怪奇譚 - 鹿鳴館の怪人.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 超中華流行歌曲 - 桃花恋情.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 近代絶頂音楽 - 天庭.rar

::SET SongPMS1=
::SET SongPMS2=
::[Bemani2BMS] pop'n music 15 ADVENTURE - 青春ロック - 儚きは我が決意.rar

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 16 (AC)
CALL :TitleLookingUp "Pop'n Music 16 Party"
SET curgamef=%f_pnm16ac%

::SET SongPMS1=
::SET SongPMS2=
::[80's POP] Girl from the Portrait.rar

::SET SongPMS1=
::SET SongPMS2=
::[AFTER A PARTY] Have a good dream.rar

::SET SongPMS1=
::SET SongPMS2=
::[AMBITION] existence.rar

::SET SongPMS1=
::SET SongPMS2=
::[ASIAN RAVE] HAGOROMO.rar

::SET SongPMS1=
::SET SongPMS2=
::[BALLS-OUT ROCK].rar

::SET SongPMS1=
::SET SongPMS2=
::[BEAT 4 DB].rar

::SET SongPMS1=
::SET SongPMS2=
::[BEAT ROCK 3] Days.rar

::SET SongPMS1=
::SET SongPMS2=
::[CAFE PARTY] EURO PICCOLA.rar

::SET SongPMS1=
::SET SongPMS2=
::[CAT JAZZ].rar

::SET SongPMS1=
::SET SongPMS2=
::[CELT TRANCE] NORTH.rar

::SET SongPMS1=
::SET SongPMS2=
::[CHEER PARA].rar

::SET SongPMS1=
::SET SongPMS2=
::[CHIP POP] EFFECT.rar

::SET SongPMS1=
::SET SongPMS2=
::[CLIMAX JUMP] Climax Jump pop'n form.rar

::SET SongPMS1=
::SET SongPMS2=
::[COLLAPSE JAZZ] Ergosphere.rar

::SET SongPMS1=
::SET SongPMS2=
::[CONTEMPORARY NATION 3] Echoes.rar

::SET SongPMS1=
::SET SongPMS2=
::[COOKIE FANTASY] Hearty Party.rar

::SET SongPMS1=
::SET SongPMS2=
::[COSMOLOGY GIRL] UNBOUND MIND.rar

::SET SongPMS1=
::SET SongPMS2=
::[CYBER DIGIBEAT] UNLIMITED.rar

::SET SongPMS1=
::SET SongPMS2=
::[ELEGOTH REMIX] the keel (Nu-Style Gabba mix).rar

::SET SongPMS1=
::SET SongPMS2=
::[EMO] Sorrows.rar

::SET SongPMS1=
::SET SongPMS2=
::[EPIC POETRY] Blind Justice ~Torn souls, Hurt Faiths~.rar

::SET SongPMS1=
::SET SongPMS2=
::[FALSE MARCHEN].rar

::SET SongPMS1=
::SET SongPMS2=
::[GEKKOU KA].rar

::SET SongPMS1=
::SET SongPMS2=
::[GIRLS ALTERNATIVE].rar

::SET SongPMS1=
::SET SongPMS2=
::[GLOSSOLALIA].rar

::SET SongPMS1=
::SET SongPMS2=
::[HINDI POP].rar

::SET SongPMS1=
::SET SongPMS2=
::[HIP ROCK REMIX].rar

::SET SongPMS1=
::SET SongPMS2=
::[HOUSE] 20,november.rar

::SET SongPMS1=
::SET SongPMS2=
::[HYPER JAPANESQUE 2].rar

::SET SongPMS1=
::SET SongPMS2=
::[HYPER J-PARTY ROCK] MIRACLE FLYER!!.rar

::SET SongPMS1=
::SET SongPMS2=
::[INFINITY] kaleido scope.rar

::SET SongPMS1=
::SET SongPMS2=
::[JAPANESE PROGRESSIVE].rar

::SET SongPMS1=
::SET SongPMS2=
::[JEWELRY ROCK] jewelry girl.rar

::SET SongPMS1=
::SET SongPMS2=
::[JIG REMIX] Tir na n'Og (Europa GT Remix).rar

::SET SongPMS1=
::SET SongPMS2=
::[J-TEKNO TRANCE REMIX] Quick Master -Naked Trance Mix-.rar

::SET SongPMS1=
::SET SongPMS2=
::[KEMARI] Kicky Kemari Kicker.rar

::SET SongPMS1=
::SET SongPMS2=
::[KERO! TO MARCH].rar

::SET SongPMS1=
::SET SongPMS2=
::[KONAYUKI].rar

::SET SongPMS1=
::SET SongPMS2=
::[LINK].rar

::SET SongPMS1=
::SET SongPMS2=
::[LITTLE ROCK] Little Rock Overture.rar

::SET SongPMS1=
::SET SongPMS2=
::[Love so sweet] Love so sweet.rar

::SET SongPMS1=
::SET SongPMS2=
::[LOVELY PARADISE TUNE].rar

::SET SongPMS1=
::SET SongPMS2=
::[MAKINA] SigSig.rar

::SET SongPMS1=
::SET SongPMS2=
::[MAXIMUM] Mighty Guy.rar

::SET SongPMS1=
::SET SongPMS2=
::[MELO CORE G] @n H@ppy Choice.rar

::SET SongPMS1=
::SET SongPMS2=
::[MERICAN ROCK].rar

::SET SongPMS1=
::SET SongPMS2=
::[MINIMAL FUSION].rar

::SET SongPMS1=
::SET SongPMS2=
::[MISTY REMIX] blue moon sea.rar

::SET SongPMS1=
::SET SongPMS2=
::[MOOD TECHNO].rar

::SET SongPMS1=
::SET SongPMS2=
::[NANIWAKKO BUKI].rar

::SET SongPMS1=
::SET SongPMS2=
::[NEGAME ROCK].rar

::SET SongPMS1=
::SET SongPMS2=
::[NEW RAVE POP] Run To You.rar

::SET SongPMS1=
::SET SongPMS2=
::[NIGHTMARE CAROUSEL].rar

::SET SongPMS1=
::SET SongPMS2=
::[PARTY TRACK].rar

::SET SongPMS1=
::SET SongPMS2=
::[PICO PUNK].rar

::SET SongPMS1=
::SET SongPMS2=
::[POPCORN PARTY] microwave popcorn.rar

::SET SongPMS1=
::SET SongPMS2=
::[PROGRESSIVE] V.rar

::SET SongPMS1=
::SET SongPMS2=
::[PSG BREAK CORE] CHIP'N'RIDDIM.rar

::SET SongPMS1=
::SET SongPMS2=
::[SHOEGAZER] chilblain.rar

::SET SongPMS1=
::SET SongPMS2=
::[SMOOTH SOUL] Runnin'Away.rar

::SET SongPMS1=
::SET SongPMS2=
::[SOBAKASU].rar

::SET SongPMS1=
::SET SongPMS2=
::[SOUSEI NO AQUARION].rar

::SET SongPMS1=
::SET SongPMS2=
::[SPANISH BALLAD] Dance to Blue.rar

::SET SongPMS1=
::SET SongPMS2=
::[SPY REMIX].rar

::SET SongPMS1=
::SET SongPMS2=
::[SUISEI RAVE] BI-BUN-SEKI-BUn.rar

::SET SongPMS1=
::SET SongPMS2=
::[SWINGING POP].rar

::SET SongPMS1=
::SET SongPMS2=
::[TAWASHI POP].rar

::SET SongPMS1=
::SET SongPMS2=
::[TEARS] Angel Fish.rar

::SET SongPMS1=
::SET SongPMS2=
::[TENTAI KANSOKU].rar

::SET SongPMS1=
::SET SongPMS2=
::[TIME TRAVEL] Rapunzel.rar

::SET SongPMS1=
::SET SongPMS2=
::[TOY CONTEMPORARY].rar

::SET SongPMS1=
::SET SongPMS2=
::[TSUKI NO WALTZ].rar

::SET SongPMS1=
::SET SongPMS2=
::[VIKING].rar

::SET SongPMS1=
::SET SongPMS2=
::[VIOLIN PROGRESSIVE].rar

::SET SongPMS1=
::SET SongPMS2=
::[VISUAL 4] Desire.rar

::SET SongPMS1=
::SET SongPMS2=
::[WORLD TOUR 2] Magical 4.rar

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 17 (AC)
CALL :TitleLookingUp "Pop'n Music 17 The Movie"
SET curgamef=%f_pnm17ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 18 (AC)
CALL :TitleLookingUp "Pop'n Music 18 Sengoku Retsuden"
SET curgamef=%f_pnm18ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 19 (AC)
CALL :TitleLookingUp "Pop'n Music 19 Tune Street"
SET curgamef=%f_pnm19ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 20 (AC)
CALL :TitleLookingUp "Pop'n Music 20 Fantasia"
SET curgamef=%f_pnm20ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 21 (AC)
CALL :TitleLookingUp "Pop'n Music 21 Sunny Park"
SET curgamef=%f_pnm21ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 22 (AC)
CALL :TitleLookingUp "Pop'n Music 22 Lapistoria"
SET curgamef=%f_pnm22ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 23 (AC)
CALL :TitleLookingUp "Pop'n Music 23 eclale"
SET curgamef=%f_pnm23ac%

::Remember, no categories lol

SET SongPMS1="brf3desrow [EASY]";"brf3desrow [EXTRA]";"brf3desrow [HYPER]";"brf3desrow [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"desrow";%RPK%;"[brf3desrow] 在るが儘に.zip";false;"Some desrow song khedice";false;false;false;

SET SongPMS1="bst_papipu [EASY]";"bst_papipu [EXTRA]";"bst_papipu [HYPER]";"bst_papipu [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"papipu";%RPK%;"[bst_papipu] パ→ピ→プ→Yeah!.zip";false;"PaPiPu Yeah";false;false;false;

SET SongPMS1="cs2301 [EASY]";"cs2301 [EXTRA]";"cs2301 [HYPER]";"cs2301 [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"briver";%RPK%;"[cs2301] Blue River.zip";false;"Blue River";false;false;false;

SET SongPMS1="kac02lady [EASY]";"kac02lady [EXTRA]";"kac02lady [HYPER]";"kac02lady [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"klady";%RPK%;"[kac02lady] 恋歌疾風！かるたクイーンいろは.zip";false;"kac lady song";false;false;false;

SET SongPMS1="koubo2315 [EASY]";"koubo2315 [EXTRA]";"koubo2315 [HYPER]";"koubo2315 [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"koubo";%RPK%;"[koubo2315] 少女と時計と恋泥棒.zip";false;"koubo i dunno khedice";false;false;false;

SET SongPMS1="koubo2333 [EASY]";"koubo2333 [EXTRA]";"koubo2333 [HYPER]";"koubo2333 [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"sss";%RPK%;"[koubo2333] Sweet,Sweet,Sweet.zip";false;"Sweet Sweet Sweet";false;false;false;

SET SongPMS1="rie23 [EASY]";"rie23 [EXTRA]";"rie23 [HYPER]";"rie23 [NORMAL]";
SET SongPMS2=" [EASY]";" [EXTRA]";" [HYPER]";" [NORMAL]";
CALL :Process false;"yumi";%RPK%;"[rie23] ユメユメアラウンド～だってYou&Me～.zip";false;"yumiyumi around datte You and Me";false;false;false;

::SET SongPMS1=
::SET SongPMS2=
::

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music 24 (AC)
CALL :TitleLookingUp "Pop'n Music 24 Usagi to neko to shounen no yume"
SET curgamef=%f_pnm24ac%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Pop'n Music ee'MALL
CALL :TitleLookingUp "Pop'n Music ee'MALL"
SET curgamef=%f_pnmee%

SET SongPMS1="memories_ex";"memories_h";"memories_n";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %pr_d1%;"beatrock";%TN1%;"BEAT ROCK.rar";"BEAT ROCK";"BEAT ROCK - memories";false;false;false;

SET SongPMS1="choco_ex";"choco_h";"choco_n";
SET SongPMS2=" [EX]";" [HYPER]";" [NORMAL]";
CALL :Process %pr_d1%;"merrygr";%TN1%;"Merry-go-round.rar";"Merry-go-round";"MERRY-GO-ROUND - CHOCOLATE PHILOSOPHY";false;false;false;

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:AllDone
TITLE	%link% - All done!
ECHO		Everything done correctly. Exiting program...
ECHO/
PAUSE
SET SongPMS1=""
SET	SongPMS2=""
CLS
GOTO	END

:Error
TITLE	%link% - Report errors
COLOR	0C
ECHO	Error, something went wrong and program has stopped to avoid disasters.
ECHO	%ErrorGot%
ECHO	Close this window manually. If you press any button bad things are bound to happen.
PING	1.1.1.1 -n 1 -w 5000 >NUL
ECHO/
PAUSE
:: Ya te cargo el payaso hijo de tu pinche puta madre
GOTO	END

::  PROCESS  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Process
:: Declare all values, since all simfiles pass the same parameters.
IF %4==false GOTO Process_Continue
IF NOT EXIST %4 GOTO :EOF
:Process_Continue
SET preview_orig=%1
SET shortf=%2
SET processtype=%3
SET filename=%4
SET origfolder=%5
SET destfolder=%6
:: Set em anyways, even if they're set on false
SET bannerfile=%7
SET chara_ver1=%8
SET chara_ver2=%9
:: DONT DO SHIT HERE, DERIVAR A OTROS MODULOS WEON CONCHETUMARE WEA QL
IF %processtype%=="Process_Regular_1"	CALL :Process_Regular_1
IF %processtype%=="Process_Regular_2"	CALL :Process_Regular_2
IF %processtype%=="Process_Regular_3"	CALL :Process_Regular_3
IF %processtype%=="Process_Neutral_1"	CALL :Process_Neutral_1
IF %processtype%=="Process_Neutral_2"	CALL :Process_Neutral_2
IF %processtype%=="Process_Simple_1"	CALL :Process_Simple_1
IF %processtype%=="Process_Simple_2"	CALL :Process_Simple_2
IF %processtype%=="Process_Extra_1"		CALL :Process_Extra_1
IF %processtype%=="Process_POP10"		CALL :Process_POP10
IF %processtype%=="Process_RootedPack"	CALL :Process_RootedPack
GOTO	:EOF

::  PROCESS MODULES  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Process_Regular_CommonStartActions
CALL	:SetTitleProcessing
CALL	:AvoidFolderConflict
CALL	:CheckOrPanic "7z.exe"
7z x	%filename%
CALL	:RenameFolderAndEnter
CALL	:DeleteOtherSimsFiles
GOTO	:EOF
::::::::::::::
:Process_Regular_CommonEndActions
CALL	:Module_simfileImages
CD		..
CALL	:AutoCategory
IF %DeleteRAR%==true DEL %filename%
CALL	:CleanAndVerify
CALL	:SetTitleInit
GOTO	:EOF

:Process_Regular_1
CALL	:Process_Regular_CommonStartActions
CALL	:Process_Regular_1_Helper
CALL	:Kotaku
CALL	:Process_Regular_CommonEndActions
GOTO	:EOF
:Process_Regular_1_Helper
CD		%shortf%%cprefix%
CALL	:CheckOrPanic %preview_orig%
MOVE /Y	%preview_orig%				"../"%preview_dest%
CD		..
RD		%shortf%%cprefix%
GOTO	:EOF

:Process_Regular_2
CALL	:Process_Regular_CommonStartActions
CALL	:Process_Regular_2_Helper
CD		..
CALL	:Kotaku
CALL	:Process_Regular_CommonEndActions
GOTO	:EOF
:Process_Regular_2_Helper
CD		%shortf%"/"%shortf%%cprefix%
CALL	:CheckOrPanic %preview_orig%
MOVE	%preview_orig%				"../"%preview_dest%
CD		..
RD		%shortf%%cprefix%
CD		..
CD		%shortf%%f2prefx%"/"%shortf%%f2prefx%%cprefix%
MOVE	%preview_orig%				"../"%preview_dest%
CD		..
RD		%shortf%%f2prefx%%cprefix%
GOTO	:EOF

:Process_Regular_3
CALL	:Process_Regular_CommonStartActions
CALL	:Process_Regular_2_Helper
CD		..
CALL	:Process_Regular_3_Helper
CD		..
CALL	:Kotaku
CALL	:Process_Regular_CommonEndActions
GOTO	:EOF
:Process_Regular_3_Helper
CD		%shortf%%f3prefx%"/"%shortf%%f3prefx%%cprefix%
CALL	:CheckOrPanic %preview_orig%
MOVE	%preview_orig%				"../"%preview_dest%
CD		..
RD		%shortf%%f3prefx%%cprefix%
GOTO	:EOF

:: Neutral: No moving through folders, just rename files
:Process_Neutral_1
CALL	:CheckOrPanic %preview_orig%
RENAME	%preview_orig%				"../"%preview_dest%
CALL	:Kotaku
GOTO	:EOF

:Process_Neutral_2
RENAME	%preview_orig%				%preview_dest%
CALL	:Kotaku
GOTO	:EOF

:Process_Simple_1
CALL	:Process_Regular_CommonStartActions
::CALL	:Process_Regular_1_Helper
CALL	:Process_Simple_1_Helper
CALL	:Kotaku
CALL	:CheckOrPanic %preview_orig%
RENAME	%preview_orig%	%preview_dest%
CALL	:Process_Regular_CommonEndActions
GOTO	:EOF
:Process_Simple_1_Helper
IF EXIST %shortf%"_2nd" (
	CD %shortf%"_2nd"
	RENAME %preview_orig%	%preview_dest%
	CD ..
)
GOTO	:EOF

:Process_Simple_2
ECHO Programando
GOTO	Error
GOTO	:EOF

:Process_Extra_1
:: Handle special non-standard cases
IF %filename%=="[PMS] Pop'n Music - ファンタジー - monde des songe 190927.rar" (
	CALL	:Process_Regular_CommonStartActions
	CALL	:Process_Regular_2_Helper
	RENAME	"ファンタジー [EX]_431.pms"		%shortf%" [EX]_431.pms"
	RENAME	"ファンタジー [HYPER]_327.pms"	%shortf%" [HYPER]_327.pms"
	RENAME	"ファンタジー [NORMAL]_327.pms"	%shortf%" [NORMAL]_327.pms"
	CD		..
	CALL	:Kotaku
	CALL	:Process_Regular_CommonEndActions
)
IF %filename%=="[PMS] Pop'n Music - ラップ - YOUNG DREAM 190927.rar" (
	CALL	:Process_Regular_CommonStartActions
	CALL	:Process_Regular_2_Helper
	RENAME	"ラップ [HYPER]_461.pms"		%shortf%" [HYPER]_461.pms"
	RENAME	"ラップ [NORMAL]_460.pms"		%shortf%" [NORMAL]_460.pms"
	CD		..
	CALL	:Kotaku
	CALL	:Process_Regular_CommonEndActions
)
IF %filename%=="[PMS] Pop'n Music 5 - ヘヴィロック - INNOVATION 191001.rar" (
	CALL	:Process_Regular_CommonStartActions
	CD		%shortf%"/"%shortf%%cprefix%
	MOVE	"0001.wav"					"../"%preview_dest%
	CD		..
	RD		%shortf%%cprefix%
	CD		..
	:: Special Case >> "2" instead of "2nd"
	CD		%shortf%"2/"%shortf%"2"%cprefix%
	MOVE	"0001.wav"					"../"%preview_dest%
	CD		..
	RD		%shortf%"2"%cprefix%
	CD		..
	CALL	:Kotaku
	CALL	:Process_Regular_CommonEndActions
)
GOTO	:EOF

:Process_POP10
CALL	:SetTitleProcessing
CALL	:RenameFolderAndEnter
IF EXIST %shortf%_2nd (
	CD		%shortf%_2nd
	RENAME	%preview_orig%			%preview_dest%
	CD		..
)
CALL	:CheckOrPanic %preview_orig%
RENAME	%preview_orig%				%preview_dest%
CALL	:Kotaku
CALL	:Module_simfileImages
CD		..
GOTO	:EOF

:Process_RootedPack
:: Pop'n Music 23 eclale
CALL	:SetTitleProcessing
CALL	:DeleteDestinFolderIfExistsAlready
CALL	:CheckOrPanic %filename%
7z x -o%destfolder%		%filename%
CALL	:CheckOrPanic %destfolder%
CD		%destfolder%
CALL	:Kotaku
CD		..
CALL	:CheckOrPanic 7z.exe
CALL	:AutoCategory
IF %DeleteRAR%==true DEL %filename%
GOTO	:EOF

::  FUNCTION MODULES  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:SetTitleInit
TITLE	%link%
GOTO	:EOF

:SetTitleProcessing
TITLE	%link% - Processing %curgamef%: %destfolder%
GOTO	:EOF

:TitleLookingUp
TITLE	%link% - Looking up %1
GOTO	:EOF

:AvoidFolderConflict
CALL	:DeleteOriginFolderIfExistsAlready
CALL	:DeleteDestinFolderIfExistsAlready
GOTO	:EOF
:DeleteOriginFolderIfExistsAlready
IF EXIST %origfolder% RD /S /Q %origfolder%
GOTO	:EOF
:DeleteDestinFolderIfExistsAlready
IF EXIST %destfolder% RD /S /Q %destfolder%
GOTO	:EOF

:RenameFolderAndEnter
RENAME	%origfolder%	%destfolder%
CD		%destfolder%
GOTO	:EOF

:RenameFolderOnly
RENAME	%origfolder%	%destfolder%
GOTO	:EOF

:CheckOrPanic
IF NOT EXIST %1 SET ErrorGot=%1 not found
IF NOT EXIST %1 GOTO Error
GOTO	:EOF

:: PMS Renamer function
:Kotaku
SETLOCAL EnableDelayedExpansion
set i=0
for %%a in (%SongPMS1%) do (
	set /A i+=1
	set lista[!i!]=%%a.pms
	set total=!i!
)
set i=0
for %%a in (%SongPMS2%) do (
	set /A i+=1
	set listb[!i!]=%%a.pms
)
for /L %%i in (1,1,!total!) do (
	RENAME !lista[%%i]!		!shortf!!listb[%%i]!
	IF NOT EXIST !shortf!!listb[%%i]! GOTO Error
)
ENDLOCAL
GOTO	:EOF

:Module_simfileImages
IF NOT %WriteImageInfoFile%==true GOTO :EOF
ECHO/
ECHO	%bannerfile% >> %SimfileImagesFileName%
ECHO	%chara_ver1% >> %SimfileImagesFileName%
ECHO	%chara_ver2% >> %SimfileImagesFileName%
ECHO/
GOTO	:EOF

:DeleteOtherSimsFiles
DEL /s	*.csv
DEL /s	*.bme
DEL /s	*.lr
GOTO	:EOF

:AutoCategory
IF NOT %AutoCategory%==true GOTO :EOF
IF NOT EXIST %curgamef% MKDIR	%curgamef%
MOVE	%destfolder%			%curgamef%
GOTO	:EOF

:CleanAndVerify
SET preview_orig=""
SET shortf=""
SET processtype=""
SET filename=""
SET origfolder=""
SET destfolder=""
SET bannerfile=""
SET chara_ver1=""
SET chara_ver2=""
SET SongPMS1=""
SET SongPMS2=""
:: Assuming we are standing on the bat folder
IF NOT EXIST 7z.exe SET ErrorGot=No existe 7z.exe
IF NOT EXIST 7z.exe GOTO Error
GOTO	:EOF

:Module_BringFilesToProgramRoot
SET		sows_folders="21 Sunny Park - pms";"pop'n";"pop'n 2";"pop'n 3";"pop'n 4";"pop'n 5";"pop'n 6";"pop'n 7";"pop'n 8";"pop'n 9";"popn eeMALL";"Pop'n Music 16 - Party! AC .pms Rip";"popn11ac";"popn12ac";"popn13ac";"popn14ac";"popn15ac";"popn15_fix";"popn16ac";"popn17ac";"popn17_fix";"popn18ac";"popn18_fix";"popn19ac";"popn20ac_part1";"popn23_pms";
FOR %%i	in (%sows_folders%) do (
	IF EXIST %%i (
		CD %%i
		MOVE *.rar		../*.rar
		MOVE *.zip		../*.zip
	)
)
GOTO	:EOF

:END